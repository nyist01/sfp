import vue from 'vue'

import userComponent from './index.vue'

const ToastConstructor = vue.extend(userComponent)

function showUserCard(data) {
  const toastDom = new ToastConstructor({
    el: document.createElement('div'),
  })
  toastDom.data = data
  document.querySelectorAll(".wrap").forEach(i => {
    i.remove()
  })
  document.body.appendChild(toastDom.$el)
}

// 注册为全局组件的函数
export default function registryUserCard() {
  vue.prototype.$userCard = showUserCard
}

