import request from '@/utils/my-request'

export function downloadFile(params) {
  return request({
    url: '/file/download',
    method: 'get',
    responseType: 'blob',
    params
  })
}

export function getFiles(id) {
  return request({
    url: '/work/select/files',
    method: 'get',
    params: {id: id}
  })
}
