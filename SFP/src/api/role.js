import request from '@/utils/my-request'

export function getRoleChild() {
  return request({
    url: '/role/getChild',
    method: 'get'
  })
}

export function updateRole(data) {
  return request({
    url: '/role/update',
    method: 'post',
    data
  })
}



