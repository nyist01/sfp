import request from '@/utils/my-request'

export function getPermission() {
  return request({
    url: '/permission/getPermission',
    method: 'get',
  })
}

export function getRole() {
  return request({
    url: '/permission/getRolePermission',
    method: 'get',
  })
}


export function refresh() {
  return request({
    url: '/permission/refreshPermissions',
    method: 'post',
  })
}
