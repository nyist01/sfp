import request from '@/utils/my-request'

export function getClockInTime() {
  return request({
    url: '/clock/getTime',
    method: 'get'
  })
}

export function updateClockInTime(data) {
  return request({
    url: '/clock/setTime',
    method: 'post',
    data
  })
}

export function clockIn(data) {
  return request({
    url: '/clock/clockIn',
    method: 'post',
    data
  })
}

export function statisticsAll() {
  return request({
    url: '/clock/statistics/all',
    method: 'get'
  })
}

export function statisticsMsg() {
  return request({
    url: '/message/statistics',
    method: 'get'
  })
}

export function getUnreadMessage() {
  return request({
    url: '/message/getUnreadMessage',
    method: 'get',
  })
}

export function statisticsOne() {
  return request({
    url: '/clock/statistics/one',
    method: 'get',
  })
}


export function getToday() {
  return request({
    url: '/clock/getToday',
    method: 'get',
  })
}
