import request from '@/utils/my-request'

export function selectAll() {
  return request({
    url: '/department/select/all',
    method: 'get'
  })
}


export function deleteDepartment(data) {
  return request({
    url: '/department/delete',
    method: 'post',
    data
  })
}

export function updateDepartment(data) {
  return request({
    url: '/department/update',
    method: 'post',
    data
  })
}

export function insertDepartment(data) {
  return request({
    url: '/department/insert',
    method: 'post',
    data
  })
}

export function getDepartmentChild() {
  return request({
    url: '/department/getChild',
    method: 'get'
  })
}
