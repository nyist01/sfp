import request from '@/utils/my-request'

export function getCount() {
  return request({
    url: '/work/getCount',
    method: 'get',
  })
}

export function getWorkTo(data) {
  return request({
    url: '/work/select/to',
    method: 'post',
    data
  })
}

export function getWorkFrom(data) {
  return request({
    url: '/work/select/from',
    method: 'post',
    data
  })
}

export function getWorkAll(data) {
  return request({
    url: '/work/select/all',
    method: 'post',
    data
  })
}

export function details(id) {
  return request({
    url: '/work/details',
    method: 'get',
    params: {id: id}
  })
}

export function disposeWork(data) {
  return request({
    url: '/work/dispose',
    method: 'post',
    data
  })
}

export function switchWork(data) {
  return request({
    url: '/work/switchOver',
    method: 'post',
    data
  })
}

export function endWork(data) {
  return request({
    url: '/work/end',
    method: 'post',
    data
  })
}

export function insertWork(data) {
  return request({
    url: '/work/insert',
    method: 'post',
    data
  })
}


export function statisticsWork() {
  return request({
    url: '/work/statistics',
    method: 'get'
  })
}

export function statisticsType() {
  return request({
    url: '/work/statisticsType',
    method: 'get'
  })
}

