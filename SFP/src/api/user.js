import request from '@/utils/my-request'

export function login(data) {
  return request({
    url: '/login',
    method: 'post',
    data
  })
}

export function getInfo() {
  return request({
    url: '/user/info',
    method: 'get'
  })
}

export function getDetails(params) {
  return request({
    url: '/user/select/details',
    method: 'get',
    params
  })
}

export function selectNoPage(data) {
  return request({
    url: '/user/select/noPage',
    method: 'post',
    data
  })
}

export function selectPage(data) {
  return request({
    url: '/user/select/detailsByDepartment',
    method: 'post',
    data
  })
}

export function deleteUser(data) {
  return request({
    url: '/user/delete',
    method: 'post',
    data
  })
}

export function update(data) {
  return request({
    url: '/user/update',
    method: 'post',
    data
  })
}

export function updateRole(data) {
  return request({
    url: '/user/updateRole',
    method: 'post',
    data
  })
}

export function statisticsLoginNumber() {
  return request({
    url: '/user/statistics',
    method: 'get',
  })
}

export function addUser(data) {
  return request({
    url: '/user/insert',
    method: 'post',
    data
  })

}

export function complete(data) {
  return request({
    url: '/user/complete',
    method: 'post',
    data
  })
}

export function getSecurity(params) {
  return request({
    url: '/user/forget/getSecurity',
    method: 'get',
    params
  })
}

export function getCode(data) {
  return request({
    url: '/user/forget/getCode',
    method: 'post',
    data
  })
}

export function verifyCode(data) {
  return request({
    url: '/user/forget/verifyCode',
    method: 'post',
    data
  })
}

export function updatePwd(data) {
  return request({
    url: '/user/forget/updatePwd',
    method: 'post',
    data
  })
}

export function updatePwdSelf(data) {
  return request({
    url: '/user/updatePwd',
    method: 'post',
    data
  })
}

export function insertFile(data) {
  return request({
    url: '/user/insertFile',
    method: 'post',
    data
  })
}

export function getAvatar(params) {
  return request({
    url: '/user/getAvatar',
    method: 'get',
    params
  })
}
