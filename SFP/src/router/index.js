import Vue from 'vue'
import Router from 'vue-router'
import Layout from '@/layout'

Vue.use(Router)

export const constantRoutes = [
  {
    path: '/login',
    component: () => import('@/views/login/index'),
    hidden: true,
    meta: {title: '登录'}
  },

  {
    path: '/info',
    component: () => import('@/views/info/index'),
    hidden: true,
    meta: {title: '完善信息'}
  },

  {
    path: '/forget',
    component: () => import('@/views/forget/index'),
    hidden: true,
    meta: {title: '忘记密码'}
  },

  {
    path: '/404',
    component: () => import('@/views/404'),
    hidden: true
  },

  {
    path: '/',
    component: Layout,
    redirect: '/dashboard',
    children: [{
      path: 'dashboard',
      name: 'Dashboard',
      component: () => import('@/views/dashboard/index'),
      meta: {title: '首页', icon: 'dashboard', affix: true}
    }]
  },

  {
    path: '/chat',
    component: Layout,
    redirect: '/chat/index',
    children: [{
      path: 'index',
      name: 'Chat',
      component: () => import('@/views/chat/index'),
      meta: {title: '聊天消息', icon: 'chat'}
    }]
  },

  {
    path: '/department',
    component: Layout,
    redirect: '/department/index',
    children: [
      {
        path: 'index',
        name: 'Department',
        component: () => import('@/views/department/index'),
        meta: {title: '用户管理', icon: 'nested'},
      }
    ]
  },


]

export const asyncRoutes = [

  {
    path: '/work',
    component: Layout,
    redirect: '/work/index',
    meta: {title: '事务管理', icon: 'form'},
    children: [
      {
        path: '/unfinished/index',
        name: 'UnfinishedWork',
        component: () => import('@/views/work/unfinished/index'),
        meta: {title: '待办事务'}
      },
      {
        path: 'unfinished/details',
        name: 'UnfinishedDetails',
        hidden: true,
        component: () => import('@/views/work/unfinished/details'),
        meta: {title: '待办详情'},
      },
      {
        path: '/my/index',
        name: 'MyWork',
        component: () => import('@/views/work/my/index'),
        meta: {title: '我的事务'}
      },
      {
        path: 'my/details',
        name: 'MyDetails',
        hidden: true,
        component: () => import('@/views/work/my/details'),
        meta: {title: '事务详情'},
      },

      {
        path: '/all/index',
        name: 'AllWork',
        component: () => import('@/views/work/all/index'),
        meta: {title: '所有事务', roles: ['超级管理员']}
      },
      {
        path: 'all/details',
        name: 'AllDetails',
        hidden: true,
        component: () => import('@/views/work/all/details'),
        meta: {title: '事务详情'},
      },
    ]
  },

  {
    path: '/role',
    component: Layout,
    name: 'Role',
    redirect: '/role/index',
    children: [
      {
        path: 'index',
        name: 'Role',
        component: () => import('@/views/role/index'),
        meta: {title: '角色管理', icon: 'example', roles: ['超级管理员']},
      }
    ]
  },

  {
    path: '/setting',
    component: Layout,
    name: 'Setting',
    meta: {
      title: '系统设置',
      icon: 'component',
    },
    children: [
      {
        path: 'info',
        name: 'Info',
        component: () => import('@/views/setting/info/index'),
        meta: {title: '我的信息'}
      },
      {
        path: 'clockInTime',
        name: 'ClockInTime',
        component: () => import('@/views/setting/clockInTime/index'),
        meta: {title: '打卡策略', roles: ['超级管理员']}
      }
    ]
  },

  {path: '*', redirect: '/404', hidden: true}
]

const createRouter = () => new Router({
  mode: 'history', // require service support
  scrollBehavior: () => ({y: 0}),
  routes: constantRoutes
})

const router = createRouter()

// Detail see: https://github.com/vuejs/vue-router/issues/1234#issuecomment-357941465
export function resetRouter() {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher // reset router
}

export default router
