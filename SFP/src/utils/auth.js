const TokenKey = 'sfp_token'
const ChatKey = 'sfp_chat_'
const User = 'sfp_user'

export function getToken() {
  return JSON.parse(localStorage.getItem(TokenKey));
}

export function setToken(token) {
  return localStorage.setItem(TokenKey, JSON.stringify(token));
}

export function removeToken() {
  return localStorage.removeItem(TokenKey);
}

export function getChat(id) {
  return JSON.parse(localStorage.getItem(ChatKey + id));
}

export function setChat(list, id) {
  return localStorage.setItem(ChatKey + id, JSON.stringify(list));
}

export function removeChat(id) {
  return localStorage.removeItem(ChatKey + id);
}


export function getUser() {
  return JSON.parse(localStorage.getItem(User));
}

export function setUser(user) {
  return localStorage.setItem(User, JSON.stringify(user));
}

export function removeUser() {
  return localStorage.removeItem(User);
}
