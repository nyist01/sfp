import axios from 'axios'
import {Message, MessageBox} from 'element-ui'
import store from '@/store'
import {getToken} from '@/utils/auth'

const service = axios.create({
  baseURL: '/my-api',
  timeout: 5000
})


service.interceptors.request.use(
  config => {

    if (store.getters.token) {
      config.headers['Authorization'] = 'Bearer:' + getToken()
    }
    return config
  },
  error => {
    console.log(error) // for debug
    return Promise.reject(error)
  }
)

service.interceptors.response.use(
  response => {
    const res = response.data
    if (response.headers['content-type'] === 'application/octet-stream') {
      return response
    }
    if (res.code !== 20000) {
      Message({
        message: res.message || '网络异常',
        type: 'error',
        duration: 5 * 1000
      })

      if (res.code === 50012 || res.code === 50014) {
        MessageBox.confirm('您已经登出，您可以取消以停留在此页面，或再次登录', 'Confirm logout', {
          confirmButtonText: 'Re-Login',
          cancelButtonText: 'Cancel',
          type: 'warning'
        }).then(() => {
          store.dispatch('user/resetToken').then(() => {
            location.reload()
          })
        })
      }
      return Promise.reject(new Error(res.message || 'Error'))
    } else {
      if (response.headers.token) {
        store.dispatch('user/setToken', response.headers.token)
      }
      return res
    }
  },
  error => {
    console.log('err' + error)
    Message({
      message: error.message,
      type: 'error',
      duration: 5 * 1000
    })
    return Promise.reject(error)
  }
)

export default service
