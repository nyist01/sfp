/*
 Navicat Premium Data Transfer

 Source Server         : 本地数据库
 Source Server Type    : MySQL
 Source Server Version : 50724
 Source Host           : localhost:3306
 Source Schema         : sfp

 Target Server Type    : MySQL
 Target Server Version : 50724
 File Encoding         : 65001

 Date: 11/11/2020 23:28:40
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for clock_in
-- ----------------------------
DROP TABLE IF EXISTS `clock_in`;
CREATE TABLE `clock_in`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `date_time` datetime(0) NOT NULL COMMENT '签到时间',
  `type` tinyint(3) NOT NULL COMMENT '类型(0上班  1下班)',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `clock_in_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '签到记录' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clock_in
-- ----------------------------
INSERT INTO `clock_in` VALUES (2, 6, '2020-03-17 17:41:03', 1);
INSERT INTO `clock_in` VALUES (3, 6, '2020-03-21 17:47:39', 1);
INSERT INTO `clock_in` VALUES (4, 8, '2020-03-31 17:32:18', 1);
INSERT INTO `clock_in` VALUES (5, 8, '2020-03-31 19:12:52', 0);
INSERT INTO `clock_in` VALUES (7, 2, '2020-03-31 20:32:03', 0);
INSERT INTO `clock_in` VALUES (8, 2, '2020-03-30 11:19:16', 0);
INSERT INTO `clock_in` VALUES (9, 2, '2020-03-30 11:19:31', 1);
INSERT INTO `clock_in` VALUES (10, 2, '2020-03-29 11:19:39', 0);
INSERT INTO `clock_in` VALUES (11, 7, '2020-04-01 12:02:19', 0);

-- ----------------------------
-- Table structure for clock_in_time
-- ----------------------------
DROP TABLE IF EXISTS `clock_in_time`;
CREATE TABLE `clock_in_time`  (
  `id` int(11) NOT NULL,
  `start_time1` time(0) NULL DEFAULT NULL COMMENT '上班签到开始时间',
  `end_time1` time(0) NULL DEFAULT NULL COMMENT '上班签到结束时间',
  `start_time2` time(0) NULL DEFAULT NULL COMMENT '下班签到开始时间',
  `end_time2` time(0) NULL DEFAULT NULL COMMENT '下班签到结束时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '签到时间' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of clock_in_time
-- ----------------------------
INSERT INTO `clock_in_time` VALUES (1, '08:00:00', '20:32:00', '22:30:00', '23:45:00');

-- ----------------------------
-- Table structure for department
-- ----------------------------
DROP TABLE IF EXISTS `department`;
CREATE TABLE `department`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) NULL DEFAULT NULL COMMENT '父部门',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '部门名称',
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `created` datetime(0) NOT NULL COMMENT '创建时间',
  `updated` datetime(0) NOT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `parent_id`(`parent_id`) USING BTREE,
  CONSTRAINT `department_ibfk_1` FOREIGN KEY (`parent_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 61 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '部门' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of department
-- ----------------------------
INSERT INTO `department` VALUES (53, NULL, '顶级部门', NULL, '2020-03-08 17:04:25', '2020-03-23 21:22:57');
INSERT INTO `department` VALUES (54, 53, '部门3', '22无备注', '2020-03-08 17:18:43', '2020-03-08 17:18:43');
INSERT INTO `department` VALUES (56, 53, '部门5', '', '2020-03-08 17:19:19', '2020-03-08 17:19:19');
INSERT INTO `department` VALUES (57, 54, '部门6', '', '2020-03-08 17:33:56', '2020-03-08 17:33:56');
INSERT INTO `department` VALUES (58, 56, '部门7', '', '2020-03-08 17:34:04', '2020-03-08 17:34:04');
INSERT INTO `department` VALUES (59, 53, '部门8', '', '2020-03-08 17:34:10', '2020-03-08 17:34:10');
INSERT INTO `department` VALUES (60, 57, '部门6-1', 'wu', '2020-03-26 09:12:50', '2020-03-26 09:12:50');

-- ----------------------------
-- Table structure for files
-- ----------------------------
DROP TABLE IF EXISTS `files`;
CREATE TABLE `files`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '路径',
  `file_name` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '文件名称',
  `is_use` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否被使用(0没有  1被使用)',
  `create_data` datetime(6) NOT NULL COMMENT '创建时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 53 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '文件' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of files
-- ----------------------------
INSERT INTO `files` VALUES (1, '/模板.xlsx', '模板.xlsx', 1, '2020-03-28 15:35:18.000000');
INSERT INTO `files` VALUES (41, '/files/chat/2929a4fc12312.jpg', '12312.jpg', 1, '2020-03-22 10:33:05.000000');
INSERT INTO `files` VALUES (42, '/files/avatar/fd44181c0cc7124ccabe84d24741b86dc227d634.jpg', '0cc7124ccabe84d24741b86dc227d634.jpg', 1, '2020-03-22 10:33:05.000000');
INSERT INTO `files` VALUES (43, '/files/2447d0a9consul配置.jpg', 'consul配置.jpg', 1, '2020-03-22 10:33:05.000000');
INSERT INTO `files` VALUES (45, '/files/avatar/db5ae78emmexport1570467632958.jpg', 'mmexport1570467632958.jpg', 1, '2020-03-27 14:28:29.062000');
INSERT INTO `files` VALUES (51, '/files/avatar/9cccb9fbmmexport1570467632958.jpg', 'mmexport1570467632958.jpg', 1, '2020-03-27 15:00:02.627000');
INSERT INTO `files` VALUES (52, '/files/avatar/b33074e70cc7124ccabe84d24741b86dc227d634.jpg', '0cc7124ccabe84d24741b86dc227d634.jpg', 1, '2020-05-15 21:29:12.519000');

-- ----------------------------
-- Table structure for message
-- ----------------------------
DROP TABLE IF EXISTS `message`;
CREATE TABLE `message`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `from` int(11) NOT NULL COMMENT '发送人',
  `to` int(11) NOT NULL COMMENT '接收人',
  `time` datetime(0) NOT NULL COMMENT '时间',
  `data` mediumtext CHARACTER SET utf8 COLLATE utf8_bin NOT NULL COMMENT '内容',
  `type` int(11) NOT NULL COMMENT '类型',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `imagess-userbasic1`(`from`) USING BTREE,
  INDEX `imagess-userbasic2`(`to`) USING BTREE,
  CONSTRAINT `message_ibfk_1` FOREIGN KEY (`from`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `message_ibfk_2` FOREIGN KEY (`to`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 308 CHARACTER SET = utf8 COLLATE = utf8_bin COMMENT = '消息' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of message
-- ----------------------------
INSERT INTO `message` VALUES (2, 6, 2, '2020-03-11 17:35:42', '我来了', 1);
INSERT INTO `message` VALUES (3, 6, 6, '2020-03-12 13:53:05', '1', 1);
INSERT INTO `message` VALUES (4, 6, 6, '2020-03-12 17:35:32', '2', 1);
INSERT INTO `message` VALUES (5, 6, 6, '2020-03-12 00:09:00', '0', 1);
INSERT INTO `message` VALUES (6, 6, 6, '2020-03-11 00:09:21', '-1', 1);
INSERT INTO `message` VALUES (7, 6, 6, '2020-03-10 00:09:37', '-2', 1);
INSERT INTO `message` VALUES (8, 6, 6, '2020-03-09 00:09:49', '-3', 1);
INSERT INTO `message` VALUES (9, 6, 6, '2020-03-08 00:10:04', '-4', 1);
INSERT INTO `message` VALUES (10, 6, 6, '2020-03-07 00:10:18', '-5', 1);
INSERT INTO `message` VALUES (11, 6, 6, '2020-03-06 00:10:32', '-6', 1);
INSERT INTO `message` VALUES (12, 6, 6, '2020-03-05 00:10:49', '-7', 1);
INSERT INTO `message` VALUES (13, 6, 6, '2020-03-04 00:11:09', '-8', 1);
INSERT INTO `message` VALUES (14, 6, 6, '2020-03-03 00:11:21', '-9', 1);
INSERT INTO `message` VALUES (15, 6, 6, '2020-03-14 15:58:12', '啥玩意<br>', 1);
INSERT INTO `message` VALUES (16, 6, 6, '2020-03-13 15:37:56', '213123', 1);
INSERT INTO `message` VALUES (17, 6, 6, '2020-03-13 15:36:59', '3616', 1);
INSERT INTO `message` VALUES (18, 6, 6, '2020-03-13 15:35:16', '234身份', 1);
INSERT INTO `message` VALUES (19, 6, 6, '2020-03-13 15:32:31', '让他', 1);
INSERT INTO `message` VALUES (20, 6, 6, '2020-03-13 15:30:47', '233', 1);
INSERT INTO `message` VALUES (21, 6, 6, '2020-03-13 15:29:41', '23', 1);
INSERT INTO `message` VALUES (22, 6, 6, '2020-03-13 15:29:02', '22', 1);
INSERT INTO `message` VALUES (23, 6, 6, '2020-03-12 23:53:08', '18<br>', 1);
INSERT INTO `message` VALUES (24, 6, 6, '2020-03-12 23:53:05', '17<br>', 1);
INSERT INTO `message` VALUES (25, 6, 6, '2020-03-12 23:53:05', '16<br>', 1);
INSERT INTO `message` VALUES (26, 6, 6, '2020-03-12 23:53:04', '15<br>', 1);
INSERT INTO `message` VALUES (27, 6, 6, '2020-03-12 23:53:03', '14<br>', 1);
INSERT INTO `message` VALUES (28, 6, 6, '2020-03-12 23:53:02', '13<br>', 1);
INSERT INTO `message` VALUES (29, 6, 6, '2020-03-12 23:53:01', '12<br>', 1);
INSERT INTO `message` VALUES (30, 6, 6, '2020-03-12 23:53:01', '11<br>', 1);
INSERT INTO `message` VALUES (31, 6, 6, '2020-03-12 23:52:59', '10<br>', 1);
INSERT INTO `message` VALUES (32, 6, 6, '2020-03-12 23:52:58', '9<br>', 1);
INSERT INTO `message` VALUES (33, 6, 6, '2020-03-12 23:52:57', '8<br>', 1);
INSERT INTO `message` VALUES (34, 6, 6, '2020-03-12 23:52:56', '7<br>', 1);
INSERT INTO `message` VALUES (35, 6, 6, '2020-03-12 23:52:56', '6<br>', 1);
INSERT INTO `message` VALUES (36, 6, 6, '2020-03-12 23:52:55', '5<br>', 1);
INSERT INTO `message` VALUES (37, 6, 6, '2020-03-12 23:52:55', '4<br>', 1);
INSERT INTO `message` VALUES (38, 6, 6, '2020-03-12 23:52:54', '3<br>', 1);
INSERT INTO `message` VALUES (158, 2, 6, '2020-03-17 09:01:08', '<h1>一，标题</h1><br>', 1);
INSERT INTO `message` VALUES (159, 2, 6, '2020-03-17 09:00:41', '<pre class=\"ql-syntax\" spellcheck=\"false\">w文方\n\n</pre>', 1);
INSERT INTO `message` VALUES (160, 2, 6, '2020-03-17 09:00:26', '稍等一会<br>', 1);
INSERT INTO `message` VALUES (161, 6, 2, '2020-03-17 09:00:16', '来我办公室一趟<br>', 1);
INSERT INTO `message` VALUES (162, 2, 6, '2020-03-17 09:00:07', '你也好<br>', 1);
INSERT INTO `message` VALUES (163, 6, 2, '2020-03-17 08:59:55', '你好<br>', 1);
INSERT INTO `message` VALUES (164, 2, 6, '2020-03-16 21:34:36', '684<br>', 1);
INSERT INTO `message` VALUES (165, 2, 6, '2020-03-16 21:33:43', '555<br>', 1);
INSERT INTO `message` VALUES (166, 2, 6, '2020-03-16 21:33:25', '351<br>', 1);
INSERT INTO `message` VALUES (167, 2, 6, '2020-03-16 21:33:12', '35161<br>', 1);
INSERT INTO `message` VALUES (168, 2, 6, '2020-03-16 21:33:05', '351<br>', 1);
INSERT INTO `message` VALUES (169, 6, 2, '2020-03-16 20:41:06', '8<br>', 1);
INSERT INTO `message` VALUES (170, 6, 2, '2020-03-16 20:40:59', '7<br>', 1);
INSERT INTO `message` VALUES (171, 6, 2, '2020-03-16 20:38:05', '<img src=\"/files/chat/0c60a74b0cc7124ccabe84d24741b86dc227d634.jpg\">', 2);
INSERT INTO `message` VALUES (172, 6, 2, '2020-03-16 20:31:21', '6<br>', 1);
INSERT INTO `message` VALUES (173, 6, 2, '2020-03-16 20:29:06', '6<br>', 1);
INSERT INTO `message` VALUES (174, 6, 2, '2020-03-16 20:24:45', '6', 1);
INSERT INTO `message` VALUES (175, 6, 2, '2020-03-16 20:24:44', '5', 1);
INSERT INTO `message` VALUES (176, 6, 2, '2020-03-16 20:24:05', '4<br>', 1);
INSERT INTO `message` VALUES (177, 6, 2, '2020-03-16 20:23:58', '3<br>', 1);
INSERT INTO `message` VALUES (178, 6, 2, '2020-03-16 20:03:17', '2', 1);
INSERT INTO `message` VALUES (179, 6, 2, '2020-03-16 19:55:03', '3', 1);
INSERT INTO `message` VALUES (180, 2, 6, '2020-03-16 19:37:52', '<img src=\"/files/chat/9f013fdc12312.jpg\">', 2);
INSERT INTO `message` VALUES (181, 2, 6, '2020-03-16 19:37:41', '33', 1);
INSERT INTO `message` VALUES (182, 2, 6, '2020-03-16 19:36:00', '为', 1);
INSERT INTO `message` VALUES (183, 2, 6, '2020-03-16 19:34:33', '234', 1);
INSERT INTO `message` VALUES (184, 2, 6, '2020-03-16 17:53:27', '<img src=\"/files/chat/b6f7c4140cc7124ccabe84d24741b86dc227d634.jpg\"><br>', 2);
INSERT INTO `message` VALUES (185, 2, 6, '2020-03-16 17:51:13', '士大夫递归', 1);
INSERT INTO `message` VALUES (186, 2, 6, '2020-03-16 17:51:03', '士大夫', 1);
INSERT INTO `message` VALUES (187, 6, 2, '2020-03-16 17:13:46', '<span style=\"background-color: rgb(158, 234, 106);\">王妃个儿歌边而后人儿歌4给骨头饿</span>', 1);
INSERT INTO `message` VALUES (188, 6, 2, '2020-03-16 17:13:41', '第三方', 1);
INSERT INTO `message` VALUES (189, 6, 2, '2020-03-16 17:13:15', '个大概VS反对v', 1);
INSERT INTO `message` VALUES (190, 6, 2, '2020-03-16 17:12:19', '哦i糊弄i那婆娘批评家哦', 1);
INSERT INTO `message` VALUES (191, 6, 2, '2020-03-16 17:12:03', '而过5', 1);
INSERT INTO `message` VALUES (192, 6, 2, '2020-03-16 17:11:53', '<pre class=\"ql-syntax\" spellcheck=\"false\">a.substring(0, 7)\n</pre><br>', 1);
INSERT INTO `message` VALUES (193, 6, 2, '2020-03-16 17:11:23', '<span style=\"background-color: rgb(158, 234, 106);\">王妃个儿歌边而后人儿歌4给骨头饿</span>', 1);
INSERT INTO `message` VALUES (194, 6, 2, '2020-03-16 17:11:06', '<span style=\"background-color: rgb(158, 234, 106);\">王妃个儿歌边而后人儿歌4给骨头饿</span>', 1);
INSERT INTO `message` VALUES (195, 6, 2, '2020-03-16 17:08:30', '王妃个儿歌边而后人儿歌4给骨头饿 ', 1);
INSERT INTO `message` VALUES (196, 6, 2, '2020-03-16 17:08:05', '我人格人格', 1);
INSERT INTO `message` VALUES (197, 6, 2, '2020-03-16 17:08:02', '而', 1);
INSERT INTO `message` VALUES (198, 6, 2, '2020-03-16 17:07:03', '<strong>二哥</strong>', 1);
INSERT INTO `message` VALUES (199, 6, 2, '2020-03-16 17:06:57', '二分', 1);
INSERT INTO `message` VALUES (200, 6, 2, '2020-03-16 17:05:08', '<strong>通过</strong>', 1);
INSERT INTO `message` VALUES (201, 6, 2, '2020-03-16 17:04:59', '二分', 1);
INSERT INTO `message` VALUES (202, 6, 2, '2020-03-16 16:53:43', '哦i你好<br>', 1);
INSERT INTO `message` VALUES (203, 2, 6, '2020-03-16 16:52:32', '<img src=\"/files/chat/10691eecE桌面12312.jpg\">', 2);
INSERT INTO `message` VALUES (204, 2, 6, '2020-03-16 16:52:11', '特容', 1);
INSERT INTO `message` VALUES (205, 2, 6, '2020-03-16 16:50:08', '56', 1);
INSERT INTO `message` VALUES (206, 2, 6, '2020-03-16 16:48:51', '78', 1);
INSERT INTO `message` VALUES (207, 2, 6, '2020-03-16 16:48:37', '66', 1);
INSERT INTO `message` VALUES (208, 2, 6, '2020-03-16 16:48:29', '55', 1);
INSERT INTO `message` VALUES (209, 2, 6, '2020-03-16 16:48:19', '44', 1);
INSERT INTO `message` VALUES (210, 2, 6, '2020-03-16 16:38:16', '34', 1);
INSERT INTO `message` VALUES (211, 2, 6, '2020-03-16 10:28:06', '<img src=\"/files/chat/97944c3dE桌面0cc7124ccabe84d24741b86dc227d634.jpg\">', 2);
INSERT INTO `message` VALUES (212, 6, 2, '2020-03-16 10:19:53', '<img src=\"/files/chat/96d01f8c2.png\">', 2);
INSERT INTO `message` VALUES (213, 6, 2, '2020-03-16 10:19:27', '一年太阳能', 1);
INSERT INTO `message` VALUES (214, 6, 2, '2020-03-16 10:19:11', '违法', 1);
INSERT INTO `message` VALUES (215, 6, 2, '2020-03-16 10:18:14', '而改为', 1);
INSERT INTO `message` VALUES (216, 6, 2, '2020-03-16 10:17:40', '闪光灯', 1);
INSERT INTO `message` VALUES (217, 6, 2, '2020-03-16 10:16:46', '法他', 1);
INSERT INTO `message` VALUES (218, 6, 2, '2020-03-16 10:15:42', '二哥', 1);
INSERT INTO `message` VALUES (219, 6, 2, '2020-03-16 10:14:20', '为', 1);
INSERT INTO `message` VALUES (220, 2, 6, '2020-03-14 23:35:37', '<img src=\"/files/chat/4e18b88fE桌面2.png\">', 1);
INSERT INTO `message` VALUES (221, 2, 6, '2020-03-14 23:31:42', '<img src=\"/files/chat/b8cc5190E桌面0cc7124ccabe84d24741b86dc227d634.jpg\">', 1);
INSERT INTO `message` VALUES (222, 2, 6, '2020-03-14 23:27:55', '<img src=\"/files/chat/73fc5d60E\\桌面\\2.png\">', 1);
INSERT INTO `message` VALUES (223, 2, 6, '2020-03-14 23:23:15', '<img src=\"/files/chat/3f15085bE\\桌面\\2.png\">', 1);
INSERT INTO `message` VALUES (224, 6, 2, '2020-03-14 23:11:10', '<img src=\"/files/chat/8af532212.png\">', 1);
INSERT INTO `message` VALUES (225, 6, 2, '2020-03-14 22:27:17', '傻吊<br>', 1);
INSERT INTO `message` VALUES (226, 2, 6, '2020-03-14 22:26:39', '<img src=\"data:image/jpeg;base64,/9j/4AAQSkZJRgABAQAAAQABAAD/2wBDAAgGBgcGBQgHBwcJCQgKDBQNDAsLDBkSEw8UHRofHh0aHBwgJC4nICIsIxwcKDcpLDAxNDQ0Hyc5PTgyPC4zNDL/2wBDAQkJCQwLDBgNDRgyIRwhMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjIyMjL/wAARCADwAPADASIAAhEBAxEB/8QAHwAAAQUBAQEBAQEAAAAAAAAAAAECAwQFBgcICQoL/8QAtRAAAgEDAwIEAwUFBAQAAAF9AQIDAAQRBRIhMUEGE1FhByJxFDKBkaEII0KxwRVS0fAkM2JyggkKFhcYGRolJicoKSo0NTY3ODk6Q0RFRkdISUpTVFVWV1hZWmNkZWZnaGlqc3R1dnd4eXqDhIWGh4iJipKTlJWWl5iZmqKjpKWmp6ipqrKztLW2t7i5usLDxMXGx8jJytLT1NXW19jZ2uHi4+Tl5ufo6erx8vP09fb3+Pn6/8QAHwEAAwEBAQEBAQEBAQAAAAAAAAECAwQFBgcICQoL/8QAtREAAgECBAQDBAcFBAQAAQJ3AAECAxEEBSExBhJBUQdhcRMiMoEIFEKRobHBCSMzUvAVYnLRChYkNOEl8RcYGRomJygpKjU2Nzg5OkNERUZHSElKU1RVVldYWVpjZGVmZ2hpanN0dXZ3eHl6goOEhYaHiImKkpOUlZaXmJmaoqOkpaanqKmqsrO0tba3uLm6wsPExcbHyMnK0tPU1dbX2Nna4uPk5ebn6Onq8vP09fb3+Pn6/9oADAMBAAIRAxEAPwD3+iiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAoorhfHHxDTw7dQaJo9mdV8SXfEFnHz5YI++/oMc44yAeQOaAOu1PVbDRrJ7zUruC0tk+9LM4VR7c9/avOrr436RPdNaeG9H1TX7gf8+sJVPzI3f+O1FpHwpl1m6XWfiBqEmr6gx3LZiQi3gzztAHXtwMD69a9Ms7G10+2W2sreK3gX7sUSBVH4CgDzRfGHxS1BgLDwDb2qkcNe3Y49MjKn8KX7R8aZ3G218NW+ByCzEf8AoRr1IU6gDy3b8af+enhb/wAf/wAKaz/GmJgxg8MXI/uguP5kV6mRRQB5gfF/xN00htR8A294h6mwvBkfhlj+FOi+NujWtwLbxDo+saHKTgm6tiU/Ajk/lXptRXFvDdQmGeJJY2+8kihlP1BoAo6P4k0bxBCJtJ1K1vEP/PGUMR9V6j8a1K881r4PeG9RuTfab9o0TUclluNOcx4P+70/LFYza38Qfh3xrtofE2gx9dQthi4iX1de+Pf/AL6oA9corG8NeKdH8W6Z/aGjXa3EOdrjo8bejKeQf59q2aACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooqnqup2ujaXc6jeyiO2to2lkY9gBn8TQBy/wARPGx8JabBb2EH2vXdQbybC1Xksx43EegyOO5IHHUQfD3wEPDdvLqmqyfa/El/mS9u2wxBJzsU+gPXHUj0AAxPhxpdz4p1m5+IuuQBbm6Jj0uBsn7PbjIBGe55592P8VeqigAooooAWiiigAooooAKKKKADFNxTqKAPLfFPw5utM1X/hKvAbix1lMtNZLxDeDqVI6Ann2Psea6XwN44tPGelvKsZttRtm8q9sX+/A/Przg4OOB0I6g11teSfETT5fBHiS0+IujxnaHWDWLdDgTxMQA/wBeg+oU+tAHrYoqCzu4L+ygvLaQSQToskbr0ZSMgj6ip6ACiiigAooooAKKKKACiiigAooooAKKKKACvKvivNL4h1rw94CtnKjUpvtN8U6rAhzz7HDH6oK9VryzwxH/AG58cvFWrsN0el28dhCeoDHlvxBVh+NAHplpbRWdrFbQRrHDCgjjRRgKoGAB9MVNRivKPFHxG8SXfie68OeAtIjv7qwXN5cSkFUPHyjLAZHI5OScgDigD1eivmjVvjJ8SfDeoJa6zp9pbTY3+VNbEbl9iG5HuK7rwB8cLPxNqMGk6zarYahcMEgkjbMMjHovPKk9s5zQB69RRRQAUUUUAFFc7448UweDfCl3rMwDvEAsEROPMkP3V+nc+wNeUP4b+MWsaPFrMfiYQzXEfnLYRymFlB5C4C7QcEcE/jQB7zRXx1Z/Fnx1pF5zrlxMUbDxXSiQHHY7hkfhivoj4Y/EWLx9pU5mhW21O0IFxChypBzh1zzg479MfSgDvap6tplvrOk3em3YLW91E0UgHowxVyigDzL4LX1xFoOpeF7583ugXj2ze8ZJKn6ZDgewFem15RZgaD+0VeQL8sGvaaJgo6NInX8cI5/GvV6ACiiigAooooAKKKKACiiigAooooAKKKKACvLfg6POvfG16SS02uyqSfQZI/8AQq9Sryv4Lkw23i1H2jbrtxn5umAM/wAqAPT52ZYmKEbwCRnpXyN4K+Jms+DNU1E29nDe/wBoSh7iKVTuaQE8qw5zyexFfT9t4v8AD2obTa6taupjEu8vtUKz7FJJ45bgevavKPFvwe1mx8WDxP4JlhWcT+f9kkIQxvnJKk/KVJ7HGM0AeYfE3xjqnjPWLO81HSn01IIfLigbdzk5LZIGc8D8KzPEFzoDahpcvhO1urVo4YzO0zk5uOpK5J46f4V33i3Rfi54yhgs9Y0PzI4JCyeV5S8kY6hv0ra+HXwMv7HWrfVfFJhRLZhLFYxtvLOD8pdhxgHnAznv6EA97gLGFN5+fA3fXHNSikAxS0AFFFBoA8c/aMiuX8FadJEGMEd8PNx0BKMFJ/UfjXAeC/E/xZ1XRJLfw2z31lbfud8oiLRfLwoZiCeMetfSOtaNY6/pNxpepQCe0uF2uh/Qg9iDyDXjknwQ8SaFdTv4P8XSWkMxBMczPEcDpkpkNj1wKAPHNF1SbwT4rupNU0WC+njSS3mtLwDCue54PIP/AOuvRP2cYJpPFes3KoBClmEcjoGZwQPyVvyqdP2e/EWo6m9zrXiKzPmNvlnj8yaRj3zuC5P416fFo0Hw18IRab4X09rrUbydYIpJFLCSZ/8AlpMR0RQCT2wMDGaAO8zS1wVp4o8RXms21tHpUZtLi8FvHNLFJHmKJT9onbqFDNtWNTyeSeK70UAeU/EfGm/E/wAAav8AKC129oxx/fKqP/Q2r1avLPjQFVvB05UkprsIGOuDzgfkK9ToAKKKKACiiigAooooAKKKKACiiigAooooAK8e+H+kW19q3xI8N6gHNvLqZeREdkLRyFjjIwcEAA+or2GvKrEnQv2h9StySIdd01J0548xOPx4Rz+NAHYQ+A/DkN5FdLp0ZeKc3CKxJQSEBVO3OPlUBUGMKM4xk10gGKUUYoAKKKKAMXxZ4iTwp4avNaltZbqO2UFoojhjlgPy5q3ourW2u6LZ6paEmC6hWVM9QCOh9x0P0qe+srfUbGeyu4lltp42jljboykYIrx6w0P4jfDZ5tO8NWtt4g0N3Z7ZLiQK8BPJz8y/jjIJ54yaAPQdI8bWuseMNb8ORWssc+lBC0rkbZA3XHcYzXUV518NfBuraJd6xr3iOaJ9b1eQPLHEcrCoJO0H1ye3AAFei0AFFFFABRRRQAUUUUAeWfFlhd+JPAelIf3k2rrN9FQrn9Gr1OvKLg/8JJ+0FaxIM2vhyxaRyBx5zjGPr86n/gJr1bFAC0UUUAFFFFABRRRQAUUUUAFFFFABRRRQAV5f8YLWfTE0PxrZR759Cuw0yrwXgcgMM/XA9gxr1Cq9/Y2+pWM9ldxiW2uI2iljPRlYYIoATT7631KwgvrSRZbe4jWWJ1OQysMg/rVmvIfAuq3PgDxJJ8Pdfkb7NJIZNFvZCAssbH/V5/vZzx/eyO6166DQAtFFFABRRRQAUUUUAFFFFABRRRQAVk+JdetPDHh+81i9YCG2jL7c4Lt0VR7kkD8a1JJEiQu7KqAZLMcAV49czSfGDxnHZ2xc+DNHm33Ew4W9nHRR6rz+RJ/iWgDc+DuiXVt4duvEWqL/AMTXX5zeSkjBEZJ2D2HJYezAdq9HpqKqKFUAADAAp1ABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQBznjPwXpnjbR/sOoKUljJe2uUHzwP6j29R3+oBHA2PjbxD8N5l0rx5aTXmnAhLbXLZS4ZewkHUn/AMe/3utew1FcW8N1A8FxEksTqVeORQysD2IPBoAqaRrmma9Zi70q+gu4D/HC4bHsfQ+xrQrybxP8MvCWhrLrtnrdz4TkTnz7aciPPpsJyf8AdUjPpWH8OPE/jjV/Fq2VvqT6z4bgdvM1G7sjEHUDseu7PQEnpzQB7rRRRQAUUUyTdtOzbuwcbumfegB9FeSG8+NsEjqNP0Ccc4IbAHPbLjiuX8TfEv4r+GB/xNNDsLSMk4uIrdpIz/wIOR+eKAPoOub8TePfDnhKDfquoxLKRlbaI75W+ijn8TgV59o/h7xd4702O/vviQjWEow8WjR7R05Ut8pBHcEGuw8NfCvwr4YkW4gsftl6Oftd6fNkz6jPCn6AUAcfcN4v+LmLb7LP4c8IsQZJZBi4vFz0A7A/l/vdK9U0XRbDw/pVvpum26wWsCBURR+ZJ7k9STWhRQAtFFFABRRRQAUUUUAFFFFABRRRQAUUUUAFFFFABRRRQAVieLPE1l4Q8OXWs32WjhX5I1PzSueFUfU9+wye1bdeXfFxFvNc8C6XcLus7rWFMqnocFQB/wCPGgCn4c8A3vjS6h8VeP2aeVxvs9JyRDbo2CNy55JwOP8AvrJ6X/i9qt94Y8P6H/YdwLAvqcUJSABN0e1jtAxjbxz26V6YAFGAMCvk7xTq2q+JfF0La/dG01G31SO0j0cIdkEZOS4boedvPU5z0xQB9Z0UUUAFFFFAGB411S+0Twdquqaakb3dpbtLGJBleOpx34yaq+FvENv4m8HaReXz2yz6lbHfASMSMOJAFPUZB49K6G/s49Q0+5spv9XcRNE/0YYP86+e/gr4bub7xbcy6rLJJD4a3wW8TN8qSs7Zx+TH6kUAdV4o8KXXw6u5PGngtPLtosHU9JB/dTRd2UdiOT7ZJHGQfUtE1e217RbPVbMk293EsqZ6gEdD7jofcVi/EXW7bw/4C1e9uQGDW7QRxn+N3G1R+ZyfYGo/hnok/h34d6Npt0CLhITJIp/hZ2L7fw3Y/CgDrqKKKACiiigAooooAKKKKACiiigAooooAKKKKACiiigAooooAK4b4p+F9Q8SeG4JtHI/tfTLlby0BIG9l6rk9+49wBXc0UAeeeG/i3oeokWGtl9D1pGCT2l6hjAf2Y8Y+uDzXJ/FXQbNviZ4Iv4WYXd7dxQygYwyxyKQ2MZJw+M9MAU/4uzaRq/jrw/4d1ia20/To42v7u+kwrsnzKI1bqM7SPxB6risy48W6H4u+MXgqz0Pc1lpZeNXKFVJC5AUHnACDk0Ae/0UUUAFFFFABXkHwrkXS/iJ470Cb5Zftn2uJW6shZiT+Tx/nXr9eJ/GyyufDWp6R470Njb6jFKLa4kH3XXB27h36MD+HpQBf+Id5aah8VfCfhzVbiO20iIfb5TK21J5dzBEOeOq4+jmvXMAYA6CvCPFdv48+IWn2+h6h4FtrOfzUP8AabXCusI4LFeSQCOwJ/Ovc4k8qFE3FtqhcnqcUAS0UCigAooooAKKKKACiiigAooooAKKKKACiqeq6gmk6XdahLFNLHbxtI0cCF3bHYAdTXML8VfB48OW2uzaqIbOeTyQDE7vHJjJR1QEqQPw9CaAOzorzKP47+CX1iW1a9kWzWIOt6YJSrt/d2bNwPuRiu28PeJtJ8VaadQ0a7F1aiQx+Z5bp8wwSMMAe47UAa9FYOk+LdP1rxDrGi2aTtPpLIlxLtHllmGdqnPUYIOQOQai8FeL7fxr4eXWLa1lto2kaPy5SC2V78UAdHRRXOaT4uttX8V65oEdtKkuk+XvlJBWTeueB2xQBf1fw5ouv+V/a2l2l75X+rM8Qfb9M1m3PgfS7jxRo+ux7rdtKjkjgtoVVYjuBHIA7bj09vxfZ+LobvxzqPhZbSVZrK2S4acsNrhtuAB1z836V0dABRXP6n4x03TPE+l+HmWafUdQ3FY4FDeUoGd0nOVXrz7Gsi/+L3gbTNRubC81zyrm2laGVPsk52upwRkJg8g9KAO3orlfD/xI8J+KtSOn6Lqv2q6EZlKfZ5U+UYBOWUDuO9WPFXjCz8LNpkM0Et1d6lcrbW1tBje5OMnkgYGVzz3FAHRVn6zouna/pzafqtpHdWjsrNG+cEg5HT3FcvqHxW8N6V4wm8PX1ysDQQiSW6Z18tG/55nvuwR+Z9K0tP8AiD4T1W+hsbHXbOe5mbbHEj5LH0H5UAdGiLGoVQAoGAAMAD0p2KwfF/iu28H6RFf3FtNcma5jtooYSN7u3QDPsDU3irxFb+FPDV5rd1GZIbVQSinBYlgoA98kUAbIoqrp959v062u/KaLz4Ul8t/vJuGcH3FY/iDxppHhjUdNtNUaWFdQcpHclP3KNxgO/QZz/jgc0AdFRXPa74z0rw9rOk6Xes/2jU3ZYiu3agXGWckjC81X8N+PNI8RQ6hIJorT7HfSWZE0ygybMfOOfunP6UAdTRWdr2rx6BoF/q00bSx2cDTMinBYKM4FYOqfEPTNF8I6b4kvrW9+yX8UcirBGJGj3oHG7kAdcZ9aAOvorzST43+HYbc3Mul6/HbgA+a9jhcHpyWr0Sxu47+wt7yEMIp4llQOMHDAEZ/OgCeiiigAooooA57xZrusaDZQz6R4efWmZm81EuVh8pQM7iSDmvNJdfTVPh4Nb0Xw1d6Yus3Mkl9Ppmqx2skTo4VSZXUD5+eAPXuefY9Q/wCQddf9cX/lXzFb+I7O7/Z+u/D8CTNc2MiT3DlCETfc/KufXHP+TgAihjv08ST6l9s18RyQCJZE8VWwuSQQcNJ3TjpjjjmvYfD/AIg1Cx+GN7eXSS2bWkbC1v8AWdSW685nY7WeWNeQGYLjHQAV5bdSaEbScx3PhQv5bBQvhm7Uk44wdvB969e+FVnbX3wh0O3u7eK4geJw0UqB1bErEZB4PIB/CgDy/QfDfwun0eG48UeJoLnWpi0t1Il0QN7HJHA5x69zmsrwFoXwx1DwtFceKdTFvqhlcMn2kp8oPy8Y9K7zxLYaB4h+Knh3w3o2l6eyaZI17qrQQIEULjbG5A55GCP9oD1q9faBokfxx0eyTSdPW2bR5HaJbdAjNuYZ24xnjrQBteFbbwndeFL/AMN+DdckSIKxaa2mLSwF84YN1B4OPpXm/hfQIdP+Kniay1TxhqES24jhZ5bva99vibhyTlioJI64xnivT7WTRdS1LxH4X0OBtH1CziiWa8s4Ej4dNylSvUjJ4Poa4XwF4T0iD4peL4NVzqr6csIjutTYSsS6EuzE8HPv0FAGBpvg3RtS8d+JItL1rUZ7G00pLiC8gvcu7FVOGYDkdePavUvhDNcXvwl0l3uZGuZFuAJZGLnPnSAE5644rzrwb4c1W/1vWfEvg1raz0aW+ezaylQlLi2x8zrznPcDgZJGeCD1Hwg8R2MHg3w14bIma9uoLuVGUfKoWdyQxyCDzx/OgDKtYLrwn4utdN026h1vx5q0qy6nfTruitIOrLgYKjpxwcAdPlFdJN4w8Q+e+fhdeyHJO/zo/m9/u1jaJ4SsvCHxs0e0tJp7iW40qe4ubm4ctJNIXOWP5D/655r2XFAHmngr4hXPim8tZbHwVcW2nzM0b6gJUKJjrnCg9Rj8q52XTPEP/C9tLm1vUIjLdW94LD7MMraxhGVCA2Ru53Ec/wBB0PwG/wCSYQf9fU38xVTxP4htf+E51m9tdMFzd+GNGeT7S85Eau4J8pkxzlTnOR0IoA4zUZdL0b4e+JV0a0jvrzTryKOfxBdQxzfbJ3ceZt3huB9SOQe+T2FtZaHcfFfRre40+PS7uxsvtNkYIo1i1AumGOVAwydhz0J475V1o9nrlp4FM/hjHh3UIv38OltIghuJMOC6LwUGDyeRluRgA7M/jLRrv4h6MLmzsLrRw8lnp+pCNg1tergPG27AGRtAwMcggnnABja74b03xd8X9cstb1q8sbaxt7ae28i6WICQqAT8wPPuMGue02xvPEPhW30W91CSLw3HrVzLqOq3FwvEcWwIm5jjLE8Z4zg84roNYHg8fGjxH/wlwsPI+x2xg+1g43bBnGPatX4V6LpWv/DXWtKmjWXTLnU7hVVTwE+UqVPbHBB9RQBj+PvBg8HaBBqlj4q8TTXBvIoVS51DcpDHnooPQetdf8SPF/h7TIJtI8R+HdX1GweJZJJre2DQrlsD94XXa2cdMHkVynjTQtV0f4XaPoWsahFeOmtQ28M8YIPkfNsDZ6sF/IADnGT1Xxxu7aH4V6nbTXEcc108KQozAGQiVGIA74Ck0Acl46tvDum+KX1fXfDeuatpQ02GO3WK2xaWScjaHBHtwem4+oxyPhJPDdtc39rqfw81vUL5rpru2iht5C8Fq23ywV3Zx15569TXoni28v8AxxZ6b4N0O0naxuoree/1Xb+4W34bCN0Ykj81I9SKniHW5PBPxR1a+NjdMtzoaw2Hk27SRtMD8qnHQZWgDb+KGj3Ov+A5tZTVdU0xLbTpJpNPViglyoOyZc84xgg+9c34fvNO0T4ew6fc3GteLTq9lAj6fbEz/Yt0WfL+9mIYJxgZG3Na/j3R9W8RfDca3qGq32myw6S093p1q7JFNIU3bXBOcDkEHP1rN3+FfDHw58PWstzrGjS6rZwXkt1ou4TSuI1Lb3wTgl+nT0oA5KOx8ZSW2naNr2m3WqaDpUokm0azuopLuOPGYlnVTuIx0GBkenGPoTw9rSa9o8V/Hp97YI5KiC9iEci4OOVBOBxXzbpuo+H18Za7PJ4o8bxWrrB5NzbzsLqbCDPnHbyFPC8Dj1r6K8IanZat4Ysriwnvbi3SMQia+BE0hQbSz56scZJ7nmgDdooooAKKKKAIrm3juraS3mXdHKhRxnGQRgiuN1D4Y6HceBJPCVgp0+ykZHaSJQ0jMrBtzE/eJwOvbgYAFdB4p1z/AIRrwxqOs/Z/tP2OEy+Tv2b8dt2Dj8qq6Z4v0y9stBe5uILS81q1S4t7R5csdyByBwM4zjOBn9KAJPFOgT+I9J+w2+r3elktl5bXG50KspQ57Hd+gqtF4NtLXwIPClne3ttbrAYUuYpNsynOS2Rjqeo7g4p/jrxK/g/wdf68lqLprXy8Ql9gbdIqdcH+9n8KsajrV3a+FW1iy0qW/uPISZbOFwGbOCQCR2BJ6ZOOBmgCv4S8EaJ4MsDbaTbsJJP9dcytullPqx/oMDk8c1T8V/D+y8U6nZaoNS1HTNRs0McdzYTeW5Q9VJxnuehHU9aw9V+MOmDw/bXegWd1qWp3aFoLFYWLAqwDq+3JUgH+XWn6h8VrQeFP7V02zaS9W+i09rS6DQkTNyyjIzwM847UAa2gfDy08PadrENvq2qS3urD/SNRmnBuAQCFZWx1XcSM55/Ks+2+E9jBpWvWr63q01zrW0XV7JIplKrnC5x0OTkdxx2ruhdwurGF1mZRnajAmuJsvihbtZ6l/aeh6nZalpu03NgkfmvsZ9quhGAwyR0/Uc0Adho+k2eh6Ta6ZYRCK1towkaD09fqTkk+prmfDHw00bwp4m1DW7CS4aS6VkjhkIKW6s25gnGcE4+nvWHcfGzTEuba1tvDXiKa5uiVt42tBH5rDqFy2TjI6DvXV+Fdc1zWRdy6x4am0WJSv2fzbhZGlHOcgcqenbv1oAkn8KQXHje08TtcyCW2s2tFg2jaQWJznrnmo/FPha98QvatZ+JdU0fyQwcWT483OMbvpg/maq6r4za38d6Z4W0+zF3cSxtPeyM5VbWEdGJweT6fTpnNctbfHLRhruqWtxaz/wBm2zhLW7t43k88DgnGMAZzg55GKAO78IeFrPwb4ct9FsZJJYoSzGSXG52YkknHHfH0FRDwZoa2muWyWexNbZ2viHJaRmBBIJzjqSOwJNZ3h74n+HvE2tLpOn/bBdtG0oWe3MY2jqcml8S+OYdE8Q6DpFuLW4m1G6aKfdcBfs8agbmPoeehx0IoAxLf4QyWVvHbWXjvxZbW0Q2xwx321UX0AAAH4Cuj0XwDoWjeHrTRvs322C2uftaveASMZsk7zwBnnHSovG/jVfDOj6feWMUF9NfXkdrCnmfK27OTuHYY/OumF7aswVbmFmJwAHBzQBhyeBdCn8S3mvXVnHdXd1EkTLcxrJGoUAAqCODxWhonh7S/DlnJZ6TarbW8krTMiszAu3U8k46dBxWpXK+KvFr+Hdd8N6ctms41i7NszmTaYvu8gYOfve1AGXN8K7O+8Qxarqmv65qEUN0bqCxubrfBE27IABB4HT6Vu6j4N0zVvFVjr9/5lxLZQtFDbyENCpY8vtP8XUfl3ANdFXL+KfiD4e8G3FvBrd08MlwheMJEz5AOD0FAGM3wi0eG5kk0jWvEOiRSElrfTNQMUeT1wCDj6dKgb4U3e4lPiF4xUdgdRJOPrWVrPx+8N20tgNLJvIpJwl0zo6NDGSMso2/NxnjI7fhq/wDC9PAP/QTuP/AST/CgDU1r4eprXh2w0WTxBrMMFtCYZXjn+a6BC5MuQQ33c/ia6nTNOg0jSrPTrUMLe0gSCPccnaihRk/QVj+JPG+heE9Ptr3V7pooLo4hKxs+44z0A9K4bX/j94as9LM2hsdQvA4AgljkiBU9TuK4yPSgDvdO8K2um+LNZ8Qx3Ez3GqrCssbY2J5abRt78j1rfrzYfHTwGVB/tK4Ge32STj9K7zStTttZ0u21KzYva3MayxOQQWUjI4PIoAu0UUUAFFFFAHI/FH/kmPiH/rzb+lYEvg+Pxf8AB3w3FE3k6raaXa3GnXKnDRSiJCOewOAD+B6gVufFWTy/hjr2I5ZC9uIwsSbmyzKo49MmuM8N+JPFtv4f0OS8sbXw34a0m2gjvLrU2PnXSoiqVjTGRnHHGTkYOeKAE+I9zrN98LNC0rWIEh1zV7y3tZIY2DfMCTu447KSB0LY5r0nxLBra+G5Y/DU8MWpxqphNwm9X2/wn0yBjPv+I8ivrHxH8Vr6fxVpLzaZY6QCdBVx81zKrAs2DwAduM9PujnDV6BonxEtNS+Hdz4luIxFcWML/bLbkGOZP4OefmOMZ9cdRQB5x49vLzbovinUdDltTbWjpe2EGpC1nikkfG4bctgn5un8XNc/YeHLqXxXpWneJLHf4YvtVd8JqIuN00sbLGjyqQSQY+vBOT6itHVNIv8AxOvgu1nisL7xdqKtf3Ul6pCLbKS0aS7BnYRx65GM0kl3YN8Ntftp9O03T9ZOsrZ6emiq6tNcxMuGUOxbgueeOD0zQB2Xwg0C1g0HxJfaQFs57vUriC1nIaQRwpxGNpbDbSWPXJ7k1oWcnxLn0m500w6bb63Z3CR/2rdKfIvYNrcgKCQwJU9McnocirnwcudPPw6srK0LLPYs8V7DINskU24lww69Tx7fTA5A6ld+KfD/AMSfExuJjpLwPZ2MJkJjZY15cL05+U59yO1AGb4stPiPF448HJqt/oEmpPNP/ZzwK/lowCb/ADMoD/dxjPQ16T4esviOuoH/AISPU9DexMTjFir+YH/hPzIBgV5/4d8GfCK48M6Tcarqemx381lDJcq+rhGEpQF8rv4OSeO1bHwZs9LsPF/jq10SWOXTYpbVbd45fNUriXo3OR+NAGNo3hWHSfHPivTdf1q5mW40IzX+pB/LcbnUsV64AGBjnPTBBxUV14ilfwx4SXTU/sawOvRx2dtBOyySWm44eQZydxJznqee+as634yvn0zxn4w0mCyjS2uYdNsb0QbnnQMBJksSrL0I4/irqLzSpYfHVl4h1DSbC90RNMWSO6ESI+nPENwOc4YHPHpxgDbkgE2h3kU3xe1iLWLQQa3FabLGVJW8qey3ZBUHo24HOPfjgk+eS2PgO68R+O5fFTQrqKahObMSzSIcYPQKQDz6123hrx5PeeOrU3qZ0jxDbl9Gnkt1SWMoTvhfHuCQcnqvrgcrB4m8O6Hr3xAtdXtZJLm4v5fIdbTzdvysOv8ADyaAKeiaLpuo6B4NvPGlzDF4XitWtrSHe4M11JI+d7LjYAADkkDgc9RW/wCKfBPhnwh418DNoemfZJrrVV3sJpHDKu3j5mOOvaui8B6BY+KPgdpmkahGGt7i3cE4yUPmNhl9CDyKyfEGmSaX4j+F+i3eoNqFzaXMm+Zl2u4UKVJXJwBjHXtQB0XxF8TappV94a0jQ5li1LU9QVGJUOBAOHyCOnzDn2OKofE//kd/h3/2Fj/OOtGz8H6jcfFa78V6xLA9tbwCDS4omJ2A53FgehwT9d/tWX8TZA/jv4dxqQZBqbPtB52jZk49KALmva/qnh74s6HDdXbN4f1e3a1SHaAsVwCCD6kk7QMn+I+lXvGfhG+1LVtO8R6K1tJrOmKyW1vfA/Z3V8q+/HzcAkjB64p3xO8K3XivwqsOm+Wuq2lxHc2buQuHBwee3ykn6gVuavajUtBm0281E2U1xB5bz28mx0buUJoA8s8ReI/iN4Y/s7+0NG8Gn7fdLaQ+TDM37xumfmGBWrc+F/G/iuA6L4nsfDNro1yR9pn0sSLcqFIYbC+V5ZQDkdM1xPxC8B2+j/8ACP7PF2r3/wBq1SK3P2m8D+SDn519GHrXUeF7GLwx8VV0y51zWr2GWyMlnNdXomhm4+fcuBsI4xk/zFAHa+M/Bza9p2mPZzAX2jyrc2Szf6uWRMbVkwM7TjnGK4XxRr3xH8I6FLrGpaL4Oa1iZVbyoZWb5iAMAuPWvX71oXs5YpbhYFmjZBJvAIyMZH514H8TPAVtoXge7v4/GOs6iyPGBbXd4JI2y4HK+3WgDrzpnxG12wey1DS/CEOnX0XlXD2yyrMsTjDMhJIDBSSMg816RomkW+g6JZaTavI8FpCsMbSkFiqjAzgAZ/AV43Z6TF4S8deFxL4g1+8sbsfLK175kazY4jeMDhcd8/yJr3FHWRdyMGU9wcigB9FFFABRRRQAVk694Z0bxNBBDrNhFeRwSiaNZP4WH07eo6HvWtRQBHDDHbwpDEipGihVRFACgdgB0rDvfBXh++0rUNNl02MWuozfaLpIyUMsmQdxIIPUA10FFAHNz+BvD9zd6ndSWCifUrdLa4dGKkovQKQfl6Dp/dX0qHSfh34W0OaxmsNKjSex8z7PK7M7KXxubJJyeOCenauqxRQBjQ+FtFt9Q1K/isIkutSjEV3IB/rQM9fQnPJHXAzUcfhHRoPCkvhq3tjDpUkbxmJHOQHJLfMec5J5rdoxQByNt8MvBdtaxW//AAjmnyCJAgeWBWdsDGWJHJ461d8O+C9E8K3mo3Oj2v2Y6gyNNGp+Qbd20KvYfMa6HFGKAMa88K6JfeH5tCm023GmSklreNfLXJbdkbcYO7nIrkB8C/AQOf7Mn+hu5P8AGvSMUUAUbDSLDTLO1tLO1iigtE8uBQP9WuMcE8/X171HZaBpenS6hJa2iI2oSma7zlhK54JIJxWlRQBVtdPs7GySytLWCC0RSqwRRhEAPUBRwBya5bw98K/CHhjVU1PTNMZLyMERyyTPJsyMHAJwDjv1rs6KAGSxJNC8Uqho5FKsp6EHgiuK0P4TeDvDmqxanp2mst3Dny3lndwh9QCcZruKKAAVyfjDwZ4X8Rm3u9f0p76SH91FsmkQqGPorDNdZVe7soL6HyrhNyZzgHGfbjseh9RxQB5lL8MvhpDbQynw07NJD5xjW9m3KMdDmTg54p0nwv8AhhHdCD+wGZtquzC8n2qDjHPmc9R0z1HqK9BfQtOfGYBgZwoOFGST06dT+gpbvRre8+9JLGMAN5e0bsY68ewoAxtZ8O+EPFVlZ6dqaW93Db4FvELpkK8YA+VgTx61y158MPhrZ2ZuW8OSOomMJCXs5ORnP/LT2r094laExAlEK7fkO0gYxwR0qpNo1jPaRWrREW8YULGrELx0yO9AHmz/AA5+F6QiQ+H5udxCi6uCcA4J/wBZiu10u90fQ7e20TT7eeKCBfLii2s20YDAZYkk/MBV8+H9OYDdEzNggsZGJOc5zzz94/y6Uo8P6aJlmMBaVRgOZG3Z9c5znjr1oAWPXLR2IO5Ap2s7Y2q2cbcg8kHrjIHc1ctLqO8t1niDbGJA3Lg8Ejp+FVho1h5cMZgDxwf6tHYsq9uhNWre2htY/LgQIm4tgepOT/OgD//Z\">35161<br>', 1);
INSERT INTO `message` VALUES (227, 2, 6, '2020-03-14 21:16:31', '玩个', 1);
INSERT INTO `message` VALUES (228, 2, 6, '2020-03-14 21:11:54', '违法', 1);
INSERT INTO `message` VALUES (229, 2, 6, '2020-03-14 21:05:48', '人格 ', 1);
INSERT INTO `message` VALUES (230, 2, 6, '2020-03-14 17:37:09', 'iwej', 1);
INSERT INTO `message` VALUES (231, 2, 6, '2020-03-14 17:36:38', '23', 1);
INSERT INTO `message` VALUES (232, 2, 6, '2020-03-14 17:36:31', '33', 1);
INSERT INTO `message` VALUES (233, 2, 6, '2020-03-14 17:35:52', '234', 1);
INSERT INTO `message` VALUES (234, 2, 6, '2020-03-14 17:35:21', '33', 1);
INSERT INTO `message` VALUES (235, 2, 6, '2020-03-14 17:33:55', '热', 1);
INSERT INTO `message` VALUES (236, 2, 6, '2020-03-14 17:33:29', '热火', 1);
INSERT INTO `message` VALUES (237, 2, 6, '2020-03-14 17:32:55', '而', 1);
INSERT INTO `message` VALUES (238, 2, 6, '2020-03-14 17:32:44', '二哥', 1);
INSERT INTO `message` VALUES (239, 2, 6, '2020-03-14 17:32:07', '额', 1);
INSERT INTO `message` VALUES (240, 2, 6, '2020-03-14 17:31:39', '违法违规', 1);
INSERT INTO `message` VALUES (241, 2, 6, '2020-03-14 17:30:44', '让他', 1);
INSERT INTO `message` VALUES (242, 2, 6, '2020-03-14 17:30:03', '二哥', 1);
INSERT INTO `message` VALUES (243, 2, 6, '2020-03-14 17:29:09', '让他', 1);
INSERT INTO `message` VALUES (244, 2, 6, '2020-03-14 17:25:02', '违规', 1);
INSERT INTO `message` VALUES (245, 2, 6, '2020-03-14 17:23:57', '我如果', 1);
INSERT INTO `message` VALUES (246, 2, 6, '2020-03-14 17:22:58', '特容', 1);
INSERT INTO `message` VALUES (247, 2, 6, '2020-03-14 17:22:16', '二哥', 1);
INSERT INTO `message` VALUES (248, 2, 6, '2020-03-14 17:21:16', '他如果', 1);
INSERT INTO `message` VALUES (249, 2, 6, '2020-03-14 17:19:44', '违法', 1);
INSERT INTO `message` VALUES (250, 2, 6, '2020-03-14 17:18:37', '而', 1);
INSERT INTO `message` VALUES (251, 2, 6, '2020-03-14 17:17:55', '若给', 1);
INSERT INTO `message` VALUES (252, 2, 6, '2020-03-14 17:17:12', '二哥', 1);
INSERT INTO `message` VALUES (253, 2, 6, '2020-03-14 17:16:13', '为人父', 1);
INSERT INTO `message` VALUES (254, 2, 6, '2020-03-14 17:14:09', '二哥', 1);
INSERT INTO `message` VALUES (255, 2, 6, '2020-03-14 17:12:01', '如果', 1);
INSERT INTO `message` VALUES (256, 2, 6, '2020-03-14 16:53:23', '绕太阳', 1);
INSERT INTO `message` VALUES (257, 2, 6, '2020-03-14 16:51:35', '二哥', 1);
INSERT INTO `message` VALUES (258, 2, 6, '2020-03-14 16:50:27', '二v沟通', 1);
INSERT INTO `message` VALUES (259, 2, 6, '2020-03-14 16:48:46', '如果', 1);
INSERT INTO `message` VALUES (260, 2, 6, '2020-03-14 16:08:00', '咋不理我<br>', 1);
INSERT INTO `message` VALUES (261, 2, 6, '2020-03-14 16:07:48', '人呢<br>', 1);
INSERT INTO `message` VALUES (262, 2, 6, '2020-03-14 15:58:54', '你在干啥呢<br>', 1);
INSERT INTO `message` VALUES (263, 2, 6, '2020-03-14 15:58:42', '噢 知道了<br>', 1);
INSERT INTO `message` VALUES (264, 6, 2, '2020-03-14 15:58:37', '啥玩意<br>', 1);
INSERT INTO `message` VALUES (265, 2, 6, '2020-03-14 15:57:37', '违法', 1);
INSERT INTO `message` VALUES (266, 6, 2, '2020-03-14 15:42:17', '为', 1);
INSERT INTO `message` VALUES (267, 2, 6, '2020-03-14 15:42:12', '二哥', 1);
INSERT INTO `message` VALUES (268, 6, 2, '2020-03-13 15:46:50', '为人父', 1);
INSERT INTO `message` VALUES (269, 6, 2, '2020-03-13 15:45:45', '热吻', 1);
INSERT INTO `message` VALUES (270, 6, 2, '2020-03-13 15:45:29', '微服务过 ', 1);
INSERT INTO `message` VALUES (271, 6, 2, '2020-03-13 15:45:22', '玩个', 1);
INSERT INTO `message` VALUES (272, 6, 2, '2020-03-13 15:44:53', '二哥', 1);
INSERT INTO `message` VALUES (273, 6, 2, '2020-03-13 15:44:45', '234', 1);
INSERT INTO `message` VALUES (274, 2, 6, '2020-03-13 15:32:22', '方法', 1);
INSERT INTO `message` VALUES (275, 2, 6, '2020-03-13 15:24:42', '22', 1);
INSERT INTO `message` VALUES (276, 2, 6, '2020-03-13 15:18:12', '士大夫', 1);
INSERT INTO `message` VALUES (277, 2, 6, '2020-03-13 15:11:15', '为', 1);
INSERT INTO `message` VALUES (278, 6, 8, '2020-03-25 13:37:47', '456465', 1);
INSERT INTO `message` VALUES (279, 6, 8, '2020-03-25 13:37:44', '789', 1);
INSERT INTO `message` VALUES (280, 6, 8, '2020-03-25 13:32:50', '321', 1);
INSERT INTO `message` VALUES (281, 6, 8, '2020-03-25 13:12:34', '123', 1);
INSERT INTO `message` VALUES (282, 6, 8, '2020-03-25 12:43:07', 'w3rwt', 1);
INSERT INTO `message` VALUES (283, 6, 8, '2020-03-25 12:41:18', '3611', 1);
INSERT INTO `message` VALUES (284, 6, 8, '2020-03-25 12:36:52', '2324234', 1);
INSERT INTO `message` VALUES (285, 6, 8, '2020-03-25 12:36:52', '2324234', 1);
INSERT INTO `message` VALUES (286, 6, 2, '2020-03-25 16:36:57', 'l;mop<br>', 1);
INSERT INTO `message` VALUES (287, 2, 6, '2020-03-25 14:40:25', '324<br>', 1);
INSERT INTO `message` VALUES (288, 6, 2, '2020-03-25 14:40:02', '3<br>', 1);
INSERT INTO `message` VALUES (289, 2, 6, '2020-03-25 14:39:16', '嗨<br>', 1);
INSERT INTO `message` VALUES (290, 2, 6, '2020-03-25 14:39:13', '好咯<br>', 1);
INSERT INTO `message` VALUES (291, 2, 6, '2020-03-25 14:33:17', '34234', 1);
INSERT INTO `message` VALUES (292, 2, 6, '2020-03-25 14:27:08', '234332额<br>', 1);
INSERT INTO `message` VALUES (293, 2, 6, '2020-03-25 14:26:26', '456', 1);
INSERT INTO `message` VALUES (294, 2, 6, '2020-03-25 14:24:54', '333', 1);
INSERT INTO `message` VALUES (295, 2, 6, '2020-03-25 14:22:55', '人呢<br>', 1);
INSERT INTO `message` VALUES (296, 2, 6, '2020-03-25 14:19:53', '？<br>', 1);
INSERT INTO `message` VALUES (297, 2, 6, '2020-03-25 14:17:35', '在吗？<br>', 1);
INSERT INTO `message` VALUES (298, 6, 2, '2020-03-21 20:34:56', '<img src=\"/files/chat/2929a4fc12312.jpg?id=41\">', 2);
INSERT INTO `message` VALUES (299, 6, 2, '2020-03-21 19:21:34', '<img src=\"/files/chat/73dff5da0cc7124ccabe84d24741b86dc227d634.jpg?id=36\">546<img src=\"/files/chat/1551beb612312.jpg?id=37\">', 2);
INSERT INTO `message` VALUES (300, 6, 2, '2020-03-18 19:54:32', '<img src=\"/files/chat/49d5f43d2.png?31\">', 2);
INSERT INTO `message` VALUES (301, 6, 2, '2020-03-18 19:42:36', '<img src=\"/files/chat/79cc04ef12312.jpg?30\">', 2);
INSERT INTO `message` VALUES (302, 6, 2, '2020-05-15 21:29:22', '23123', 1);
INSERT INTO `message` VALUES (303, 2, 6, '2020-05-15 21:28:31', '232', 1);
INSERT INTO `message` VALUES (307, 6, 15, '2020-03-25 16:36:45', 'kuh<br>', 1);

-- ----------------------------
-- Table structure for permission
-- ----------------------------
DROP TABLE IF EXISTS `permission`;
CREATE TABLE `permission`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) NULL DEFAULT NULL COMMENT '父权限',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限名称',
  `en_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '权限英文名称',
  `url` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '授权路径',
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `created` datetime(0) NOT NULL COMMENT '创建时间',
  `updated` datetime(0) NOT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 104 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of permission
-- ----------------------------
INSERT INTO `permission` VALUES (37, 0, '总权限', 'all', '/', '总权限', '2019-02-15 23:22:54', '2019-04-04 23:22:56');
INSERT INTO `permission` VALUES (51, 37, '刷新权限', 'refreshPermissions', '/refreshPermissions', '权限修改后，刷新后才生效', '2020-02-21 14:04:28', '2020-02-21 14:04:30');
INSERT INTO `permission` VALUES (52, 37, '签到总权限', 'clock', '/clock', '签到管理总权限', '2020-03-29 13:19:56', '2020-03-29 13:19:57');
INSERT INTO `permission` VALUES (53, 52, '获取签到时间', 'clockGetTile', '/clock/getTime', '获取签到时间', '2020-03-29 13:21:09', '2020-03-29 13:21:11');
INSERT INTO `permission` VALUES (54, 52, '签到', 'clockClockIn', '/clock/clockIn', '签到', '2020-03-29 13:21:57', '2020-03-29 13:21:59');
INSERT INTO `permission` VALUES (55, 52, '统计打卡情况', 'clockStatisticsAll', '/clock/statistics/all', '统计所有人打卡情况', '2020-03-29 13:23:36', '2020-03-29 13:23:38');
INSERT INTO `permission` VALUES (56, 52, '修改签到时间', 'clockSetTime', '/clock/setTime', '修改签到时间', '2020-03-29 13:24:38', '2020-03-29 13:24:40');
INSERT INTO `permission` VALUES (57, 37, '部门管理', 'department', '/department', '部门管理', '2020-03-29 13:36:33', '2020-03-29 13:36:35');
INSERT INTO `permission` VALUES (58, 57, '查询所有部门', 'departmentSelectAll', '/department/select/all', '获取所有部门信息', '2020-03-29 13:35:06', '2020-03-29 13:35:08');
INSERT INTO `permission` VALUES (59, 57, '删除部门', 'departmentDelete', '/department/delete', '删除部门', '2020-03-29 13:37:42', '2020-03-29 13:37:44');
INSERT INTO `permission` VALUES (60, 57, '添加部门', 'departmentInsert', '/department/insert', '添加部门', '2020-03-29 13:38:17', '2020-03-29 13:38:20');
INSERT INTO `permission` VALUES (61, 57, '修改部门', 'departmentUpdate', '/department/update', '修改部门信息', '2020-03-29 13:38:56', '2020-03-29 13:38:58');
INSERT INTO `permission` VALUES (62, 57, '获取子部门', 'departmentGetChild', '/department/getChild', '获取子部门', '2020-03-29 13:39:59', '2020-03-29 13:40:02');
INSERT INTO `permission` VALUES (63, 37, '文件管理', 'file', '/file', '文件管理', '2020-03-29 13:40:54', '2020-03-29 13:40:56');
INSERT INTO `permission` VALUES (64, 63, '上传文件', 'fileUpload', '/file/upload', '上传文件', '2020-03-29 13:41:39', '2020-03-29 13:41:41');
INSERT INTO `permission` VALUES (65, 63, '下载文件', 'fileDownload', '/file/download', '下载文件', '2020-03-29 13:42:55', '2020-03-29 13:42:57');
INSERT INTO `permission` VALUES (66, 37, '消息管理', 'message', '/message', '消息管理', '2020-03-29 14:45:57', '2020-03-29 14:46:00');
INSERT INTO `permission` VALUES (67, 66, '消息统计', 'messageStatistics', '/message/statistics', '消息统计', '2020-03-29 14:46:42', '2020-03-29 14:46:44');
INSERT INTO `permission` VALUES (68, 66, '获取未读消息', 'messageGetUnreadMessage', '/message/getUnreadMessage', '获取未读消息', '2020-03-29 14:47:43', '2020-03-29 14:47:45');
INSERT INTO `permission` VALUES (69, 37, '权限管理', 'permission', '/permission', '权限管理', '2020-03-29 14:49:19', '2020-03-29 14:49:24');
INSERT INTO `permission` VALUES (70, 69, '获取所有权限', 'permissionGetPermission', '/permission/getPermission', '获取所有权限', '2020-03-29 14:50:14', '2020-03-29 14:50:16');
INSERT INTO `permission` VALUES (71, 69, '刷新权限', 'permissionRefreshPermissions', '/permission/refreshPermissions', '刷新权限', '2020-03-29 14:51:14', '2020-03-29 14:51:17');
INSERT INTO `permission` VALUES (72, 69, '获取角色权限信息', 'permissionGetRolePermission', '/permission/getRolePermission', '获取角色权限信息', '2020-03-29 14:52:53', '2020-03-29 14:52:55');
INSERT INTO `permission` VALUES (73, 37, '角色管理', 'role', '/role', '角色管理', '2020-03-29 14:57:40', '2020-03-29 14:57:42');
INSERT INTO `permission` VALUES (74, 73, '获取所有子角色', 'roleGetChild', '/role/getChild', '获取所有子角色（不包含自己）', '2020-03-29 14:59:06', '2020-03-29 14:59:09');
INSERT INTO `permission` VALUES (75, 73, '修改角色', 'roleUpdate', '/role/update', '修改角色', '2020-03-29 15:00:53', '2020-03-29 15:00:55');
INSERT INTO `permission` VALUES (76, 37, '用户管理', 'user', '/user', '用户管理', '2020-03-29 15:05:15', '2020-03-29 15:05:17');
INSERT INTO `permission` VALUES (77, 76, '获取信息(token)', 'userInfo', '/user/info', '获取信息', '2020-03-29 15:06:03', '2020-03-29 15:06:05');
INSERT INTO `permission` VALUES (78, 76, '不分页查询', 'userNoPage', '/user/select/noPage', '不分页查询', '2020-03-29 15:06:52', '2020-03-29 15:06:54');
INSERT INTO `permission` VALUES (79, 76, '分页查询', 'userPage', '/user/select/page', '分页查询', '2020-03-29 15:07:29', '2020-03-29 15:07:31');
INSERT INTO `permission` VALUES (80, 76, '根据部门查询', 'userSelectDetailsBypartment', '/user/select/detailsBypartment', '根据部门查询', '2020-03-29 15:08:50', '2020-03-29 15:08:52');
INSERT INTO `permission` VALUES (81, 76, '获取id获取详细信息', 'userSelectDetails', '/user/select/details', '获取详细信息', '2020-03-29 15:11:07', '2020-03-29 15:11:09');
INSERT INTO `permission` VALUES (82, 76, '删除用户', 'userDelete', '/user/delete', '删除用户', '2020-03-29 15:11:58', '2020-03-29 15:12:00');
INSERT INTO `permission` VALUES (83, 76, '修改信息(个人)', 'userUpdate', '/user/update', '修改个人信息', '2020-03-29 15:12:51', '2020-03-29 15:12:54');
INSERT INTO `permission` VALUES (84, 76, '统计登录次数', 'userStatistics', '/user/statistics', '统计登录次数', '2020-03-29 15:13:38', '2020-03-29 15:13:40');
INSERT INTO `permission` VALUES (85, 76, '添加用户', 'userInsert', '/user/insert', '添加用户', '2020-03-29 15:14:55', '2020-03-29 15:14:57');
INSERT INTO `permission` VALUES (86, 76, '修改用户角色', 'userUpdateRole', '/user/updateRole', '修改用户角色', '2020-03-29 15:17:33', '2020-03-29 15:17:35');
INSERT INTO `permission` VALUES (87, 76, '完善个人信息', 'userComplete', '/user/complete', '第一次登陆完善信息', '2020-03-29 15:19:18', '2020-03-29 15:19:20');
INSERT INTO `permission` VALUES (88, 76, '批量添加', 'userInsertFile', '/user/insertFile', '批量添加', '2020-03-29 15:20:40', '2020-03-29 15:20:42');
INSERT INTO `permission` VALUES (89, 37, '工单管理', 'work', '/work', '工单管理', '2020-03-29 15:21:35', '2020-03-29 15:21:38');
INSERT INTO `permission` VALUES (90, 89, '查询需要处理的工单', 'workSelectTo', '/work/select/to', '查询需要自己处理的订单', '2020-03-29 15:22:32', '2020-03-29 15:22:34');
INSERT INTO `permission` VALUES (91, 89, '查询提交的工单', 'workSelectForm', '/work/select/from', '查询提交的工单', '2020-03-29 15:23:26', '2020-03-29 15:23:27');
INSERT INTO `permission` VALUES (92, 89, '工单详情', 'work', '/work/details', '工单详情', '2020-03-29 15:24:13', '2020-03-29 15:24:15');
INSERT INTO `permission` VALUES (93, 89, '获取附件', 'workSelectFiles', '/work/select/files', '获取附件', '2020-03-29 15:25:12', '2020-03-29 15:25:15');
INSERT INTO `permission` VALUES (94, 89, '处理工单', ' workDispose', '/work/dispose', '处理工单', '2020-03-29 15:26:18', '2020-03-29 15:26:20');
INSERT INTO `permission` VALUES (95, 89, '转接工单', 'workSwitchOver', '/work/switchOver', '转接工单', '2020-03-29 15:27:01', '2020-03-29 15:27:02');
INSERT INTO `permission` VALUES (96, 89, '完结工单', 'workEnd', '/work/endd', '完结工单', '2020-03-29 15:27:35', '2020-03-29 15:27:37');
INSERT INTO `permission` VALUES (97, 89, '新建工单', 'workInsert', '/work/insert', '新建工单', '2020-03-29 15:28:06', '2020-03-29 15:28:08');
INSERT INTO `permission` VALUES (98, 89, '获取工单统计信息', 'workStatistics', '/work/statistics', '获取工单统计信息', '2020-03-29 15:29:14', '2020-03-29 15:29:15');
INSERT INTO `permission` VALUES (99, 89, '获取工单类型统计信息', 'workStatisticsType', '/work/statisticsType', '获取工单类型统计信息', '2020-03-29 15:30:05', '2020-03-29 15:30:07');
INSERT INTO `permission` VALUES (100, 52, '获取自己的签到记录', 'clockStatisticsOne', '/clock/statistics/one', '获取签到记录', '2020-03-29 18:30:03', '2020-03-29 18:30:05');
INSERT INTO `permission` VALUES (101, 89, '查看未完成工单数量', 'workGetCount', '/work/getCount', '查看未完成工单数量', '2020-04-01 16:02:53', '2020-04-01 16:02:55');
INSERT INTO `permission` VALUES (102, 89, '查询所有工单', 'workSelectAll', '/work/select/all', '查询所有工单', '2020-04-08 12:38:26', '2020-04-08 12:38:28');
INSERT INTO `permission` VALUES (103, 76, '修改个人密码', 'userUpdatePwd', '/user/updatePwd', '修改个人密码', '2020-04-08 19:18:01', '2020-04-08 19:18:03');

-- ----------------------------
-- Table structure for role
-- ----------------------------
DROP TABLE IF EXISTS `role`;
CREATE TABLE `role`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `parent_id` int(20) NULL DEFAULT NULL COMMENT '父角色',
  `name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色名称',
  `en_name` varchar(64) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL COMMENT '角色英文名称',
  `description` varchar(200) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '备注',
  `created` datetime(0) NOT NULL COMMENT '创建时间',
  `updated` datetime(0) NOT NULL COMMENT '最后修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 55 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role
-- ----------------------------
INSERT INTO `role` VALUES (37, 0, '超级管理员', 'admin', NULL, '2019-04-04 23:22:03', '2020-04-08 19:19:24');
INSERT INTO `role` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, '2020-03-29 15:40:00', '2020-04-08 19:19:24');
INSERT INTO `role` VALUES (52, 37, '普通管理员', 'systemManager', NULL, '2020-03-29 18:43:28', '2020-04-08 19:19:24');
INSERT INTO `role` VALUES (53, 51, '123', '123', NULL, '2020-06-28 11:23:55', '2020-06-28 11:23:57');
INSERT INTO `role` VALUES (54, 222, '453', '345', NULL, '2020-06-28 13:49:34', '2020-06-28 13:49:36');

-- ----------------------------
-- Table structure for role_permission
-- ----------------------------
DROP TABLE IF EXISTS `role_permission`;
CREATE TABLE `role_permission`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `role_id` int(20) NOT NULL COMMENT '角色 ID',
  `permission_id` int(20) NOT NULL COMMENT '权限 ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  INDEX `permission_id`(`permission_id`) USING BTREE,
  CONSTRAINT `role_permission_ibfk_1` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `role_permission_ibfk_2` FOREIGN KEY (`permission_id`) REFERENCES `permission` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 314 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '角色权限表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of role_permission
-- ----------------------------
INSERT INTO `role_permission` VALUES (285, 37, 37);
INSERT INTO `role_permission` VALUES (286, 51, 54);
INSERT INTO `role_permission` VALUES (287, 51, 100);
INSERT INTO `role_permission` VALUES (288, 51, 58);
INSERT INTO `role_permission` VALUES (289, 51, 62);
INSERT INTO `role_permission` VALUES (290, 51, 63);
INSERT INTO `role_permission` VALUES (291, 51, 68);
INSERT INTO `role_permission` VALUES (292, 51, 70);
INSERT INTO `role_permission` VALUES (293, 51, 72);
INSERT INTO `role_permission` VALUES (294, 51, 74);
INSERT INTO `role_permission` VALUES (295, 51, 77);
INSERT INTO `role_permission` VALUES (296, 51, 78);
INSERT INTO `role_permission` VALUES (297, 51, 79);
INSERT INTO `role_permission` VALUES (298, 51, 80);
INSERT INTO `role_permission` VALUES (299, 51, 81);
INSERT INTO `role_permission` VALUES (300, 51, 83);
INSERT INTO `role_permission` VALUES (301, 51, 87);
INSERT INTO `role_permission` VALUES (302, 51, 90);
INSERT INTO `role_permission` VALUES (303, 51, 91);
INSERT INTO `role_permission` VALUES (304, 51, 92);
INSERT INTO `role_permission` VALUES (305, 51, 93);
INSERT INTO `role_permission` VALUES (306, 51, 94);
INSERT INTO `role_permission` VALUES (307, 51, 95);
INSERT INTO `role_permission` VALUES (308, 51, 96);
INSERT INTO `role_permission` VALUES (309, 51, 97);
INSERT INTO `role_permission` VALUES (310, 51, 53);
INSERT INTO `role_permission` VALUES (311, 51, 101);
INSERT INTO `role_permission` VALUES (312, 51, 103);
INSERT INTO `role_permission` VALUES (313, 52, 52);

-- ----------------------------
-- Table structure for security
-- ----------------------------
DROP TABLE IF EXISTS `security`;
CREATE TABLE `security`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `issue` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '问题',
  `answers` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '答案',
  `user_id` int(11) NULL DEFAULT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `security_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin COMMENT = '密保问题' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of security
-- ----------------------------
INSERT INTO `security` VALUES (1, '问题三', '你爸爸', 6);
INSERT INTO `security` VALUES (2, '问题四', '123124', 2);
INSERT INTO `security` VALUES (3, '问题三', '1111', 7);
INSERT INTO `security` VALUES (4, '问题三', '2424123', 15);

-- ----------------------------
-- Table structure for user
-- ----------------------------
DROP TABLE IF EXISTS `user`;
CREATE TABLE `user`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `principal` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '账号',
  `credentials` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '密码',
  `nickname` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '昵称',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `update_time` datetime(0) NOT NULL COMMENT '最后登录时间',
  `login_number` int(11) NOT NULL DEFAULT 0 COMMENT '登录次数',
  `tel` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '手机',
  `mail` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '邮箱',
  `is_admin` tinyint(2) NOT NULL DEFAULT 0 COMMENT '是否为管理员(0否，1是)',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '头像',
  `department_id` int(11) NULL DEFAULT NULL COMMENT '部门id',
  `sex` varchar(5) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL DEFAULT '男' COMMENT '性别',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`principal`) USING BTREE,
  INDEX `department_id`(`department_id`) USING BTREE,
  CONSTRAINT `user_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `department` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 23 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user
-- ----------------------------
INSERT INTO `user` VALUES (2, 'admin1', '$2a$10$Nxq1jhiGZjTVWszeQrXBFO19P3wvTLz2S57/CzpxKspyeHernVYn6', '王二', '2020-03-11 17:37:14', '2020-05-15 21:28:22', 17, '13462031301', '446626585@qq.com', 0, '/files/avatar/9cccb9fbmmexport1570467632958.jpg?id=51', 56, '男');
INSERT INTO `user` VALUES (6, 'admin', '$2a$10$4eGh6AvicfyLimw73kzpBO1bjldH.NMXHIY7PKpPyshJ2UuevIDdu', '超级管理员', '2020-03-08 17:03:13', '2020-06-26 21:58:37', 45, '17613672817', '1806540582@qq.com', 1, '/files/avatar/b33074e70cc7124ccabe84d24741b86dc227d634.jpg?id=52', 53, '男');
INSERT INTO `user` VALUES (7, 'admin2', '$2a$10$xoN6XHlP7l00LO31.CwUmOEI/42aRQY82kO5d1rJCWdMf7ki3SAja', '张三', '2020-03-22 10:20:44', '2020-03-25 17:01:44', 1, '13271327026', '446626585@qq.com', 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 56, '男');
INSERT INTO `user` VALUES (8, 'lianeco', '$2a$10$Zfm8qrfu7Mpo0pnsOTyFK.zpawcqX6P1yRPeHOzgLgSnrSOvbpeVW', 'lianeco', '2020-03-23 17:48:10', '2020-03-23 17:48:10', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男');
INSERT INTO `user` VALUES (10, 'sjndkjn', '$2a$10$eKgs1ufEPABWu0tSsZ/AA.q639sftOLWt1TBiwLj.JebTeBgs5YIm', 'sjndkjn', '2020-03-23 20:43:54', '2020-03-23 20:43:54', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', NULL, '男');
INSERT INTO `user` VALUES (11, '12312', '$2a$10$AOsUzh/YifcqZP0fhhP.IOiuk/JRu4.G4Xa9tQhgHvGMsYvWfmplG', '12312', '2020-03-23 20:47:16', '2020-03-23 20:47:16', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', NULL, '男');
INSERT INTO `user` VALUES (12, '123325', '$2a$10$8TZyEr6rcje7/.vm3yB8ieprEqYbmKXJVSvydYeQ8Uc9x9JwpWSPe', '123325', '2020-03-23 20:48:54', '2020-03-23 20:48:54', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', NULL, '男');
INSERT INTO `user` VALUES (13, '23432', '$2a$10$LOO0ZnPMuLJvtu/L16UHke/Z/b.R0CGcXcivwPUUrN3nFVvc6fA0m', '23432', '2020-03-23 20:49:39', '2020-03-23 20:49:39', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 59, '男');
INSERT INTO `user` VALUES (14, 'sdfw', '$2a$10$dxGz8UKDxP2FK8fOHLulwuDn3xnlP/t0319VJpPmtTqepcqiHySAq', 'sdfw', '2020-03-23 20:50:48', '2020-03-23 20:50:48', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 58, '男');
INSERT INTO `user` VALUES (15, '123123', '$2a$10$eBCAHu3xW.Id3ahzk/RiKuETINGwbg2xNi6TDdSUraH7QN2hgaF0W', '我的神呐', '2020-05-15 22:05:36', '2020-05-15 22:14:22', 3, '13271327026', '446626585@11.com', 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男');
INSERT INTO `user` VALUES (18, '35151', '$2a$10$A4LrI8DtkmpoQqru.ehy8.3.9ZW3XcKto2xpK0G.sUmJb8KH1MV52', '35151', '2020-03-27 17:54:12', '2020-03-27 17:54:12', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 54, '男');
INSERT INTO `user` VALUES (19, '351351', '$2a$10$61DLj1q4A5vB3C1JrIA3xuAhg5/DhIK/wQwnzLKGms60zenkERL3m', '王五', '2020-03-27 17:56:56', '2020-03-27 17:56:56', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 54, '男');
INSERT INTO `user` VALUES (20, '032817510', '123456', '张三', '2020-03-28 17:51:12', '2020-03-28 17:51:12', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男');
INSERT INTO `user` VALUES (21, '032819320', '123456', '张三', '2020-03-28 19:32:31', '2020-03-28 19:32:31', 0, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男');
INSERT INTO `user` VALUES (22, '041011150', '$2a$10$Nxq1jhiGZjTVWszeQrXBFO19P3wvTLz2S57/CzpxKspyeHernVYn6', '你爸爸的爸爸', '2020-04-10 11:15:30', '2020-05-15 22:05:01', 1, NULL, NULL, 0, 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男');

-- ----------------------------
-- Table structure for user_role
-- ----------------------------
DROP TABLE IF EXISTS `user_role`;
CREATE TABLE `user_role`  (
  `id` int(20) NOT NULL AUTO_INCREMENT,
  `user_id` int(20) NOT NULL COMMENT '用户 ID',
  `role_id` int(20) NOT NULL COMMENT '角色 ID',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  INDEX `role_id`(`role_id`) USING BTREE,
  CONSTRAINT `user_role_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `user_role_ibfk_2` FOREIGN KEY (`role_id`) REFERENCES `role` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 67 CHARACTER SET = utf8 COLLATE = utf8_general_ci COMMENT = '用户角色表' ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of user_role
-- ----------------------------
INSERT INTO `user_role` VALUES (38, 6, 37);
INSERT INTO `user_role` VALUES (62, 2, 51);
INSERT INTO `user_role` VALUES (63, 22, 51);
INSERT INTO `user_role` VALUES (65, 15, 52);
INSERT INTO `user_role` VALUES (66, 15, 51);

-- ----------------------------
-- Table structure for work_logs_files
-- ----------------------------
DROP TABLE IF EXISTS `work_logs_files`;
CREATE TABLE `work_logs_files`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `files_id` int(11) NULL DEFAULT NULL COMMENT '文件id',
  `work_logs_id` int(11) NULL DEFAULT NULL COMMENT '工单记录id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `files_id`(`files_id`) USING BTREE,
  INDEX `work_order_id`(`work_logs_id`) USING BTREE,
  CONSTRAINT `work_logs_files_ibfk_1` FOREIGN KEY (`files_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `work_logs_files_ibfk_2` FOREIGN KEY (`work_logs_id`) REFERENCES `work_order_logs` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_logs_files
-- ----------------------------

-- ----------------------------
-- Table structure for work_order
-- ----------------------------
DROP TABLE IF EXISTS `work_order`;
CREATE TABLE `work_order`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL COMMENT '用户id',
  `title` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '标题',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '内容',
  `create_time` datetime(0) NOT NULL COMMENT '创建时间',
  `status` int(255) NOT NULL COMMENT '状态',
  `type` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NOT NULL COMMENT '类型',
  `emergency_level` int(5) NOT NULL DEFAULT 1 COMMENT '紧急级别（1，2，3，4）',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `work_order_ibfk_1`(`user_id`) USING BTREE,
  CONSTRAINT `work_order_ibfk_1` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 12 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_order
-- ----------------------------
INSERT INTO `work_order` VALUES (2, 6, '无可反驳', '<p>为佛教皮肤飞机迫降</p>', '2020-03-22 09:56:45', 0, '薪资', 3);
INSERT INTO `work_order` VALUES (3, 6, '2312', '<p>324</p>', '2020-03-22 14:26:52', 1, '工作', 2);
INSERT INTO `work_order` VALUES (4, 6, '123123', '<p>234234</p>', '2020-03-22 14:27:22', 0, '人事', 2);
INSERT INTO `work_order` VALUES (5, 6, '23', '<p>234234</p>', '2020-03-22 14:27:34', 0, '损耗', 2);
INSERT INTO `work_order` VALUES (6, 6, '22', '<p>33</p>', '2020-03-22 14:49:11', 0, '其他', 1);
INSERT INTO `work_order` VALUES (7, 6, '343', '<p>453245</p>', '2020-03-22 14:49:23', 0, '工作', 2);
INSERT INTO `work_order` VALUES (8, 2, '1', '<p>1</p>', '2020-03-25 16:56:55', 0, '薪资', 2);
INSERT INTO `work_order` VALUES (9, 6, '789123', '<p>123789</p>', '2020-03-25 17:08:15', 1, '工作', 4);
INSERT INTO `work_order` VALUES (10, 6, '和回复他忽然', '<p>的服务</p>', '2020-03-25 17:08:54', 1, '工作', 4);
INSERT INTO `work_order` VALUES (11, 6, '三月份工资未到账', '<p>三月份工资未到账</p><p><br></p>', '2020-05-14 17:15:59', 0, '薪资', 4);

-- ----------------------------
-- Table structure for work_order_files
-- ----------------------------
DROP TABLE IF EXISTS `work_order_files`;
CREATE TABLE `work_order_files`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `files_id` int(11) NOT NULL COMMENT '文件id',
  `work_order_id` int(11) NULL DEFAULT NULL COMMENT '工单id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `files_id`(`files_id`) USING BTREE,
  INDEX `work_order_id`(`work_order_id`) USING BTREE,
  CONSTRAINT `work_order_files_ibfk_1` FOREIGN KEY (`files_id`) REFERENCES `files` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `work_order_files_ibfk_2` FOREIGN KEY (`work_order_id`) REFERENCES `work_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_order_files
-- ----------------------------
INSERT INTO `work_order_files` VALUES (1, 43, 2);

-- ----------------------------
-- Table structure for work_order_logs
-- ----------------------------
DROP TABLE IF EXISTS `work_order_logs`;
CREATE TABLE `work_order_logs`  (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `work_order_id` int(11) NOT NULL COMMENT '工单id',
  `create_time` datetime(0) NULL DEFAULT NULL COMMENT '创建时间',
  `content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '内容',
  `sketch` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_bin NULL DEFAULT NULL COMMENT '简述',
  `user_id` int(11) NOT NULL COMMENT '用户id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `work_order_id`(`work_order_id`) USING BTREE,
  INDEX `user_id`(`user_id`) USING BTREE,
  CONSTRAINT `work_order_logs_ibfk_1` FOREIGN KEY (`work_order_id`) REFERENCES `work_order` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `work_order_logs_ibfk_2` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB AUTO_INCREMENT = 25 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_bin ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES (1, 2, '2020-03-22 09:56:45', '无', '提交工单给王二', 2);
INSERT INTO `work_order_logs` VALUES (2, 2, '2020-03-22 10:21:57', '<p>没有</p>', '转接工单给张三', 2);
INSERT INTO `work_order_logs` VALUES (3, 2, '2020-03-25 17:03:48', '无', '0000', 7);
INSERT INTO `work_order_logs` VALUES (4, 3, '2020-03-22 14:26:53', '无', '提交工单给王二', 2);
INSERT INTO `work_order_logs` VALUES (5, 3, '2020-03-26 17:27:23', '<p>士大夫</p>', 'u由于v', 2);
INSERT INTO `work_order_logs` VALUES (6, 4, '2020-03-22 14:27:22', '无', '提交工单给超级管理员', 6);
INSERT INTO `work_order_logs` VALUES (7, 4, '2020-03-22 14:27:23', NULL, NULL, 6);
INSERT INTO `work_order_logs` VALUES (8, 5, '2020-03-22 14:27:34', '无', '提交工单给王二', 2);
INSERT INTO `work_order_logs` VALUES (9, 5, '2020-03-22 14:27:35', NULL, NULL, 2);
INSERT INTO `work_order_logs` VALUES (10, 6, '2020-03-22 14:49:11', '无', '提交工单给超级管理员', 6);
INSERT INTO `work_order_logs` VALUES (11, 6, '2020-03-22 14:49:12', NULL, NULL, 6);
INSERT INTO `work_order_logs` VALUES (12, 7, '2020-03-22 14:49:23', '无', '提交工单给超级管理员', 6);
INSERT INTO `work_order_logs` VALUES (13, 7, '2020-03-22 14:49:24', NULL, NULL, 6);
INSERT INTO `work_order_logs` VALUES (14, 8, '2020-03-25 16:56:55', '无', '提交工单给张三', 7);
INSERT INTO `work_order_logs` VALUES (15, 8, '2020-03-25 16:56:56', NULL, NULL, 7);
INSERT INTO `work_order_logs` VALUES (16, 2, '2020-03-25 17:03:50', NULL, NULL, 6);
INSERT INTO `work_order_logs` VALUES (17, 9, '2020-03-25 17:08:15', '无', '提交工单给王二', 2);
INSERT INTO `work_order_logs` VALUES (18, 9, '2020-03-26 17:11:21', '<p>24231</p>', '1231', 2);
INSERT INTO `work_order_logs` VALUES (19, 10, '2020-03-25 17:08:54', '无', '提交工单给张三', 7);
INSERT INTO `work_order_logs` VALUES (20, 10, '2020-03-25 17:09:30', '<p>111</p>', '111', 7);
INSERT INTO `work_order_logs` VALUES (21, 10, '2020-03-25 17:09:31', NULL, NULL, 6);
INSERT INTO `work_order_logs` VALUES (22, 9, '2020-03-26 17:11:22', NULL, NULL, 6);
INSERT INTO `work_order_logs` VALUES (23, 11, '2020-04-08 17:16:00', '无', '提交工单给超级管理员', 6);
INSERT INTO `work_order_logs` VALUES (24, 11, '2020-04-08 17:16:01', NULL, NULL, 6);

-- ----------------------------
-- View structure for message_statistics_view
-- ----------------------------
DROP VIEW IF EXISTS `message_statistics_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `message_statistics_view` AS select cast(`asd`.`time` as date) AS `days`,`asd`.`type` AS `type`,count(0) AS `count` from (select `sfp`.`message`.`id` AS `id`,`sfp`.`message`.`from` AS `from`,`sfp`.`message`.`to` AS `to`,`sfp`.`message`.`time` AS `time`,`sfp`.`message`.`data` AS `data`,`sfp`.`message`.`type` AS `type` from `sfp`.`message` where ((curdate() - interval 7 day) <= cast(`sfp`.`message`.`time` as date))) `asd` group by `days`,`asd`.`type`;

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------

-- ----------------------------
-- View structure for permission_role_view
-- ----------------------------
DROP VIEW IF EXISTS `permission_role_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `permission_role_view` AS select `permission`.`id` AS `permission_id`,`permission`.`parent_id` AS `permission_parent_id`,`role`.`id` AS `role_id`,`role`.`parent_id` AS `role_parent_id`,`role`.`en_name` AS `en_name`,`permission`.`url` AS `url`,`role`.`name` AS `name` from ((`permission` left join `role_permission` on((`permission`.`id` = `role_permission`.`permission_id`))) left join `role` on((`role`.`id` = `role_permission`.`role_id`)));

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES (37, 0, 37, 0, 'admin', '/', '超级管理员');
INSERT INTO `work_order_logs` VALUES (53, 52, 51, 37, 'ordinaryUser', '/clock/getTime', '普通用户');
INSERT INTO `work_order_logs` VALUES (54, 52, 51, 37, 'ordinaryUser', '/clock/clockIn', '普通用户');
INSERT INTO `work_order_logs` VALUES (58, 57, 51, 37, 'ordinaryUser', '/department/select/all', '普通用户');
INSERT INTO `work_order_logs` VALUES (62, 57, 51, 37, 'ordinaryUser', '/department/getChild', '普通用户');
INSERT INTO `work_order_logs` VALUES (63, 37, 51, 37, 'ordinaryUser', '/file', '普通用户');
INSERT INTO `work_order_logs` VALUES (68, 66, 51, 37, 'ordinaryUser', '/message/getUnreadMessage', '普通用户');
INSERT INTO `work_order_logs` VALUES (70, 69, 51, 37, 'ordinaryUser', '/permission/getPermission', '普通用户');
INSERT INTO `work_order_logs` VALUES (72, 69, 51, 37, 'ordinaryUser', '/permission/getRolePermission', '普通用户');
INSERT INTO `work_order_logs` VALUES (74, 73, 51, 37, 'ordinaryUser', '/role/getChild', '普通用户');
INSERT INTO `work_order_logs` VALUES (77, 76, 51, 37, 'ordinaryUser', '/user/info', '普通用户');
INSERT INTO `work_order_logs` VALUES (78, 76, 51, 37, 'ordinaryUser', '/user/select/noPage', '普通用户');
INSERT INTO `work_order_logs` VALUES (79, 76, 51, 37, 'ordinaryUser', '/user/select/page', '普通用户');
INSERT INTO `work_order_logs` VALUES (80, 76, 51, 37, 'ordinaryUser', '/user/select/detailsBypartment', '普通用户');
INSERT INTO `work_order_logs` VALUES (81, 76, 51, 37, 'ordinaryUser', '/user/select/details', '普通用户');
INSERT INTO `work_order_logs` VALUES (83, 76, 51, 37, 'ordinaryUser', '/user/update', '普通用户');
INSERT INTO `work_order_logs` VALUES (87, 76, 51, 37, 'ordinaryUser', '/user/complete', '普通用户');
INSERT INTO `work_order_logs` VALUES (90, 89, 51, 37, 'ordinaryUser', '/work/select/to', '普通用户');
INSERT INTO `work_order_logs` VALUES (91, 89, 51, 37, 'ordinaryUser', '/work/select/from', '普通用户');
INSERT INTO `work_order_logs` VALUES (92, 89, 51, 37, 'ordinaryUser', '/work/details', '普通用户');
INSERT INTO `work_order_logs` VALUES (93, 89, 51, 37, 'ordinaryUser', '/work/select/files', '普通用户');
INSERT INTO `work_order_logs` VALUES (94, 89, 51, 37, 'ordinaryUser', '/work/dispose', '普通用户');
INSERT INTO `work_order_logs` VALUES (95, 89, 51, 37, 'ordinaryUser', '/work/switchOver', '普通用户');
INSERT INTO `work_order_logs` VALUES (96, 89, 51, 37, 'ordinaryUser', '/work/endd', '普通用户');
INSERT INTO `work_order_logs` VALUES (97, 89, 51, 37, 'ordinaryUser', '/work/insert', '普通用户');
INSERT INTO `work_order_logs` VALUES (100, 52, 51, 37, 'ordinaryUser', '/clock/statistics/one', '普通用户');
INSERT INTO `work_order_logs` VALUES (101, 89, 51, 37, 'ordinaryUser', '/work/getCount', '普通用户');
INSERT INTO `work_order_logs` VALUES (103, 76, 51, 37, 'ordinaryUser', '/user/updatePwd', '普通用户');
INSERT INTO `work_order_logs` VALUES (52, 37, 52, 37, 'systemManager', '/clock', '普通管理员');
INSERT INTO `work_order_logs` VALUES (51, 37, NULL, NULL, NULL, '/refreshPermissions', NULL);
INSERT INTO `work_order_logs` VALUES (55, 52, NULL, NULL, NULL, '/clock/statistics/all', NULL);
INSERT INTO `work_order_logs` VALUES (56, 52, NULL, NULL, NULL, '/clock/setTime', NULL);
INSERT INTO `work_order_logs` VALUES (57, 37, NULL, NULL, NULL, '/department', NULL);
INSERT INTO `work_order_logs` VALUES (59, 57, NULL, NULL, NULL, '/department/delete', NULL);
INSERT INTO `work_order_logs` VALUES (60, 57, NULL, NULL, NULL, '/department/insert', NULL);
INSERT INTO `work_order_logs` VALUES (61, 57, NULL, NULL, NULL, '/department/update', NULL);
INSERT INTO `work_order_logs` VALUES (64, 63, NULL, NULL, NULL, '/file/upload', NULL);
INSERT INTO `work_order_logs` VALUES (65, 63, NULL, NULL, NULL, '/file/download', NULL);
INSERT INTO `work_order_logs` VALUES (66, 37, NULL, NULL, NULL, '/message', NULL);
INSERT INTO `work_order_logs` VALUES (67, 66, NULL, NULL, NULL, '/message/statistics', NULL);
INSERT INTO `work_order_logs` VALUES (69, 37, NULL, NULL, NULL, '/permission', NULL);
INSERT INTO `work_order_logs` VALUES (71, 69, NULL, NULL, NULL, '/permission/refreshPermissions', NULL);
INSERT INTO `work_order_logs` VALUES (73, 37, NULL, NULL, NULL, '/role', NULL);
INSERT INTO `work_order_logs` VALUES (75, 73, NULL, NULL, NULL, '/role/update', NULL);
INSERT INTO `work_order_logs` VALUES (76, 37, NULL, NULL, NULL, '/user', NULL);
INSERT INTO `work_order_logs` VALUES (82, 76, NULL, NULL, NULL, '/user/delete', NULL);
INSERT INTO `work_order_logs` VALUES (84, 76, NULL, NULL, NULL, '/user/statistics', NULL);
INSERT INTO `work_order_logs` VALUES (85, 76, NULL, NULL, NULL, '/user/insert', NULL);
INSERT INTO `work_order_logs` VALUES (86, 76, NULL, NULL, NULL, '/user/updateRole', NULL);
INSERT INTO `work_order_logs` VALUES (88, 76, NULL, NULL, NULL, '/user/insertFile', NULL);
INSERT INTO `work_order_logs` VALUES (89, 37, NULL, NULL, NULL, '/work', NULL);
INSERT INTO `work_order_logs` VALUES (98, 89, NULL, NULL, NULL, '/work/statistics', NULL);
INSERT INTO `work_order_logs` VALUES (99, 89, NULL, NULL, NULL, '/work/statisticsType', NULL);
INSERT INTO `work_order_logs` VALUES (102, 89, NULL, NULL, NULL, '/work/select/all', NULL);

-- ----------------------------
-- View structure for role_permission_view
-- ----------------------------
DROP VIEW IF EXISTS `role_permission_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `role_permission_view` AS select `role`.`id` AS `id`,`role`.`parent_id` AS `parent_id`,`role`.`name` AS `name`,`role`.`en_name` AS `en_name`,`role`.`description` AS `description`,`permission`.`id` AS `permission_id`,`permission`.`name` AS `permission_name` from (`role` left join (`role_permission` left join `permission` on((`permission`.`id` = `role_permission`.`permission_id`))) on((`role`.`id` = `role_permission`.`role_id`)));

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES (37, 0, '超级管理员', 'admin', NULL, 37, '总权限');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 54, '签到');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 100, '获取自己的签到记录');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 58, '查询所有部门');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 62, '获取子部门');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 63, '文件管理');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 68, '获取未读消息');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 70, '获取所有权限');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 72, '获取角色权限信息');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 74, '获取所有子角色');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 77, '获取信息(token)');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 78, '不分页查询');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 79, '分页查询');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 80, '根据部门查询');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 81, '获取id获取详细信息');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 83, '修改信息(个人)');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 87, '完善个人信息');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 90, '查询需要处理的工单');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 91, '查询提交的工单');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 92, '工单详情');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 93, '获取附件');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 94, '处理工单');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 95, '转接工单');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 96, '完结工单');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 97, '新建工单');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 53, '获取签到时间');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 101, '查看未完成工单数量');
INSERT INTO `work_order_logs` VALUES (51, 37, '普通用户', 'ordinaryUser', NULL, 103, '修改个人密码');
INSERT INTO `work_order_logs` VALUES (52, 37, '普通管理员', 'systemManager', NULL, 52, '签到总权限');
INSERT INTO `work_order_logs` VALUES (53, 51, '123', '123', NULL, NULL, NULL);
INSERT INTO `work_order_logs` VALUES (54, 222, '453', '345', NULL, NULL, NULL);

-- ----------------------------
-- View structure for user_details
-- ----------------------------
DROP VIEW IF EXISTS `user_details`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `user_details` AS select `department`.`name` AS `department_name`,`user`.`id` AS `id`,`user`.`principal` AS `principal`,`user`.`create_time` AS `create_time`,`user`.`update_time` AS `update_time`,`user`.`login_number` AS `login_number`,`user`.`tel` AS `tel`,`user`.`mail` AS `mail`,`user`.`is_admin` AS `is_admin`,`user`.`nickname` AS `nickname`,`user`.`avatar` AS `avatar`,`user`.`department_id` AS `department_id`,`user`.`sex` AS `sex`,`user`.`credentials` AS `credentials` from (`user` left join `department` on((`user`.`department_id` = `department`.`id`)));

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES ('部门5', 2, 'admin1', '2020-03-11 17:37:14', '2020-05-15 21:28:22', 17, '13462031301', '446626585@qq.com', 0, '王二', '/files/avatar/9cccb9fbmmexport1570467632958.jpg?id=51', 56, '男', '$2a$10$Nxq1jhiGZjTVWszeQrXBFO19P3wvTLz2S57/CzpxKspyeHernVYn6');
INSERT INTO `work_order_logs` VALUES ('顶级部门', 6, 'admin', '2020-03-08 17:03:13', '2020-06-26 21:58:37', 45, '17613672817', '1806540582@qq.com', 1, '超级管理员', '/files/avatar/b33074e70cc7124ccabe84d24741b86dc227d634.jpg?id=52', 53, '男', '$2a$10$4eGh6AvicfyLimw73kzpBO1bjldH.NMXHIY7PKpPyshJ2UuevIDdu');
INSERT INTO `work_order_logs` VALUES ('部门5', 7, 'admin2', '2020-03-22 10:20:44', '2020-03-25 17:01:44', 1, '13271327026', '446626585@qq.com', 0, '张三', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 56, '男', '$2a$10$xoN6XHlP7l00LO31.CwUmOEI/42aRQY82kO5d1rJCWdMf7ki3SAja');
INSERT INTO `work_order_logs` VALUES ('顶级部门', 8, 'lianeco', '2020-03-23 17:48:10', '2020-03-23 17:48:10', 0, NULL, NULL, 0, 'lianeco', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男', '$2a$10$Zfm8qrfu7Mpo0pnsOTyFK.zpawcqX6P1yRPeHOzgLgSnrSOvbpeVW');
INSERT INTO `work_order_logs` VALUES (NULL, 10, 'sjndkjn', '2020-03-23 20:43:54', '2020-03-23 20:43:54', 0, NULL, NULL, 0, 'sjndkjn', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', NULL, '男', '$2a$10$eKgs1ufEPABWu0tSsZ/AA.q639sftOLWt1TBiwLj.JebTeBgs5YIm');
INSERT INTO `work_order_logs` VALUES (NULL, 11, '12312', '2020-03-23 20:47:16', '2020-03-23 20:47:16', 0, NULL, NULL, 0, '12312', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', NULL, '男', '$2a$10$AOsUzh/YifcqZP0fhhP.IOiuk/JRu4.G4Xa9tQhgHvGMsYvWfmplG');
INSERT INTO `work_order_logs` VALUES (NULL, 12, '123325', '2020-03-23 20:48:54', '2020-03-23 20:48:54', 0, NULL, NULL, 0, '123325', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', NULL, '男', '$2a$10$8TZyEr6rcje7/.vm3yB8ieprEqYbmKXJVSvydYeQ8Uc9x9JwpWSPe');
INSERT INTO `work_order_logs` VALUES ('部门8', 13, '23432', '2020-03-23 20:49:39', '2020-03-23 20:49:39', 0, NULL, NULL, 0, '23432', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 59, '男', '$2a$10$LOO0ZnPMuLJvtu/L16UHke/Z/b.R0CGcXcivwPUUrN3nFVvc6fA0m');
INSERT INTO `work_order_logs` VALUES ('部门7', 14, 'sdfw', '2020-03-23 20:50:48', '2020-03-23 20:50:48', 0, NULL, NULL, 0, 'sdfw', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 58, '男', '$2a$10$dxGz8UKDxP2FK8fOHLulwuDn3xnlP/t0319VJpPmtTqepcqiHySAq');
INSERT INTO `work_order_logs` VALUES ('顶级部门', 15, '123123', '2020-05-15 22:05:36', '2020-05-15 22:14:22', 3, '13271327026', '446626585@11.com', 0, '我的神呐', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男', '$2a$10$eBCAHu3xW.Id3ahzk/RiKuETINGwbg2xNi6TDdSUraH7QN2hgaF0W');
INSERT INTO `work_order_logs` VALUES ('部门3', 18, '35151', '2020-03-27 17:54:12', '2020-03-27 17:54:12', 0, NULL, NULL, 0, '35151', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 54, '男', '$2a$10$A4LrI8DtkmpoQqru.ehy8.3.9ZW3XcKto2xpK0G.sUmJb8KH1MV52');
INSERT INTO `work_order_logs` VALUES ('部门3', 19, '351351', '2020-03-27 17:56:56', '2020-03-27 17:56:56', 0, NULL, NULL, 0, '王五', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 54, '男', '$2a$10$61DLj1q4A5vB3C1JrIA3xuAhg5/DhIK/wQwnzLKGms60zenkERL3m');
INSERT INTO `work_order_logs` VALUES ('顶级部门', 20, '032817510', '2020-03-28 17:51:12', '2020-03-28 17:51:12', 0, NULL, NULL, 0, '张三', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男', '123456');
INSERT INTO `work_order_logs` VALUES ('顶级部门', 21, '032819320', '2020-03-28 19:32:31', '2020-03-28 19:32:31', 0, NULL, NULL, 0, '张三', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男', '123456');
INSERT INTO `work_order_logs` VALUES ('顶级部门', 22, '041011150', '2020-04-10 11:15:30', '2020-05-15 22:05:01', 1, NULL, NULL, 0, '你爸爸的爸爸', 'https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif', 53, '男', '$2a$10$Nxq1jhiGZjTVWszeQrXBFO19P3wvTLz2S57/CzpxKspyeHernVYn6');

-- ----------------------------
-- View structure for user_role_view
-- ----------------------------
DROP VIEW IF EXISTS `user_role_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `user_role_view` AS select `role`.`name` AS `name`,`role`.`en_name` AS `en_name`,`user_role`.`user_id` AS `user_id`,`role`.`id` AS `role_id` from (`role` join `user_role` on((`role`.`id` = `user_role`.`role_id`)));

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES ('超级管理员', 'admin', 6, 37);
INSERT INTO `work_order_logs` VALUES ('普通用户', 'ordinaryUser', 2, 51);
INSERT INTO `work_order_logs` VALUES ('普通用户', 'ordinaryUser', 22, 51);
INSERT INTO `work_order_logs` VALUES ('普通管理员', 'systemManager', 15, 52);
INSERT INTO `work_order_logs` VALUES ('普通用户', 'ordinaryUser', 15, 51);

-- ----------------------------
-- View structure for user_simple
-- ----------------------------
DROP VIEW IF EXISTS `user_simple`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `user_simple` AS select `user`.`id` AS `id`,`user`.`principal` AS `principal`,`user`.`credentials` AS `credentials`,`user`.`is_admin` AS `is_admin` from `user`;

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES (2, 'admin1', '$2a$10$Nxq1jhiGZjTVWszeQrXBFO19P3wvTLz2S57/CzpxKspyeHernVYn6', 0);
INSERT INTO `work_order_logs` VALUES (6, 'admin', '$2a$10$4eGh6AvicfyLimw73kzpBO1bjldH.NMXHIY7PKpPyshJ2UuevIDdu', 1);
INSERT INTO `work_order_logs` VALUES (7, 'admin2', '$2a$10$xoN6XHlP7l00LO31.CwUmOEI/42aRQY82kO5d1rJCWdMf7ki3SAja', 0);
INSERT INTO `work_order_logs` VALUES (8, 'lianeco', '$2a$10$Zfm8qrfu7Mpo0pnsOTyFK.zpawcqX6P1yRPeHOzgLgSnrSOvbpeVW', 0);
INSERT INTO `work_order_logs` VALUES (10, 'sjndkjn', '$2a$10$eKgs1ufEPABWu0tSsZ/AA.q639sftOLWt1TBiwLj.JebTeBgs5YIm', 0);
INSERT INTO `work_order_logs` VALUES (11, '12312', '$2a$10$AOsUzh/YifcqZP0fhhP.IOiuk/JRu4.G4Xa9tQhgHvGMsYvWfmplG', 0);
INSERT INTO `work_order_logs` VALUES (12, '123325', '$2a$10$8TZyEr6rcje7/.vm3yB8ieprEqYbmKXJVSvydYeQ8Uc9x9JwpWSPe', 0);
INSERT INTO `work_order_logs` VALUES (13, '23432', '$2a$10$LOO0ZnPMuLJvtu/L16UHke/Z/b.R0CGcXcivwPUUrN3nFVvc6fA0m', 0);
INSERT INTO `work_order_logs` VALUES (14, 'sdfw', '$2a$10$dxGz8UKDxP2FK8fOHLulwuDn3xnlP/t0319VJpPmtTqepcqiHySAq', 0);
INSERT INTO `work_order_logs` VALUES (15, '123123', '$2a$10$eBCAHu3xW.Id3ahzk/RiKuETINGwbg2xNi6TDdSUraH7QN2hgaF0W', 0);
INSERT INTO `work_order_logs` VALUES (18, '35151', '$2a$10$A4LrI8DtkmpoQqru.ehy8.3.9ZW3XcKto2xpK0G.sUmJb8KH1MV52', 0);
INSERT INTO `work_order_logs` VALUES (19, '351351', '$2a$10$61DLj1q4A5vB3C1JrIA3xuAhg5/DhIK/wQwnzLKGms60zenkERL3m', 0);
INSERT INTO `work_order_logs` VALUES (20, '032817510', '123456', 0);
INSERT INTO `work_order_logs` VALUES (21, '032819320', '123456', 0);
INSERT INTO `work_order_logs` VALUES (22, '041011150', '$2a$10$Nxq1jhiGZjTVWszeQrXBFO19P3wvTLz2S57/CzpxKspyeHernVYn6', 0);

-- ----------------------------
-- View structure for work_logs_files_view
-- ----------------------------
DROP VIEW IF EXISTS `work_logs_files_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `work_logs_files_view` AS select `files`.`path` AS `path`,`files`.`file_name` AS `file_name`,`work_logs_files`.`work_logs_id` AS `work_logs_id`,`work_logs_files`.`id` AS `id` from (`files` join `work_logs_files` on((`files`.`id` = `work_logs_files`.`files_id`)));

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------

-- ----------------------------
-- View structure for work_logs_user_view
-- ----------------------------
DROP VIEW IF EXISTS `work_logs_user_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `work_logs_user_view` AS select `work_order_logs`.`id` AS `id`,`work_order_logs`.`work_order_id` AS `work_order_id`,`work_order_logs`.`create_time` AS `create_time`,`work_order_logs`.`content` AS `content`,`work_order_logs`.`sketch` AS `sketch`,`work_order_logs`.`user_id` AS `user_id`,`user`.`nickname` AS `nickname` from (`user` join `work_order_logs` on((`user`.`id` = `work_order_logs`.`user_id`)));

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES (1, 2, '2020-03-22 09:56:45', '无', '提交工单给王二', 2, '王二');
INSERT INTO `work_order_logs` VALUES (2, 2, '2020-03-22 10:21:57', '<p>没有</p>', '转接工单给张三', 2, '王二');
INSERT INTO `work_order_logs` VALUES (3, 2, '2020-03-25 17:03:48', '无', '0000', 7, '张三');
INSERT INTO `work_order_logs` VALUES (4, 3, '2020-03-22 14:26:53', '无', '提交工单给王二', 2, '王二');
INSERT INTO `work_order_logs` VALUES (5, 3, '2020-03-26 17:27:23', '<p>士大夫</p>', 'u由于v', 2, '王二');
INSERT INTO `work_order_logs` VALUES (6, 4, '2020-03-22 14:27:22', '无', '提交工单给超级管理员', 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (7, 4, '2020-03-22 14:27:23', NULL, NULL, 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (8, 5, '2020-03-22 14:27:34', '无', '提交工单给王二', 2, '王二');
INSERT INTO `work_order_logs` VALUES (9, 5, '2020-03-22 14:27:35', NULL, NULL, 2, '王二');
INSERT INTO `work_order_logs` VALUES (10, 6, '2020-03-22 14:49:11', '无', '提交工单给超级管理员', 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (11, 6, '2020-03-22 14:49:12', NULL, NULL, 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (12, 7, '2020-03-22 14:49:23', '无', '提交工单给超级管理员', 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (13, 7, '2020-03-22 14:49:24', NULL, NULL, 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (14, 8, '2020-03-25 16:56:55', '无', '提交工单给张三', 7, '张三');
INSERT INTO `work_order_logs` VALUES (15, 8, '2020-03-25 16:56:56', NULL, NULL, 7, '张三');
INSERT INTO `work_order_logs` VALUES (16, 2, '2020-03-25 17:03:50', NULL, NULL, 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (17, 9, '2020-03-25 17:08:15', '无', '提交工单给王二', 2, '王二');
INSERT INTO `work_order_logs` VALUES (18, 9, '2020-03-26 17:11:21', '<p>24231</p>', '1231', 2, '王二');
INSERT INTO `work_order_logs` VALUES (19, 10, '2020-03-25 17:08:54', '无', '提交工单给张三', 7, '张三');
INSERT INTO `work_order_logs` VALUES (20, 10, '2020-03-25 17:09:30', '<p>111</p>', '111', 7, '张三');
INSERT INTO `work_order_logs` VALUES (21, 10, '2020-03-25 17:09:31', NULL, NULL, 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (22, 9, '2020-03-26 17:11:22', NULL, NULL, 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (23, 11, '2020-04-08 17:16:00', '无', '提交工单给超级管理员', 6, '超级管理员');
INSERT INTO `work_order_logs` VALUES (24, 11, '2020-04-08 17:16:01', NULL, NULL, 6, '超级管理员');

-- ----------------------------
-- View structure for work_order_files_view
-- ----------------------------
DROP VIEW IF EXISTS `work_order_files_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `work_order_files_view` AS select `files`.`path` AS `path`,`files`.`file_name` AS `file_name`,`work_order_files`.`work_order_id` AS `work_order_id`,`files`.`id` AS `id` from (`files` join `work_order_files` on((`files`.`id` = `work_order_files`.`files_id`)));

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES ('/files/2447d0a9consul配置.jpg', 'consul配置.jpg', 2, 43);

-- ----------------------------
-- View structure for work_type_statistics_view
-- ----------------------------
DROP VIEW IF EXISTS `work_type_statistics_view`;
CREATE ALGORITHM = UNDEFINED SQL SECURITY DEFINER VIEW `work_type_statistics_view` AS select `work_order`.`type` AS `type`,count(0) AS `count` from `work_order` group by `work_order`.`type`;

-- ----------------------------
-- Records of work_order_logs
-- ----------------------------
INSERT INTO `work_order_logs` VALUES ('人事', 1);
INSERT INTO `work_order_logs` VALUES ('其他', 1);
INSERT INTO `work_order_logs` VALUES ('工作', 4);
INSERT INTO `work_order_logs` VALUES ('损耗', 1);
INSERT INTO `work_order_logs` VALUES ('薪资', 3);

SET FOREIGN_KEY_CHECKS = 1;
