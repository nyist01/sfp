package xyz.nyist.test;

import java.time.LocalTime;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/28 16:36
 */
public class Test {


    public static void main(String[] args) {
        System.out.println(LocalTime.parse("20:25:00").isBefore(LocalTime.parse("20:25:00")));
        String a = "$2a$10$JAhicY4nRiYV53fP7Guntuyf.jKV/OUTz1wVgHy7.Xw.7FSCrFJy2";
        System.out.println(new BCryptPasswordEncoder().matches("147258a", a));
        System.out.println(LocalTime.parse("20:25:00").isBefore(LocalTime.parse("20:25:00")));
    }


}
