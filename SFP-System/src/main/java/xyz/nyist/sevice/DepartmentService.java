package xyz.nyist.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.nyist.entity.Department;

import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/3 19:55
 */
public interface DepartmentService extends IService<Department> {
    /**
     * 查询  所有子节点(包括自己)
     *
     * @param id id
     * @return 结果
     */
    List<Department> findChild(Integer id);

}

