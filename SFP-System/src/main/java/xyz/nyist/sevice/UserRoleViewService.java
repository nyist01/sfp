package xyz.nyist.sevice;

import xyz.nyist.view.UserRoleView;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/21 16:17
 */
public interface UserRoleViewService extends IService<UserRoleView> {


}

