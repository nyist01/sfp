package xyz.nyist.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.nyist.view.MessageStatisticsView;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 13:16
 */
public interface MessageStatisticsViewService extends IService<MessageStatisticsView> {


}
