package xyz.nyist.sevice;

import xyz.nyist.entity.ClockInTime;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/17 16:09
 */
public interface ClockInTimeService extends IService<ClockInTime> {


}


