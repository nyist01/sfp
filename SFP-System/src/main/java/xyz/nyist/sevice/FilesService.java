package xyz.nyist.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.nyist.entity.Files;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/3 19:55
 */
public interface FilesService extends IService<Files> {

    /**
     * 根据路径删除file
     *
     * @param path 路径
     * @return 结果
     */
    boolean removeByPath(String path);
}




