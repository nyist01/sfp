package xyz.nyist.sevice;

import xyz.nyist.entity.WorkOrderLogs;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 12:23
 */
public interface WorkOrderLogsService extends IService<WorkOrderLogs> {


}


