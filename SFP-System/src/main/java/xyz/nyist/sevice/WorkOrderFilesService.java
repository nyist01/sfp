package xyz.nyist.sevice;

import xyz.nyist.entity.WorkOrderFiles;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/3 19:55
 */
public interface WorkOrderFilesService extends IService<WorkOrderFiles> {


}

