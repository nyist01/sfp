package xyz.nyist.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.nyist.entity.Role;

import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:25
 */
public interface RoleService extends IService<Role> {
    /**
     * 查询  所有子节点
     *
     * @param id id
     * @return 结果
     */
    List<Role> findChild(Integer id);

    /**
     * 根据角色名称查询id
     *
     * @param name 角色名称
     * @return id
     */
    List<Integer> getId(String name);
}
