package xyz.nyist.sevice;

import xyz.nyist.entity.WorkOrder;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/3 19:55
 */
public interface WorkOrderService extends IService<WorkOrder> {


}


