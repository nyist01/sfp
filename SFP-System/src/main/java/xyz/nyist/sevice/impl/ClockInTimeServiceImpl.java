package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.entity.ClockInTime;
import xyz.nyist.mapper.ClockInTimeMapper;
import xyz.nyist.sevice.ClockInTimeService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/17 16:09
 */
@Service
public class ClockInTimeServiceImpl extends ServiceImpl<ClockInTimeMapper, ClockInTime> implements ClockInTimeService {

}


