package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.UserDetailsMapper;
import xyz.nyist.view.UserDetails;
import xyz.nyist.sevice.UserDetailsService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 19:46
 */
@Service
public class UserDetailsServiceImpl extends ServiceImpl<UserDetailsMapper, UserDetails> implements UserDetailsService {

}


