package xyz.nyist.sevice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.entity.Department;
import xyz.nyist.mapper.DepartmentMapper;
import xyz.nyist.sevice.DepartmentService;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/3 19:55
 */
@Service
public class DepartmentServiceImpl extends ServiceImpl<DepartmentMapper, Department> implements DepartmentService {

    @Override
    public List<Department> findChild(Integer id) {
        return baseMapper.findChild(id);
    }
}

