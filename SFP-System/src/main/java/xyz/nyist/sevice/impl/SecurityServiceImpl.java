package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.entity.Security;
import xyz.nyist.mapper.SecurityMapper;
import xyz.nyist.sevice.SecurityService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/14 13:25
  */
@Service
public class SecurityServiceImpl extends ServiceImpl<SecurityMapper, Security> implements SecurityService{

}
