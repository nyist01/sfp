package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.MessageMapper;
import xyz.nyist.entity.Message;
import xyz.nyist.sevice.MessageService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/11 15:22
  */
@Service
public class MessageServiceImpl extends ServiceImpl<MessageMapper, Message> implements MessageService{

}
