package xyz.nyist.sevice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.MessageStatisticsViewMapper;
import xyz.nyist.sevice.MessageStatisticsViewService;
import xyz.nyist.view.MessageStatisticsView;
import org.springframework.stereotype.Service;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 13:16
 */
@Service
public class MessageStatisticsViewServiceImpl extends ServiceImpl<MessageStatisticsViewMapper, MessageStatisticsView> implements MessageStatisticsViewService {

}
