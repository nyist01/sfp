package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.WorkLogsUserViewMapper;
import xyz.nyist.view.WorkLogsUserView;
import xyz.nyist.sevice.WorkLogsUserViewService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 18:15
  */
@Service
public class WorkLogsUserViewServiceImpl extends ServiceImpl<WorkLogsUserViewMapper, WorkLogsUserView> implements WorkLogsUserViewService{

}
