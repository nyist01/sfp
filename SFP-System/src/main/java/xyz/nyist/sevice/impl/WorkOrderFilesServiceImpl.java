package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.WorkOrderFilesMapper;
import xyz.nyist.entity.WorkOrderFiles;
import xyz.nyist.sevice.WorkOrderFilesService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/3 19:55
 */
@Service
public class WorkOrderFilesServiceImpl extends ServiceImpl<WorkOrderFilesMapper, WorkOrderFiles> implements WorkOrderFilesService {

}

