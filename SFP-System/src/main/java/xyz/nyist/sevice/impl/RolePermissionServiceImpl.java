package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.RolePermissionMapper;
import xyz.nyist.entity.RolePermission;
import xyz.nyist.sevice.RolePermissionService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:25
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionMapper, RolePermission> implements RolePermissionService {

}

