package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.entity.Permission;
import xyz.nyist.mapper.PermissionMapper;
import xyz.nyist.sevice.PermissionService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/14 13:25
  */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionMapper, Permission> implements PermissionService{

}
