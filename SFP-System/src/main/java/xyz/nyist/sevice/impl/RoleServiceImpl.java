package xyz.nyist.sevice.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.entity.Role;
import xyz.nyist.mapper.RoleMapper;
import xyz.nyist.sevice.RoleService;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:25
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements RoleService {

    @Override
    public List<Role> findChild(Integer id) {
        return baseMapper.findChild(id);
    }

    @Override
    public List<Integer> getId(String name) {
        return baseMapper.selectList(new QueryWrapper<Role>().in("name", name.split(","))).stream().map(Role::getId).collect(Collectors.toList());
    }
}
