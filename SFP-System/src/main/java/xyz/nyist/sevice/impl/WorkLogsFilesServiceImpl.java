package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.WorkLogsFilesMapper;
import xyz.nyist.entity.WorkLogsFiles;
import xyz.nyist.sevice.WorkLogsFilesService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 12:23
  */
@Service
public class WorkLogsFilesServiceImpl extends ServiceImpl<WorkLogsFilesMapper, WorkLogsFiles> implements WorkLogsFilesService{

}
