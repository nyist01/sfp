package xyz.nyist.sevice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.UserSimpleMapper;
import xyz.nyist.sevice.UserSimpleService;
import xyz.nyist.view.UserSimple;
import org.springframework.stereotype.Service;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/16 14:07
 */
@Service
public class UserSimpleServiceImpl extends ServiceImpl<UserSimpleMapper, UserSimple> implements UserSimpleService {

}
