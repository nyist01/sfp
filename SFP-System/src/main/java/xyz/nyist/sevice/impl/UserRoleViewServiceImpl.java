package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.UserRoleViewMapper;
import xyz.nyist.view.UserRoleView;
import xyz.nyist.sevice.UserRoleViewService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/21 16:17
 */
@Service
public class UserRoleViewServiceImpl extends ServiceImpl<UserRoleViewMapper, UserRoleView> implements UserRoleViewService {

}

