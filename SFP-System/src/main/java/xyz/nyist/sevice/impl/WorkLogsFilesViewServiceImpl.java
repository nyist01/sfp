package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.WorkLogsFilesViewMapper;
import xyz.nyist.view.WorkLogsFilesView;
import xyz.nyist.sevice.WorkLogsFilesViewService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 17:37
  */
@Service
public class WorkLogsFilesViewServiceImpl extends ServiceImpl<WorkLogsFilesViewMapper, WorkLogsFilesView> implements WorkLogsFilesViewService{

}
