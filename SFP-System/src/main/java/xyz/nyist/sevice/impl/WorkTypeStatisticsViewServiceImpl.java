package xyz.nyist.sevice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.WorkTypeStatisticsViewMapper;
import xyz.nyist.sevice.WorkTypeStatisticsViewService;
import xyz.nyist.view.WorkTypeStatisticsView;
import org.springframework.stereotype.Service;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 14:53
 */
@Service
public class WorkTypeStatisticsViewServiceImpl extends ServiceImpl<WorkTypeStatisticsViewMapper, WorkTypeStatisticsView> implements WorkTypeStatisticsViewService {

}
