package xyz.nyist.sevice.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.extension.toolkit.SqlHelper;
import com.google.common.collect.ImmutableMap;
import xyz.nyist.entity.Files;
import xyz.nyist.mapper.FilesMapper;
import xyz.nyist.sevice.FilesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.io.File;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/3 19:55
 */
@Slf4j
@Service
public class FilesServiceImpl extends ServiceImpl<FilesMapper, Files> implements FilesService {

    @Value("${file-path}")
    private String path;

    @Override
    public boolean removeByPath(String path) {
        int index = path.indexOf("?id=");
        if (index != -1) {
            path = path.substring(0, index);
        }
        log.warn("删除文件" + path + "----" + new File(this.path + path).delete());
        return SqlHelper.retBool(baseMapper.deleteByMap(ImmutableMap.of("path", path)));
    }
}




