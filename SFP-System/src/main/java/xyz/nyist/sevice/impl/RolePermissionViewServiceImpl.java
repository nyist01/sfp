package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.RolePermissionViewMapper;
import xyz.nyist.view.RolePermissionView;
import xyz.nyist.sevice.RolePermissionViewService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/21 16:23
  */
@Service
public class RolePermissionViewServiceImpl extends ServiceImpl<RolePermissionViewMapper, RolePermissionView> implements RolePermissionViewService{

}
