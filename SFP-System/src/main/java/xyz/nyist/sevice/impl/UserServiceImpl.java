package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.entity.User;
import xyz.nyist.mapper.UserMapper;
import xyz.nyist.sevice.UserService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:25
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserMapper, User> implements UserService {

}






