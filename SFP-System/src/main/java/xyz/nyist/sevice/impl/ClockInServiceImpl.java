package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.entity.ClockIn;
import xyz.nyist.mapper.ClockInMapper;
import xyz.nyist.sevice.ClockInService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/17 16:20
  */
@Service
public class ClockInServiceImpl extends ServiceImpl<ClockInMapper, ClockIn> implements ClockInService{

}
