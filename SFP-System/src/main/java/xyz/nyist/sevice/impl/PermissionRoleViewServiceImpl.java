package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.view.PermissionRoleView;
import xyz.nyist.mapper.PermissionRoleViewMapper;
import xyz.nyist.sevice.PermissionRoleViewService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/21 16:34
  */
@Service
public class PermissionRoleViewServiceImpl extends ServiceImpl<PermissionRoleViewMapper, PermissionRoleView> implements PermissionRoleViewService{

}
