package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.entity.WorkOrderLogs;
import xyz.nyist.mapper.WorkOrderLogsMapper;
import xyz.nyist.sevice.WorkOrderLogsService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 12:23
 */
@Service
public class WorkOrderLogsServiceImpl extends ServiceImpl<WorkOrderLogsMapper, WorkOrderLogs> implements WorkOrderLogsService {

}


