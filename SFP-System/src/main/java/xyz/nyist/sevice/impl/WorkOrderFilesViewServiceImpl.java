package xyz.nyist.sevice.impl;

import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.nyist.mapper.WorkOrderFilesViewMapper;
import xyz.nyist.view.WorkOrderFilesView;
import xyz.nyist.sevice.WorkOrderFilesViewService;
/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 17:37
  */
@Service
public class WorkOrderFilesViewServiceImpl extends ServiceImpl<WorkOrderFilesViewMapper, WorkOrderFilesView> implements WorkOrderFilesViewService{

}
