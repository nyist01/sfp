package xyz.nyist.sevice;

import xyz.nyist.view.WorkLogsUserView;
import com.baomidou.mybatisplus.extension.service.IService;
    /**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 18:15
  */
public interface WorkLogsUserViewService extends IService<WorkLogsUserView>{


}
