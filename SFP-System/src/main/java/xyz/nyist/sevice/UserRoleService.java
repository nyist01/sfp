package xyz.nyist.sevice;

import xyz.nyist.entity.UserRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:25
 */
public interface UserRoleService extends IService<UserRole> {


}

