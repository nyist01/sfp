package xyz.nyist.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.nyist.view.WorkTypeStatisticsView;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 14:53
 */
public interface WorkTypeStatisticsViewService extends IService<WorkTypeStatisticsView> {


}
