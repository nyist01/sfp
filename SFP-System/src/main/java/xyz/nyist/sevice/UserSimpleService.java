package xyz.nyist.sevice;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.nyist.view.UserSimple;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/16 14:07
 */
public interface UserSimpleService extends IService<UserSimple> {


}
