package xyz.nyist.sevice;

import xyz.nyist.view.UserDetails;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 19:46
 */
public interface UserDetailsService extends IService<UserDetails> {


}


