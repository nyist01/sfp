package xyz.nyist.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import xyz.nyist.entity.Department;
import xyz.nyist.entity.User;
import xyz.nyist.sevice.DepartmentService;
import xyz.nyist.sevice.UserService;
import xyz.nyist.view.UserSimple;
import xyz.nyist.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/7 15:22
 */
@Slf4j
@RestController
@RequestMapping("/department")
public class DepartmentController {
    private final DepartmentService departmentService;
    private final UserService userService;

    public DepartmentController(DepartmentService departmentService, UserService userService) {
        this.departmentService = departmentService;
        this.userService = userService;
    }

    /**
     * 查询所有部门
     *
     * @return 部门
     */
    @GetMapping("/select/all")
    public Result<List<Department>> selectAll() {
        return new Result<>(departmentService.list());
    }

    /**
     * 删除部门
     *
     * @param userSimple token
     * @param department 部门
     * @return 结果
     */
    @PostMapping("/delete")
    public Result<Void> delete(UserSimple userSimple, @RequestBody Department department) {
        List<Integer> ids = departmentService.findChild(department.getId()).stream().map(Department::getId)
                .collect(Collectors.toList());
        if (!userService.list(new QueryWrapper<User>()
                .in("department_id", ids)).isEmpty()) {
            return new Result<>(Result.CodeStatus.FAIL, "删除失败,该部门下存在用户");
        }
        return new Result<>(department.getId(), "删除部门", userSimple.getPrincipal(), departmentService.removeById(department.getId()));
    }

    /**
     * 添加部门
     *
     * @param userSimple token
     * @param department 部门
     * @return 结果
     */
    @PostMapping("/insert")
    public Result<Void> insert(UserSimple userSimple, @RequestBody Department department) {
        if (!departmentService.list(new QueryWrapper<Department>().eq("name", department.getName())).isEmpty()) {
            return new Result<>(Result.CodeStatus.FAIL, "名字已存在");
        }
        department.setCreated(LocalDateTime.now());
        department.setUpdated(LocalDateTime.now());
        departmentService.save(department);
        return new Result<>(department.getId(), "添加部门", userSimple.getPrincipal(), true);
    }

    /**
     * 修改部门
     *
     * @param userSimple token
     * @param department 部门
     * @return 结果
     */
    @PostMapping("/update")
    public Result<Void> update(UserSimple userSimple, @RequestBody Department department) {
        if (department.getName() != null && !departmentService.getById(department.getId()).getName().equals(department.getName())) {
            if (!departmentService.list(new QueryWrapper<Department>().eq("name", department.getName())).isEmpty()) {
                return new Result<>(Result.CodeStatus.FAIL, "名字已存在");
            }
        }
        department.setCreated(null);
        department.setUpdated(LocalDateTime.now());
        departmentService.updateById(department);
        return new Result<>(department.getId(), "修改部门", userSimple.getPrincipal(), true);
    }

    /**
     * 获取用户所在部门的所有子部门（含自己）
     *
     * @param userSimple token
     * @return 结果
     */
    @GetMapping("/getChild")
    public Result<List<Department>> getChile(UserSimple userSimple) {
        Integer departmentId = userService.getById(userSimple.getId()).getDepartmentId();
        return new Result<>(Result.CodeStatus.OK, "获取所有子部门", departmentService.findChild(departmentId));
    }
}
