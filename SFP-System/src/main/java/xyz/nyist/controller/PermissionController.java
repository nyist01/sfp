package xyz.nyist.controller;

import xyz.nyist.configs.security.CustomFilterInvocationSecurityMetadataSource;
import xyz.nyist.entity.Permission;
import xyz.nyist.sevice.PermissionService;
import xyz.nyist.sevice.RolePermissionViewService;
import xyz.nyist.view.RolePermissionView;
import xyz.nyist.vo.Result;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:26
 */
@RestController
@RequestMapping("/permission")
public class PermissionController {

    private final CustomFilterInvocationSecurityMetadataSource customFilterInvocationSecurityMetadataSource;
    private final PermissionService permissionService;
    private final RolePermissionViewService rolePermissionViewService;

    public PermissionController(PermissionService permissionService, CustomFilterInvocationSecurityMetadataSource customFilterInvocationSecurityMetadataSource, RolePermissionViewService permissionViewService) {
        this.permissionService = permissionService;
        this.customFilterInvocationSecurityMetadataSource = customFilterInvocationSecurityMetadataSource;
        this.rolePermissionViewService = permissionViewService;
    }

    @GetMapping("/getPermission")
    public Result<List<Permission>> test() {
        return new Result<>(permissionService.list());
    }

    @PostMapping("/refreshPermissions")
    public Result<Void> refreshPermissions() {
        customFilterInvocationSecurityMetadataSource.getResourcePermission();
        return new Result<>(Result.CodeStatus.OK, "success");
    }

    @GetMapping("/getRolePermission")
    public Result<List<RolePermissionView>> getRolePermission() {
        return new Result<>(rolePermissionViewService.list());
    }

}
