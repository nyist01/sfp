package xyz.nyist.controller;

import xyz.nyist.entity.Files;
import xyz.nyist.sevice.FilesService;
import xyz.nyist.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.http.HttpServletResponse;
import java.io.*;
import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/5 15:54
 */
@Slf4j
@RestController
@RequestMapping("/file")
public class FileController {

    @Value("${file-path}")
    private String path;
    private final FilesService filesService;


    public FileController(FilesService filesService) {
        this.filesService = filesService;
    }

    /**
     * 上传文件
     *
     * @param file 文件对象
     * @return id
     */
    @PostMapping("/upload")
    public Result<Files> upload(MultipartFile file, String type) {
        if (file == null) {
            return new Result<>(Result.CodeStatus.FAIL, "参数错误");
        }
        String fileName = Objects.requireNonNull(file.getOriginalFilename()).substring(file.getOriginalFilename().lastIndexOf("\\") + 1);
        String path = "/files/" + (type == null ? "" : type + "/") + UUID.randomUUID().toString().substring(0, 8) + fileName;
        try {
            log.info("上传文件：" + this.path + path);
            FileUtils.copyInputStreamToFile(file.getInputStream(), new File(this.path + path));
            Files files = new Files(null, path, fileName, Byte.valueOf("0"), LocalDateTime.now());
            if (filesService.save(files)) {
                files.setPath(files.getPath() + "?id=" + files.getId());
                return new Result<>(Result.CodeStatus.OK, "上传成功", files);
            }
            else {
                return new Result<>(Result.CodeStatus.FAIL, "上传失败");
            }
        } catch (Exception e) {
            e.printStackTrace();
            return new Result<>(Result.CodeStatus.FAIL, "上传失败");
        }
    }

    /**
     * 下载文件
     *
     * @param response response
     * @param id       文件id
     * @throws IOException io异常
     */
    @GetMapping("/download")
    public Result<Void> download(HttpServletResponse response, Integer id) throws IOException {
        Files files = filesService.getById(id);
        try {
            if (files != null) {
                response.reset();
                response.setContentType("application/octet-stream");
                response.setHeader("Content-Disposition", "attachment;filename=" + files.getFileName());
                InputStream fis = new BufferedInputStream(new FileInputStream(this.path + files.getPath()));
                OutputStream toClient = new BufferedOutputStream(response.getOutputStream());
                byte[] temp = new byte[1024 * 10];
                int size;
                while ((size = fis.read(temp)) != -1) {
                    toClient.write(temp, 0, size);
                }
                fis.close();
                toClient.flush();
                toClient.close();
            }
            return null;
        } catch (IOException e) {
            e.printStackTrace();
            return new Result<>(Result.CodeStatus.FAIL, "网络异常");
        }
    }


}
