package xyz.nyist.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import xyz.nyist.entity.ClockIn;
import xyz.nyist.entity.ClockInTime;
import xyz.nyist.entity.User;
import xyz.nyist.sevice.ClockInService;
import xyz.nyist.sevice.ClockInTimeService;
import xyz.nyist.sevice.UserService;
import xyz.nyist.view.UserSimple;
import xyz.nyist.vo.ClockInStatistics;
import xyz.nyist.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/17 16:10
 */
@Slf4j
@RestController
@RequestMapping("/clock")
public class ClockInController {


    private final ClockInService clockInService;
    private final ClockInTimeService clockInTimeService;


    @Autowired
    private UserService userService;

    @Autowired
    public ClockInController(ClockInService clockInService, ClockInTimeService clockInTimeService) {
        this.clockInService = clockInService;
        this.clockInTimeService = clockInTimeService;
    }

    /**
     * 设置打卡时间
     *
     * @param userSimple  token
     * @param clockInTime 时间
     * @return 结果
     */
    @PostMapping("/setTime")
    public Result<Void> setTime(UserSimple userSimple, @RequestBody ClockInTime clockInTime) {
        clockInTime.setId(1);
        return new Result<>("设置打卡时间", userSimple.getPrincipal(), clockInTimeService.updateById(clockInTime));
    }

    /**
     * 获取打卡时间
     *
     * @return 结果
     */
    @GetMapping("/getTime")
    public Result<ClockInTime> getTime() {
        return new Result<>(Result.CodeStatus.OK, "获取打卡时间", clockInTimeService.getById(1));
    }

    /**
     * 打卡
     *
     * @param userSimple token
     * @return 结果
     */
    @PostMapping("/clockIn")
    public Result<Void> clockIn(UserSimple userSimple) {
        ClockInTime clockInTime = getTime().getData();
        LambdaQueryWrapper<ClockIn> lq = Wrappers.lambdaQuery();
        lq.eq(ClockIn::getUserId, userSimple.getId());
        lq.between(ClockIn::getDateTime, LocalDate.now(), LocalDate.now().plusDays(1));
        LocalTime now = LocalTime.parse(LocalTime.now().format(DateTimeFormatter.ofPattern("HH:mm")));
        if (!now.isBefore(clockInTime.getStartTime1()) && !now.isAfter(clockInTime.getEndTime1())) {
            lq.eq(ClockIn::getType, 0);
            if (!clockInService.list(lq).isEmpty()) {
                return new Result<>(Result.CodeStatus.FAIL, "已打过卡，不要重复打卡");
            }
            return new Result<>("打卡", userSimple.getPrincipal(), clockInService.save(new ClockIn(null, userSimple.getId(), LocalDateTime.now(), Byte.valueOf("0"))));
        }
        else if (!now.isBefore(clockInTime.getStartTime2()) && !now.isAfter(clockInTime.getEndTime2())) {
            lq.eq(ClockIn::getType, 1);
            if (!clockInService.list(lq).isEmpty()) {
                return new Result<>(Result.CodeStatus.FAIL, "已打过卡，不要重复打卡");
            }
            return new Result<>("打卡", userSimple.getPrincipal(), clockInService.save(new ClockIn(null, userSimple.getId(), LocalDateTime.now(), Byte.valueOf("1"))));
        }
        else {
            return new Result<>(Result.CodeStatus.FAIL, "不在打卡时间");
        }
    }

    /**
     * 统计最近7天迟到人数，早退人数，新增用户数
     *
     * @return 统计结果
     */
    @GetMapping("/statistics/all")
    public Result<List<ClockInStatistics>> statisticsAll() {
        List<ClockInStatistics> list = new ArrayList<>(7);
        LocalDate today = LocalDate.now();
        for (int i = 6; i > -1; i--) {
            LambdaQueryWrapper<User> userLq = Wrappers.lambdaQuery();
            LambdaQueryWrapper<User> userLq1 = Wrappers.lambdaQuery();
            userLq.le(User::getCreateTime, today.minusDays(i - 1));
            userLq.eq(User::getIsAdmin, 0);
            LambdaQueryWrapper<ClockIn> onClockLq = Wrappers.lambdaQuery();
            LambdaQueryWrapper<ClockIn> offClockLq = Wrappers.lambdaQuery();
            //今天的前i天 ---今天的前i-1天
            onClockLq.between(ClockIn::getDateTime, today.minusDays(i), today.minusDays(i - 1));
            //le ≠
            //userLq.le(User::getCreateTime, today.minusDays(i));
            //统计总人数
            int a = userService.count(userLq);
            onClockLq.eq(ClockIn::getType, 0);
            //统计上班打卡人数
            int b = clockInService.count(onClockLq);
            offClockLq.between(ClockIn::getDateTime, today.minusDays(i), today.minusDays(i - 1));
            offClockLq.eq(ClockIn::getType, 1);
            //统计下班打卡人数
            int c = clockInService.count(offClockLq);
            userLq1.between(User::getCreateTime, today.minusDays(i), today.minusDays(i - 1));
            //统计今日新增用户数
            int d = userService.count(userLq1);
            list.add(new ClockInStatistics(a, b, c, d));
        }
        /*list = Arrays.asList(new ClockInStatistics(100, 95, 89, 5),
                new ClockInStatistics(110, 99, 100, 3),
                new ClockInStatistics(120, 110, 103, 5),
                new ClockInStatistics(100, 99, 100, 7),
                new ClockInStatistics(130, 126, 99, 9),
                new ClockInStatistics(90, 79, 69, 10),
                new ClockInStatistics(108, 100, 107, 2));*/
        return new Result<>(Result.CodeStatus.OK, "获取打卡统计", list);
    }

    /**
     * 获取个人打卡记录
     *
     * @param userSimple token
     * @return 结果
     */
    @GetMapping("/statistics/one")
    public Result<List<ClockIn>> statisticsOne(UserSimple userSimple) {
        return new Result<>(Result.CodeStatus.OK, "获取个人打卡记录", clockInService.list(new QueryWrapper<ClockIn>().eq("user_id", userSimple.getId())));
    }

    /**
     * 获取个人当天打卡记录
     *
     * @return 结果
     */
    @GetMapping("/getToday")
    public Result<Map<String, Boolean>> getToday(UserSimple userSimple) {
        Map<String, Boolean> map = new HashMap<>(2);
        QueryWrapper<ClockIn> qw = new QueryWrapper<ClockIn>().eq("user_id", userSimple.getId());
        qw.between("date_time", LocalDate.now(), LocalDate.now().plusDays(1L));
        List<ClockIn> list = clockInService.list(qw);
        list.forEach(clockIn -> {
            if (clockIn.getType().equals(Byte.valueOf("0"))) {
                map.put("start", true);
            }
            else if (clockIn.getType().equals(Byte.valueOf("1"))) {
                map.put("end", true);
            }
        });
        return new Result<>(Result.CodeStatus.OK, "获取个人当天打卡记录", map);
    }
}
