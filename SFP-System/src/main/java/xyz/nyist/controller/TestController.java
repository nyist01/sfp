package xyz.nyist.controller;

import xyz.nyist.utils.FileUtil;
import xyz.nyist.utils.MessageUtil;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/21 21:42
 */

@RestController
@RequestMapping("/test")
public class TestController {

    @Resource(name = "messageUtil")
    private MessageUtil messageUtil;
    @Resource(name = "fileUtil")
    private FileUtil fileUtil;

    @GetMapping("/1")
    public String test1() throws InterruptedException {
        messageUtil.messageRedisToMySql();
        fileUtil.clearUpFile();
        return "1";
    }

    @GetMapping("/2")
    public String test2() throws InterruptedException {
        Thread.sleep(5000);
        return "2";
    }
}
