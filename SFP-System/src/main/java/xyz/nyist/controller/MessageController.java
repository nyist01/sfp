package xyz.nyist.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import xyz.nyist.sevice.MessageStatisticsViewService;
import xyz.nyist.utils.ArrayListUtil;
import xyz.nyist.view.MessageStatisticsView;
import xyz.nyist.view.UserSimple;
import xyz.nyist.vo.Msg;
import xyz.nyist.vo.Result;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/11 15:22
 */
@RestController
@RequestMapping("/message")
public class MessageController {
    private static final Integer TYPE_COMMON = 1;
    private static final Integer TYPE_IMG = 2;
    private static final Integer TYPE_FILE = 3;
    private final MessageStatisticsViewService messageStatisticsViewService;
    private static final String UNREAD_MSG_KEY = "sfp:unread:";

    @Resource
    private RedisTemplate<String, Msg.UnreadMessage> redisTemplate;

    public MessageController(MessageStatisticsViewService messageStatisticsViewService) {
        this.messageStatisticsViewService = messageStatisticsViewService;
    }

    /**
     * 聊天消息统计
     *
     * @return 结果
     */
    @GetMapping("/statistics")
    public Result<Map<Integer, Long[]>> statistics() {
        Map<Integer, Long[]> map = new HashMap<>(3);
        LocalDate today = LocalDate.now();
        Long[] a = new Long[7];
        Long[] b = new Long[7];
        Long[] c = new Long[7];
        List<MessageStatisticsView> list = messageStatisticsViewService.list(new QueryWrapper<MessageStatisticsView>().orderByAsc("days"));
        for (int i = 0; i < 7; i++) {
            int finalI = 6 - i;
            MessageStatisticsView ams = list.stream().filter(t -> TYPE_COMMON.equals(t.getType()) && t.getDays().equals(today.minusDays(finalI))).collect(ArrayListUtil.toSingleton());
            a[i] = ams == null ? 0L : ams.getCount();

            MessageStatisticsView bms = list.stream().filter(t -> TYPE_IMG.equals(t.getType()) && t.getDays().equals(today.minusDays(finalI))).collect(ArrayListUtil.toSingleton());
            b[i] = bms == null ? 0L : bms.getCount();

            MessageStatisticsView cms = list.stream().filter(t -> TYPE_FILE.equals(t.getType()) && t.getDays().equals(today.minusDays(finalI))).collect(ArrayListUtil.toSingleton());
            c[i] = cms == null ? 0L : cms.getCount();
        }
        /*//Todo  假数据
        a = new Long[]{79L, 52L, 200L, 334L, 390L, 330L, 220L};
        b = new Long[]{80L, 52L, 200L, 334L, 390L, 330L, 220L};
        c = new Long[]{30L, 52L, 200L, 334L, 390L, 330L, 220L};*/
        map.put(TYPE_COMMON, a);
        map.put(TYPE_IMG, b);
        map.put(TYPE_FILE, c);
        return new Result<>(Result.CodeStatus.OK, "获取聊天消息统计", map);
    }

    /**
     * 获取未读消息
     *
     * @param userSimple token
     * @return 未读消息
     */
    @GetMapping("/getUnreadMessage")
    public Result<List<Msg.UnreadMessage>> getUnreadMessage(UserSimple userSimple) {
        List<Msg.UnreadMessage> list = redisTemplate.opsForList().range(UNREAD_MSG_KEY + userSimple.getId(), 0, -1);
        redisTemplate.delete(UNREAD_MSG_KEY + userSimple.getId());
        return new Result<>(Result.CodeStatus.OK, "获取未读消息", list);
    }
}
