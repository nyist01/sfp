package xyz.nyist.controller;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import xyz.nyist.entity.WorkLogsFiles;
import xyz.nyist.entity.WorkOrder;
import xyz.nyist.entity.WorkOrderFiles;
import xyz.nyist.entity.WorkOrderLogs;
import xyz.nyist.sevice.*;
import xyz.nyist.utils.FileUtil;
import xyz.nyist.view.*;
import xyz.nyist.vo.*;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 12:25
 */
@RestController
@RequestMapping("/work")
public class WorkController {

    private final WorkTypeStatisticsViewService workTypeStatisticsViewService;
    private final FileUtil fileUtil;
    private final WorkOrderFilesService workOrderFilesService;
    private final WorkLogsFilesService workLogsFilesService;
    private final UserDetailsService userDetailsService;
    private final UserService userService;
    private final WorkLogsUserViewService workLogsUserViewService;
    private final WorkOrderLogsService workOrderLogsService;
    private final WorkLogsFilesViewService workLogsFilesViewService;
    private final WorkOrderFilesViewService workOrderFilesViewService;
    private final WorkOrderService workOrderService;

    public WorkController(WorkOrderFilesViewService workOrderFilesViewService, WorkOrderService workOrderService, WorkOrderLogsService workOrderLogsService, WorkLogsFilesViewService workLogsFilesViewService, WorkLogsUserViewService workLogsUserViewService, UserService userService, UserDetailsService userDetailsService, WorkLogsFilesService workLogsFilesService, WorkOrderFilesService workOrderFilesService, FileUtil fileUtil, WorkTypeStatisticsViewService workTypeStatisticsViewService) {
        this.workOrderFilesViewService = workOrderFilesViewService;
        this.workOrderService = workOrderService;
        this.workOrderLogsService = workOrderLogsService;
        this.workLogsFilesViewService = workLogsFilesViewService;
        this.workLogsUserViewService = workLogsUserViewService;
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.workLogsFilesService = workLogsFilesService;
        this.workOrderFilesService = workOrderFilesService;
        this.fileUtil = fileUtil;
        this.workTypeStatisticsViewService = workTypeStatisticsViewService;
    }

    /**
     * 查询待处理工单数量
     *
     * @param userSimple token
     * @return 结果
     */
    @GetMapping("/getCount")
    public Result<Integer> getCount(UserSimple userSimple) {
        LambdaQueryWrapper<WorkOrder> lq = getLq(userSimple);
        lq.eq(WorkOrder::getStatus, 0);
        return new Result<>(Result.CodeStatus.OK, "获取需要自己处理工单未完成数量", workOrderService.count(lq));
    }

    /**
     * 查询需要自己处理的工单
     * 条件：紧急级别，内容(标题or内容)，类型
     *
     * @param userSimple 用户信息
     * @param pageQuery  分页信息
     * @return list《工单》
     */
    @PostMapping("/select/to")
    public Result<IPage<WorkOrder>> selectTo(UserSimple userSimple, @RequestBody PageQuery<WorkOrder> pageQuery) {
        return selectWork(getLq(userSimple), pageQuery);
    }

    private LambdaQueryWrapper<WorkOrder> getLq(UserSimple userSimple) {
        List<WorkOrderLogs> workOrderLogs = workOrderLogsService.list(new QueryWrapper<WorkOrderLogs>().select("work_order_id").eq("user_id", userSimple.getId()));
        List<Integer> ids = workOrderLogs.stream().map(WorkOrderLogs::getWorkOrderId).collect(Collectors.toList());
        if (ids.isEmpty()) {
            ids.add(0);
        }
        //条件
        LambdaQueryWrapper<WorkOrder> lq = Wrappers.lambdaQuery();
        lq.in(WorkOrder::getId, ids);
        lq.ne(WorkOrder::getUserId, userSimple.getId());
        return lq;
    }

    /**
     * 查询自己的工单
     * 条件：紧急级别，内容(标题or内容)，类型
     *
     * @param userSimple 用户信息
     * @param pageQuery  分页信息
     * @return list《工单》
     */
    @PostMapping("/select/from")
    public Result<IPage<WorkOrder>> selectFrom(UserSimple userSimple, @RequestBody PageQuery<WorkOrder> pageQuery) {
        LambdaQueryWrapper<WorkOrder> lq = Wrappers.lambdaQuery();
        lq.eq(WorkOrder::getUserId, userSimple.getId());
        return selectWork(lq, pageQuery);
    }

    /**
     * 查询所有的工单
     * 条件：紧急级别，内容(标题or内容)，类型
     *
     * @param pageQuery 分页信息
     * @return list《工单》
     */
    @PostMapping("/select/all")
    public Result<IPage<WorkOrder>> selectAll(@RequestBody PageQuery<WorkOrder> pageQuery) {
        return selectWork(Wrappers.lambdaQuery(), pageQuery);
    }

    private Result<IPage<WorkOrder>> selectWork(LambdaQueryWrapper<WorkOrder> lq, PageQuery<WorkOrder> pageQuery) {
        lq.and(StringUtils.isNotEmpty(pageQuery.getKeywords()), i ->
                i.like(WorkOrder::getTitle, pageQuery.getKeywords())
                        .or().like(WorkOrder::getContent, pageQuery.getKeywords()));
        WorkOrder workOrder = pageQuery.getT();
        if (workOrder != null) {
            lq.eq(workOrder.getEmergencyLevel() != null, WorkOrder::getEmergencyLevel, workOrder.getEmergencyLevel());
            lq.eq(StringUtils.isNotEmpty(workOrder.getType()), WorkOrder::getType, workOrder.getType());
        }
        //分页 排序
        Page<WorkOrder> page = pageQuery.getPageCondition();
        page.setOrders(Arrays.asList(OrderItem.asc("status"), OrderItem.desc("emergency_level"), OrderItem.asc("create_time")));
        return new Result<>(workOrderService.page(page, lq));
    }

    /**
     * 查询工单详情
     *
     * @param id 工单id
     * @return 详情
     */
    @GetMapping("/details")
    public Result<WorkOrder> details(Integer id) {
        WorkOrder workOrder = workOrderService.getById(id);
        workOrder.setWorkOrderFilesViews(workOrderFilesViewService.list(new QueryWrapper<WorkOrderFilesView>().eq("work_order_id", id)));
        workOrder.setWorkLogsUserViews(workLogsUserViewService.list(new QueryWrapper<WorkLogsUserView>().orderByAsc("create_time").eq("work_order_id", id)));
        workOrder.setUserDetails(userDetailsService.getById(workOrder.getUserId()));
        return new Result<>(workOrder);
    }

    /**
     * 获取工单操作日志的文件
     *
     * @param id id
     * @return 文件
     */
    @GetMapping("/select/files")
    public Result<List<WorkLogsFilesView>> selectFiles(Integer id) {
        return new Result<>(workLogsFilesViewService.list(new QueryWrapper<WorkLogsFilesView>().eq("work_logs_id", id)));
    }

    /**
     * 处理工单
     *
     * @param workOrderDispose 信息
     * @return 结果
     */
    @PostMapping("/dispose")
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> dispose(@RequestBody WorkOrderDispose workOrderDispose) {
        WorkOrderLogs workOrderLogs = new WorkOrderLogs();
        workOrderLogs.setId(workOrderDispose.getId());
        workOrderLogs.setSketch(workOrderDispose.getSketch());
        workOrderLogs.setContent(StringUtils.isEmpty(workOrderDispose.getContent()) ? "无" : workOrderDispose.getContent());
        workOrderLogs.setCreateTime(LocalDateTime.now());
        try {
            workOrderLogsService.updateById(workOrderLogs);
            if (!workOrderDispose.getFilesId().isEmpty()) {
                fileUtil.useFile(workOrderDispose.getFilesId());
                List<WorkLogsFiles> logsFiles = workOrderDispose.getFilesId().stream().map(i -> new WorkLogsFiles(null, i, workOrderDispose.getId())).collect(Collectors.toList());
                workLogsFilesService.saveBatch(logsFiles);
            }
            if (!workOrderDispose.isEnd()) {
                Integer userId = workOrderService.getById(workOrderDispose.getWorkOrderId()).getUserId();
                workOrderLogsService.save(new WorkOrderLogs(workOrderDispose.getWorkOrderId(), userId));
            }
            else {
                WorkOrder workOrder = new WorkOrder();
                workOrder.setId(workOrderDispose.getWorkOrderId());
                workOrder.setStatus(1);
                workOrderService.updateById(workOrder);
            }
            return new Result<>(Result.CodeStatus.OK, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(Result.CodeStatus.FAIL, "操作失败，请重试");
        }
    }

    /**
     * 转接
     *
     * @param workSwitchOver 信息
     * @return 转接
     */
    @PostMapping("/switchOver")
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> switchOver(@RequestBody WorkSwitchOver workSwitchOver) {
        String nickname = userService.getById(workSwitchOver.getUserId()).getNickname();
        WorkOrderLogs workOrderLogs = new WorkOrderLogs();
        workOrderLogs.setId(workSwitchOver.getId());
        workOrderLogs.setSketch("转接工单给" + nickname);
        workOrderLogs.setContent(StringUtils.isEmpty(workSwitchOver.getContent()) ? "无" : workSwitchOver.getContent());
        workOrderLogs.setCreateTime(LocalDateTime.now());
        try {
            workOrderLogsService.updateById(workOrderLogs);
            workOrderLogsService.save(new WorkOrderLogs(workSwitchOver.getWorkOrderId(), workSwitchOver.getUserId()));
            return new Result<>(Result.CodeStatus.OK, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(Result.CodeStatus.FAIL, "操作失败，请重试");
        }
    }

    /**
     * 完结
     *
     * @param workOrder 信息
     * @return 完结
     */
    @PostMapping("/end")
    public Result<Void> end(@RequestBody WorkOrder workOrder) {
        workOrder.setStatus(1);
        workOrderService.updateById(workOrder);
        return new Result<>(Result.CodeStatus.OK, "操作成功");
    }

    /**
     * 新建工单
     *
     * @param userSimple      用户
     * @param workOrderInsert 工单信息
     * @return 结果
     */
    @PostMapping("/insert")
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> insert(UserSimple userSimple, @RequestBody WorkOrderInsert workOrderInsert) {
        WorkOrder workOrder = new WorkOrder();
        workOrder.setTitle(workOrderInsert.getTitle());
        workOrder.setContent(workOrderInsert.getContent());
        workOrder.setType(workOrderInsert.getType());
        workOrder.setEmergencyLevel(workOrderInsert.getEmergencyLevel());
        workOrder.setUserId(userSimple.getId());
        workOrder.setCreateTime(LocalDateTime.now());
        workOrder.setStatus(0);
        String nickname = userService.getById(workOrderInsert.getUserId()).getNickname();
        try {
            workOrderService.save(workOrder);
            workOrderLogsService.save(new WorkOrderLogs(null, workOrder.getId(), LocalDateTime.now(), "无", "提交工单给" + nickname, workOrderInsert.getUserId()));
            workOrderLogsService.save(new WorkOrderLogs(null, workOrder.getId(), LocalDateTime.now().plusSeconds(1), null, null, workOrderInsert.getUserId()));
            if (!workOrderInsert.getFilesId().isEmpty()) {
                fileUtil.useFile(workOrderInsert.getFilesId());
                List<WorkOrderFiles> logsFiles = workOrderInsert.getFilesId().stream().map(i -> new WorkOrderFiles(null, i, workOrder.getId())).collect(Collectors.toList());
                workOrderFilesService.saveBatch(logsFiles);
            }
            return new Result<>(workOrder.getId(), "提交工单", userSimple.getPrincipal(), true);
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(workOrder.getId(), "提交工单", userSimple.getPrincipal(), false);
        }

    }

    /**
     * 统计工单最近总数和完成情况
     *
     * @return 结果
     */
    @GetMapping("/statistics")
    public Result<List<WorkStatistics>> statistics() {
        List<WorkStatistics> list = new ArrayList<>(7);
        LocalDate today = LocalDate.now();
        for (int i = 6; i > -1; i--) {
            LambdaQueryWrapper<WorkOrder> lq = Wrappers.lambdaQuery();
            lq.between(WorkOrder::getCreateTime, today.minusDays(i), today.minusDays(i - 1));
            lq.eq(WorkOrder::getStatus, 1);
            LambdaQueryWrapper<WorkOrder> lq1 = Wrappers.lambdaQuery();
            lq1.le(WorkOrder::getCreateTime, today.minusDays(i - 1));
            int a = workOrderService.count(lq1);
            int b = workOrderService.count(lq);
            list.add(new WorkStatistics(a, b));
        }
        //Todo  假数据
        /*list = Arrays.asList(new WorkStatistics(56, 55),
                new WorkStatistics(55, 44),
                new WorkStatistics(48, 48),
                new WorkStatistics(50, 48),
                new WorkStatistics(39, 37),
                new WorkStatistics(67, 50),
                new WorkStatistics(59, 59));*/
        return new Result<>(Result.CodeStatus.OK, "获取工单完成情况统计", list);
    }

    /**
     * 统计工单类型情况
     *
     * @return 结果
     */
    @GetMapping("/statisticsType")
    public Result<Map<String, Long>> statisticsType() {
        List<WorkTypeStatisticsView> list = workTypeStatisticsViewService.list();
        Map<String, Long> map = list.stream().collect(Collectors.toMap(WorkTypeStatisticsView::getType, WorkTypeStatisticsView::getCount));
        //Todo  假数据
        map.put("薪资", 320L);
        map.put("工作", 240L);
        map.put("人事", 149L);
        map.put("损耗", 100L);
        map.put("其他", 59L);
        return new Result<>(Result.CodeStatus.OK, "获取工单类型统计", map);
    }
}
