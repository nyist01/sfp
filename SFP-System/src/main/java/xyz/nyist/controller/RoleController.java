package xyz.nyist.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import xyz.nyist.entity.Role;
import xyz.nyist.entity.RolePermission;
import xyz.nyist.sevice.RolePermissionService;
import xyz.nyist.sevice.RoleService;
import xyz.nyist.sevice.UserRoleViewService;
import xyz.nyist.view.UserRoleView;
import xyz.nyist.view.UserSimple;
import xyz.nyist.vo.Result;
import xyz.nyist.vo.RoleVo;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/23 19:31
 */
@RestController
@RequestMapping("/role")
public class RoleController {

    private final RolePermissionService rolePermissionService;
    private final UserRoleViewService userRoleViewService;
    private final RoleService roleService;

    public RoleController(RoleService roleService, UserRoleViewService userRoleViewService, RolePermissionService rolePermissionService) {
        this.roleService = roleService;
        this.userRoleViewService = userRoleViewService;
        this.rolePermissionService = rolePermissionService;
    }

    @GetMapping("/getChild")
    public Result<Set<Role>> getChile(UserSimple userSimple) {
        List<UserRoleView> roles = userRoleViewService.list(new QueryWrapper<UserRoleView>().eq("user_id", userSimple.getId()));
        Set<Role> set = new HashSet<>();
        roles.forEach(i -> set.addAll(roleService.findChild(i.getRoleId())));
        return new Result<>(Result.CodeStatus.OK, "获取所有子角色", set);
    }

    @PostMapping("/update")
    public Result<Void> update(@RequestBody List<RoleVo> list) {

        List<Integer> ids = list.stream().map(RoleVo::getId).collect(Collectors.toList());
        List<Role> roleList = roleService.list();
        //筛选出数据库中角色在本次提交中不存在(删除掉的)
        List<Role> difference = roleList.stream().filter(role -> !ids.contains(role.getId())).collect(Collectors.toList());
        if (!difference.isEmpty()) {
            roleService.removeByIds(difference.stream().map(Role::getId).collect(Collectors.toList()));
        }

        //根据参数中的List<RoleVo>构建List<Role>
        roleList = list.stream().map(Role::new).collect(Collectors.toList());
        roleService.saveOrUpdateBatch(roleList);

        List<RolePermission> permissions = new ArrayList<>();
        roleList.forEach(role -> {
            role.getPermissionIds().forEach(i -> {
                if (i != null) {
                    permissions.add(new RolePermission(null, role.getId(), i));
                }
            });
        });
        rolePermissionService.remove(new QueryWrapper<RolePermission>().ne("id", 0));
        if (!permissions.isEmpty()) {
            rolePermissionService.saveBatch(permissions);
        }
        return new Result<>(Result.CodeStatus.OK, "操作成功");
    }
}
