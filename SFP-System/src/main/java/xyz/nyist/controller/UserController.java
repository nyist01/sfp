package xyz.nyist.controller;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.StringUtils;
import xyz.nyist.entity.*;
import xyz.nyist.sevice.*;
import xyz.nyist.utils.FileUtil;
import xyz.nyist.utils.UserUtil;
import xyz.nyist.view.UserDetails;
import xyz.nyist.view.UserRoleView;
import xyz.nyist.view.UserSimple;
import xyz.nyist.vo.CompleteInfo;
import xyz.nyist.vo.PageQuery;
import xyz.nyist.vo.Result;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.interceptor.TransactionAspectSupport;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

import javax.annotation.Resource;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/1 18:29
 */
@Slf4j
@RestController
@RequestMapping("/user")
public class UserController {

    @Value("${file-path}")
    private String path;
    private static final String DEFAULT_CREDENTIALS = "123456";
    private static final String DEFAULT_AVATAR = "https://wpimg.wallstcn.com/f778738c-e4f8-4870-b634-56703b4acafe.gif";

    @Resource
    private BCryptPasswordEncoder bCryptPasswordEncoder;
    private final RoleService roleService;
    private final FilesService filesService;
    private final UserUtil userUtil;
    private final SecurityService securityService;
    private final UserRoleService userRoleService;
    private final UserRoleViewService userRoleViewService;
    private final UserDetailsService userDetailsService;
    private final UserService userService;
    private final UserSimpleService userSimpleService;
    private final FileUtil fileUtil;
    private final DepartmentService departmentService;

    public UserController(UserService userService, UserDetailsService userDetailsService, UserRoleViewService userRoleViewService, FileUtil fileUtil, UserRoleService userRoleService, SecurityService securityService, UserSimpleService userSimpleService, UserUtil userUtil, DepartmentService departmentService, FilesService filesService, RoleService roleService) {
        this.userService = userService;
        this.userDetailsService = userDetailsService;
        this.userRoleViewService = userRoleViewService;
        this.fileUtil = fileUtil;
        this.userRoleService = userRoleService;
        this.securityService = securityService;
        this.userSimpleService = userSimpleService;
        this.userUtil = userUtil;
        this.departmentService = departmentService;
        this.filesService = filesService;
        this.roleService = roleService;
    }

    /**
     * 获取用户信息 根据token
     *
     * @param userSimple token解析出的信息
     * @return 信息
     */
    @GetMapping("/info")
    public Result<UserDetails> info(UserSimple userSimple) {
        return getDetails(userSimple.getId());
    }

    /**
     * 查询用户 不分页
     *
     * @param user 条件
     * @return 用户列表
     */
    @PostMapping("/select/noPage")
    public Result<List<User>> selectNoPage(@RequestBody User user) {
        //查询出列名≠credentials的数据
        return new Result<>(userService.list(new QueryWrapper<>(user).select(i -> !"credentials".equals(i.getProperty()))));
    }

    /**
     * 查询用户 分页
     *
     * @param pageQuery 分页
     * @return 用户列表
     */
    @PostMapping("/select/page")
    public Result<IPage<User>> selectPage(@RequestBody PageQuery<User> pageQuery) {
        return new Result<>(userService.page(pageQuery.getPageCondition(), new QueryWrapper<>(pageQuery.getT()).select(i -> !"credentials".equals(i.getProperty()))));
    }

    /**
     * 根据部门查询用户
     *
     * @param pageQuery 条件
     * @return 结果
     */
    @PostMapping("/select/detailsByDepartment")
    public Result<IPage<UserDetails>> getDetailsListByDepartment(@RequestBody PageQuery<UserDetails> pageQuery) {
        if (pageQuery.getT() == null || pageQuery.getT().getDepartmentId() == null) {
            return new Result<>(Result.CodeStatus.FAIL, "参数错误");
        }
        Collection<Integer> ids = departmentService.findChild(pageQuery.getT().getDepartmentId()).stream().map(Department::getId).collect(Collectors.toList());
        QueryWrapper<UserDetails> qw = new QueryWrapper<>(new UserDetails());
        qw.select(i -> !"credentials".equals(i.getProperty())).in("department_id", ids);
        IPage<UserDetails> page = userDetailsService.page(pageQuery.getPageCondition(), qw);
        page.getRecords().forEach(userDetails -> userDetails.setRoles(userRoleViewService.list(new QueryWrapper<UserRoleView>().select("role_id").eq("user_id", userDetails.getId()))));
        return new Result<>(page);
    }

    /**
     * 获取用户详细信息
     *
     * @param id 用户id
     * @return 详细信息
     */
    @GetMapping("/select/details")
    public Result<UserDetails> getDetails(Integer id) {
        UserDetails userDetails = userDetailsService.getById(id);
        userDetails.setFirstLogin(bCryptPasswordEncoder.matches(DEFAULT_CREDENTIALS, userDetails.getCredentials()));
        userDetails.setRoles(userRoleViewService.list(new QueryWrapper<UserRoleView>().eq("user_id", id)));
        return new Result<>(Result.CodeStatus.OK, "获取详细信息", userDetails);
    }

    /**
     * 删除用户
     *
     * @param userSimple token
     * @param ids        用户id
     * @return 结果
     */
    @PostMapping("/delete")
    public Result<Void> delete(UserSimple userSimple, @RequestBody List<Integer> ids) {
        if (ids.isEmpty()) {
            ids.add(0);
        }
        else {
            //根据用户id去得到用户头像路径，并删除
            ids.forEach(i -> filesService.removeByPath(userService.getById(i).getAvatar()));
        }
        return new Result<>(ids, "删除用户", userSimple.getPrincipal(), userService.removeByIds(ids));
    }

    /**
     * 修改用户信息(自己修改自己的)
     *
     * @param user       信息
     * @param userSimple token
     * @return 结果
     */
    @PostMapping("/update")
    public Result<Void> update(@RequestBody User user, UserSimple userSimple) {
        String oldAvatar = userService.getById(userSimple.getId()).getAvatar();
        if (!oldAvatar.equals(user.getAvatar())) {
            filesService.removeByPath(userService.getById(userSimple.getId()).getAvatar());
        }
        fileUtil.useFile(user.getAvatar());
        user.setId(userSimple.getId());
        return new Result<>(userSimple.getId(), "修改信息", userSimple.getPrincipal(), userService.updateById(user));
    }

    /**
     * 统计最近七天登录次数
     *
     * @return 结果
     */
    @GetMapping("/statistics")
    public Result<Integer[]> statistics() {
        Integer[] res = userUtil.statistics();
        //res = new Integer[]{5489, 6985, 4789, 6589, 4375, 6849, 7824};
        return new Result<>(Result.CodeStatus.OK, "获取登录次数统计", res);
    }

    /**
     * 添加用户
     *
     * @param userSimple token
     * @param user       用户信息
     * @return 结果
     */
    @PostMapping("/insert")
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> insert(UserSimple userSimple, @RequestBody User user) {
        if (!userService.list(new QueryWrapper<User>().eq("principal", user.getPrincipal())).isEmpty()) {
            return new Result<>(Result.CodeStatus.FAIL, "账号已存在");
        }
        user.setCredentials(bCryptPasswordEncoder.encode(DEFAULT_CREDENTIALS));
        user.setCreateTime(LocalDateTime.now());
        user.setUpdateTime(LocalDateTime.now());
        user.setAvatar(DEFAULT_AVATAR);
        try {
            userService.save(user);
            List<UserRole> list = user.getRoleIds().stream().map(i -> new UserRole(null, user.getId(), i)).collect(Collectors.toList());
            userRoleService.saveBatch(list);
            return new Result<>(user.getId(), "添加用户", userSimple.getPrincipal(), true);
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(Result.CodeStatus.FAIL, "添加失败");
        }
    }

    /**
     * 修改用户角色和部门
     *
     * @param user       用户
     * @param userSimple token
     * @return 结果
     */
    @PostMapping("/updateRole")
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> updateRole(@RequestBody User user, UserSimple userSimple) {
        try {
            userRoleService.remove(new QueryWrapper<UserRole>().eq("user_id", user.getId()));
            User finalUser = user;
            List<UserRole> list = user.getRoleIds().stream().map(i -> new UserRole(null, finalUser.getId(), i)).collect(Collectors.toList());
            userRoleService.saveBatch(list);
            user = new User(user.getId(), user.getDepartmentId());
            userService.updateById(user);
            return new Result<>(user.getId(), "修改用户", userSimple.getPrincipal(), true);
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(Result.CodeStatus.FAIL, "修改失败");
        }
    }

    /**
     * 用户首次登陆完善信息
     *
     * @param userSimple   token
     * @param completeInfo 信息
     * @return 结果
     */
    @PostMapping("/complete")
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> complete(UserSimple userSimple, @RequestBody CompleteInfo completeInfo) {
        User user = new User();
        user.setId(userSimple.getId());
        user.setTel(completeInfo.getTel());
        user.setMail(completeInfo.getMail());
        user.setCredentials(bCryptPasswordEncoder.encode(completeInfo.getPass()));
        try {
            userService.updateById(user);
            Security security = new Security(null, completeInfo.getIssue(), completeInfo.getAnswer(), user.getId());
            securityService.save(security);
            return new Result<>(Result.CodeStatus.OK, "操作成功");
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(Result.CodeStatus.FAIL, "网络异常");
        }
    }

    /**
     * 找回密码第一步
     *
     * @param principal 账号
     * @return 密保问题
     */
    @GetMapping("/forget/getSecurity")
    public Result<Security> getSecurity(String principal) {
        UserSimple userSimple = userSimpleService.getOne(new QueryWrapper<UserSimple>().eq("principal", principal));
        if (userSimple == null) {
            return new Result<>(Result.CodeStatus.FAIL, "账号不存在");
        }
        Security security = securityService.getOne(new QueryWrapper<Security>().select("issue").eq("user_id", userSimple.getId()));
        return new Result<>(Result.CodeStatus.OK, "查询密码问题", security);
    }

    /**
     * 找回密码第二步 发送验证码
     *
     * @param completeInfo 验证信息
     * @return 结果
     */
    @PostMapping("/forget/getCode")
    public Result<Void> getSecurity(@RequestBody CompleteInfo completeInfo) {
        QueryWrapper<User> qw = new QueryWrapper<>();
        qw.eq("principal", completeInfo.getPrincipal());
        qw.eq("tel", completeInfo.getTel());
        qw.eq("mail", completeInfo.getMail());
        User user = userService.getOne(qw);
        if (user == null) {
            return new Result<>(Result.CodeStatus.FAIL, "信息验证错误");
        }
        String answers = securityService.getOne(new QueryWrapper<Security>().select("answers").eq("user_id", user.getId())).getAnswers();
        if (answers != null && !answers.equals(completeInfo.getAnswer())) {
            return new Result<>(Result.CodeStatus.FAIL, "信息验证错误");
        }
        userUtil.setCode(user.getMail(), user.getId());
        return new Result<>(Result.CodeStatus.OK, "成功");
    }

    /**
     * 找回密码第三步  验证验证码
     *
     * @param completeInfo 验证信息
     * @return 结果
     */
    @PostMapping("/forget/verifyCode")
    public Result<Void> verifyCode(@RequestBody CompleteInfo completeInfo) {
        QueryWrapper<User> qw = new QueryWrapper<User>().eq("principal", completeInfo.getPrincipal());
        User user = userService.getOne(qw);
        return new Result<>("验证验证码", user.getPrincipal(), userUtil.verifyCode(user.getId(), completeInfo.getCode(), true));
    }

    /**
     * 找回密码第四步  设置密码
     *
     * @param completeInfo 信息
     * @return 结果
     */
    @PostMapping("/forget/updatePwd")
    public Result<Void> updatePwd(@RequestBody CompleteInfo completeInfo) {
        if (StringUtils.isEmpty(completeInfo.getPrincipal()) || StringUtils.isEmpty(completeInfo.getCode()) || StringUtils.isEmpty(completeInfo.getPass())) {
            return new Result<>(Result.CodeStatus.FAIL, "参数错误");
        }
        QueryWrapper<User> qw = new QueryWrapper<User>().eq("principal", completeInfo.getPrincipal());
        User user = userService.getOne(qw);
        if (userUtil.verifyCode(user.getId(), completeInfo.getCode(), false)) {
            userService.update(new UpdateWrapper<User>().eq("id", user.getId()).set("credentials", bCryptPasswordEncoder.encode(completeInfo.getPass())));
            return new Result<>(Result.CodeStatus.OK, "修改成功");
        }
        return new Result<>(Result.CodeStatus.FAIL, "操作超时");
    }

    /**
     * 批量添加
     *
     * @param userSimple   token
     * @param departmentId 部门id
     * @param file         文件
     * @return 结果
     */
    @PostMapping("/insertFile")
    @Transactional(rollbackFor = Exception.class)
    public Result<Void> insertFile(UserSimple userSimple, Integer departmentId, MultipartFile file) {
        if (departmentId == null || file == null) {
            return new Result<>(Result.CodeStatus.FAIL, "参数错误");
        }
        try {
            StringBuilder sb = new StringBuilder();
            Set<String> roleNames = new HashSet<>();
            //获取该用户的所有子角色名字
            userRoleService.list(new QueryWrapper<UserRole>().eq("user_id", userSimple.getId())).forEach(userRole -> {
                roleNames.addAll(roleService.findChild(userRole.getRoleId()).stream().map(Role::getName).collect(Collectors.toList()));
            });
            Workbook workbook = WorkbookFactory.create(file.getInputStream());
            Sheet sheet = workbook.getSheetAt(0);
            String principal = LocalDateTime.now().format(DateTimeFormatter.ofPattern("MMddHHmm"));
            int j = 0;
            int success = 0;
            for (int i = 0; i < sheet.getLastRowNum(); i++) {
                Row row = sheet.getRow(i + 1);
                String nickname = row.getCell(0).getStringCellValue();
                Collection<String> roles = Arrays.asList(row.getCell(1).getStringCellValue().split(","));
                if (roleNames.containsAll(roles)) {
                    //判断账号是否存在
                    while (!userService.list(new QueryWrapper<User>().eq("principal", principal + j)).isEmpty()) {
                        j++;
                    }
                    User user = new User(principal + j++, DEFAULT_CREDENTIALS, nickname, DEFAULT_AVATAR, departmentId);
                    userService.save(user);
                    roleService.getId(row.getCell(1).getStringCellValue()).forEach(id -> userRoleService.save(new UserRole(null, user.getId(), id)));
                    success++;
                }
                else {
                    sb.append("姓名:").append(nickname).append("    角色: ").append(roles).append("不符<br/><br/>");
                }
            }
            sb.append("共:").append(sheet.getLastRowNum()).append("个,成功:").append(success).append("个");
            return new Result<>(Result.CodeStatus.OK, sb.toString());
        } catch (Exception e) {
            e.printStackTrace();
            TransactionAspectSupport.currentTransactionStatus().setRollbackOnly();
            return new Result<>(Result.CodeStatus.FAIL, "文件格式有误");
        }
    }

    /**
     * 自我修改密码
     *
     * @param userSimple token
     * @return 结果
     */
    @PostMapping("/updatePwd")
    public Result<Void> updatePwd(UserSimple userSimple, String pwd, String npwd) {
        if (StringUtils.isEmpty(pwd) || StringUtils.isEmpty(npwd)) {
            return new Result<>(Result.CodeStatus.FAIL, "请输入密码");
        }
        QueryWrapper<User> qw = new QueryWrapper<User>().eq("principal", userSimple.getPrincipal());
        User user = userService.getOne(qw);
        if (!bCryptPasswordEncoder.matches(pwd, user.getCredentials())) {
            return new Result<>(Result.CodeStatus.FAIL, "密码错误");
        }
        userService.update(new UpdateWrapper<User>().eq("id", user.getId()).set("credentials", bCryptPasswordEncoder.encode(npwd)));
        return new Result<>(Result.CodeStatus.OK, "修改成功");
    }

    @GetMapping("/getAvatar")
    public Result<String> getAvatar(Integer id) {
        UserDetails userSimple = userDetailsService.getById(id);
        return new Result<>(userSimple.getAvatar());
    }
}








