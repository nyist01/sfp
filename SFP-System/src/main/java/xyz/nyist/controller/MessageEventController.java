package xyz.nyist.controller;

import com.corundumstudio.socketio.SocketIOClient;
import com.corundumstudio.socketio.annotation.OnConnect;
import com.corundumstudio.socketio.annotation.OnDisconnect;
import com.corundumstudio.socketio.annotation.OnEvent;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;
import xyz.nyist.entity.Message;
import xyz.nyist.utils.MessageUtil;
import xyz.nyist.vo.Msg;
import xyz.nyist.vo.PageQuery;

import javax.annotation.Resource;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2019/11/4 14:00
 */
@Slf4j
@Component
public class MessageEventController {

    private static final String UNREAD_MSG_KEY = "sfp:unread:";
    static ConcurrentHashMap<String, SocketIOClient> webSocketMap = new ConcurrentHashMap<>();

    @Resource
    private MessageUtil messageUtil;
    @Resource
    private RedisTemplate<String, Msg.UnreadMessage> redisTemplate;

    @OnConnect
    public void onConnect(SocketIOClient client) {
        log.info("客户端:" + client.getSessionId() + "已连接");
    }

    @OnDisconnect
    public void onDisconnect(SocketIOClient client) {
        Collection<SocketIOClient> col = webSocketMap.values();
        while (col.contains(client)) {
            col.remove(client);
        }
        log.info("客户端:" + client.getSessionId() + "断开连接");
    }

    @OnEvent(value = "loginEvent")
    public void loginEvent(SocketIOClient client, Message message) {
        webSocketMap.put(message.getFrom().toString(), client);
    }

    @OnEvent(value = "logoutEvent")
    public void logoutEvent(Message message) {
        webSocketMap.remove(message.getFrom().toString());
        log.info("用户(id):" + message.getFrom() + "下线");
    }

    @OnEvent(value = "selectChat")
    public void selectChat(SocketIOClient client, PageQuery<Map<String, Integer>> pageQuery) {
        client.sendEvent("getChat", messageUtil.getMessage(pageQuery));
    }

    @OnEvent(value = "sendEvent")
    public void sendEvent(Msg msg) {
        Message message = msg.getMessage();
        Msg.UnreadMessage unreadMessage = msg.getUnreadMessage();
        SocketIOClient c = webSocketMap.get(message.getTo().toString());
        if (c != null) {
            c.sendEvent("receiveEvent", message);
        } else {
            //对方不在线，添加到未读消息
            //获取所有未读消息
            List<Msg.UnreadMessage> list = redisTemplate.opsForList().range(UNREAD_MSG_KEY + msg.getMessage().getTo(), 0, -1);
            //删除redis中对方未读消息(无法直接修改redis值)
            redisTemplate.delete(UNREAD_MSG_KEY + msg.getMessage().getTo());
            if (list != null) {
                //修改对方未读消息中我的消息
                Msg.UnreadMessage u = null;
                Iterator<Msg.UnreadMessage> it = list.iterator();
                while (it.hasNext()) {
                    Msg.UnreadMessage t = it.next();
                    if (t.getId().equals(message.getFrom())) {
                        u = t;
                        it.remove();
                    }
                }
                if (u == null) {
                    u = unreadMessage;
                    u.setBadge(1);
                } else {
                    u.setBadge(u.getBadge() + 1);
                }
                list.add(u);
            } else {
                list = new ArrayList<>(1);
                unreadMessage.setBadge(1);
                list.add(unreadMessage);
            }
            redisTemplate.opsForList().leftPushAll(UNREAD_MSG_KEY + msg.getMessage().getTo(), list);
        }
        messageUtil.addMessage(msg.getMessage());
    }
}
