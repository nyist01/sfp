package xyz.nyist.configs.jwt;


import com.alibaba.fastjson.JSON;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.impl.DefaultClaims;
import xyz.nyist.utils.UserUtil;
import xyz.nyist.view.UserSimple;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.Serializable;
import java.util.Date;

/**
 * @author Silence
 * @Description: jwt工具类
 */
@Slf4j
@Component("jwtTokenConfig")
public class JwtTokenConfig implements Serializable {
    private static final String CLAIM_KEY_USER = "sub";
    private static final String CLAIM_KEY_IP = "ip";
    private static final long serialVersionUID = -8305152446124853696L;

    @Resource
    private UserUtil userUtil;

    /**
     * 密钥
     */
    private final static String BASE64_SECRET = "%^&*()draw@#$%^&*!~sfp";
    /**
     * token有效期 24h
     */
    private final static Long EXPIRATION = 24 * 60 * 60 * 1000L;
    /**
     * token有效时间不足5分钟就刷新
     */
    private final static Long REFRESH_TIME = 5 * 60 * 1000L;


    /**
     * 创建token
     *
     * @param claims 内容
     * @return token
     */
    private String createToken(Claims claims) {
        UserSimple userSimple = (UserSimple) claims.get(CLAIM_KEY_USER);
        userUtil.addLoginNumber(userSimple.getId());
        Date expirationDate = new Date(System.currentTimeMillis() + EXPIRATION);
        return Jwts.builder().setClaims(claims).setExpiration(expirationDate).signWith(SignatureAlgorithm.HS512, BASE64_SECRET).compact();
    }

    /**
     * 添加令牌
     *
     * @param userSimple 主体
     * @param ip         请求ip
     * @return jwt
     */
    public String generateToken(UserSimple userSimple, String ip) {
        Claims claims = new DefaultClaims();
        claims.setId(userSimple.getCredentials());
        claims.put(CLAIM_KEY_USER, userSimple);
        claims.put(CLAIM_KEY_IP, ip);
        return createToken(claims);
    }

    /**
     * 验证令牌
     *
     * @param requestIp 这次请求的ip
     * @return 如果token有效且有效期不足5分钟就返回新的token，否则返回false或true
     */
    public CheckResult<String> validateToken(Claims claims, String requestIp) {
        try {
            Date expiration = claims.getExpiration();
            String tokenIp = claims.get(CLAIM_KEY_IP, String.class);
            log.debug("token中的ip：" + tokenIp);
            log.debug("本次请求的ip：" + requestIp);
            if (tokenIp.equals(requestIp)) {
                log.debug("token过期时间: " + expiration + " ---- 剩余有效时间: " + ((expiration.getTime() - System.currentTimeMillis()) / 1000) + "s");
                if (new Date(System.currentTimeMillis() + REFRESH_TIME).after(expiration)) {
                    return new CheckResult<>(true, createToken(claims));
                }
                else {
                    return new CheckResult<>(true, null);
                }
            }
            else {
                log.warn("ip不匹配");
                return new CheckResult<>(false, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error("token解析失败");
            return new CheckResult<>(false, null);
        }
    }

    /**
     * 获取用户信息
     *
     * @param claims claims
     * @return 用户信息
     */
    public UserSimple getUser(Claims claims) {
        return JSON.parseObject(JSON.toJSONString(claims.get(CLAIM_KEY_USER)), UserSimple.class);
    }

    /**
     * 判断令牌是否过期
     *
     * @param token token
     * @return 是否过期
     */
    public CheckResult<Claims> isExpired(String token) {
        try {
            Claims claims = Jwts.parser().setSigningKey(BASE64_SECRET).parseClaimsJws(token).getBody();
            Date expiration = claims.getExpiration();
            log.debug("token过期时间: " + expiration + " ---- 剩余有效时间: " + ((expiration.getTime() - System.currentTimeMillis()) / 1000) + "s");
            return new CheckResult<>(expiration.after(new Date()), claims);
        } catch (Exception e) {
            return new CheckResult<>(false, null);
        }
    }


    @Data
    @AllArgsConstructor
    public static class CheckResult<T> {
        boolean success;
        T data;
    }


}
