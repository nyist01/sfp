package xyz.nyist.configs.mybatis;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;

/**
 * @Author: Silence
 * @Description: 自定义分页 初始值
 * @Date:Create：in 2020/3/12 22:05
 */
public class MyPage<T> extends Page<T> {

    /**
     * redis符合条件的总数-已取出数量
     */
    private long difference;

    /**
     * 原来的size
     */
    private long oldSize;

    @Override
    public long offset() {
        return (getCurrent() > 0 ? (getCurrent() - 1) * this.oldSize : 0) - difference;
    }

    public MyPage(long current, long size, long oldSize, long difference) {
        super(current, size);
        this.oldSize = oldSize;
        this.difference = difference;
    }
}
