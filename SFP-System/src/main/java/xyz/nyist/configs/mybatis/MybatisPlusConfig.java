package xyz.nyist.configs.mybatis;

import com.baomidou.mybatisplus.extension.plugins.PaginationInterceptor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Author: Silence
 * @Description: mybatis plus 分页配置
 * @Date:Create：in 2020/2/14 13:26
 */
@Configuration
@EnableTransactionManagement
public class MybatisPlusConfig {
    
    /**
     * 分页插件
     */
    @Bean
    public PaginationInterceptor paginationInterceptor() {
        PaginationInterceptor paginationInterceptor = new PaginationInterceptor();
        //你的最大单页限制数量，默认 500 条，小于 0 如 -1 不受限制
        paginationInterceptor.setLimit(500);
        return paginationInterceptor;
    }

}
