package xyz.nyist.configs;

import com.corundumstudio.socketio.SocketIOServer;
import com.corundumstudio.socketio.annotation.SpringAnnotationScanner;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2019/11/4 13:57
 */
@Slf4j
@Configuration
public class NettySocketIoConfig implements CommandLineRunner {
    @Value("${host}")
    private String host;


    /**
     * netty-socketIo服务器
     */
    @Bean("socketIoServer")
    public SocketIOServer socketIoServer() {
        com.corundumstudio.socketio.Configuration config = new com.corundumstudio.socketio.Configuration();
        //单次发送数据最大值1M
        config.setMaxFramePayloadLength(1024 * 1024);
        //socket连接数大小（如只监听一个端口boss线程组为1即可）
        config.setBossThreads(1);
        config.setWorkerThreads(100);
        //身份验证
        config.setAuthorizationListener(handshakeData -> true);
        //config.setHostname(host);
        config.setPort(8001);
        /*config.setTransports(Transport.POLLING, Transport.WEBSOCKET);
        config.setOrigin(":*:");*/
        return new SocketIOServer(config);
    }

    /**
     * 用于扫描netty-socketIo的注解，比如 @OnConnect、@OnEvent
     */
    @Bean
    public SpringAnnotationScanner springAnnotationScanner() {
        return new SpringAnnotationScanner(socketIoServer());
    }


    @Override
    public void run(String... strings) {
        socketIoServer().start();
        log.info("socket.io启动成功！");
    }
}
