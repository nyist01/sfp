package xyz.nyist.configs.security;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import xyz.nyist.sevice.UserRoleViewService;
import xyz.nyist.sevice.UserSimpleService;
import xyz.nyist.view.UserRoleView;
import xyz.nyist.view.UserSimple;
import org.springframework.security.authentication.AuthenticationProvider;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.List;

/**
 * @Author: Silence
 * @Description: 自定义登陆验证和授权
 * @Date:Create：in 2020/2/16 15:35
 */
@Component("customAuthenticationProvider")
public class CustomAuthenticationProvider implements AuthenticationProvider {

    @Resource
    private PasswordEncoder passwordEncoder;
    @Resource
    private UserSimpleService userSimpleService;
    @Resource
    private UserRoleViewService userRoleViewService;


    @Override
    public Authentication authenticate(Authentication authentication) throws AuthenticationException {
        UserSimple userSimple = (UserSimple) authentication;
        //查询数据库匹配的账号和身份
        QueryWrapper<UserSimple> qw = new QueryWrapper<UserSimple>().eq("is_admin", userSimple.getIsAdmin()).eq("principal", userSimple.getPrincipal());
        UserSimple user = userSimpleService.getOne(qw);
        //密码验证
        if (user == null || !passwordEncoder.matches(userSimple.getCredentials(), user.getCredentials())) {
            throw new BadCredentialsException("账号或密码错误");
        }
        //并从数据库查询权限
        List<UserRoleView> list = userRoleViewService.list(new QueryWrapper<UserRoleView>().eq("user_id", user.getId()));
        return new UserSimple(user, list);
    }

    @Override
    public boolean supports(Class<?> authentication) {
        //指定该认证处理器能对UserSimple对象进行认证
        return UserSimple.class.isAssignableFrom(authentication);
    }
}
