package xyz.nyist.configs.security;


import xyz.nyist.sevice.PermissionRoleViewService;
import xyz.nyist.utils.ArrayListUtil;
import xyz.nyist.view.PermissionRoleView;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpServletRequest;
import java.util.*;


/**
 * @Author: Silence
 * @Description: 自定义的资源（url）权限（角色）数据获取类
 * @Date:Create：in 2020/2/20 16:08
 */
@Slf4j
@Component("customFilterInvocationSecurityMetadataSource")
public class CustomFilterInvocationSecurityMetadataSource implements FilterInvocationSecurityMetadataSource {

    private final PermissionRoleViewService permissionRoleViewService;
    private static HashMap<String, Collection<ConfigAttribute>> map = null;
    List<PermissionRoleView> list = null;

    /**
     * 初始化路径角色map
     */
    public void getResourcePermission() {
        list = permissionRoleViewService.list();
        map = new HashMap<>(list.size());
        list.forEach(permissionTemp -> {
            Set<ConfigAttribute> set = new HashSet<>();
            set.add(new SecurityConfig(getEnName(permissionTemp)));
            getSecurityConfig(permissionTemp.getPermissionParentId(), permissionTemp.getRoleParentId(), set);
            map.put(permissionTemp.getUrl(), set);
        });
        log.debug("接口访问权限: " + map);
    }

    /**
     * 递归设置url的role
     *
     * @param permissionParentId 父权限id
     * @param roleParentId       父角色id
     * @param securityConfigs    角色容器
     */
    private void getSecurityConfig(Integer permissionParentId, Integer roleParentId, Set<ConfigAttribute> securityConfigs) {
        if (permissionParentId != null && permissionParentId != 0) {
            PermissionRoleView temp = list.stream().filter(p -> permissionParentId.equals(p.getPermissionId())).collect(ArrayListUtil.toSingleton());
            securityConfigs.add(new SecurityConfig(getEnName(temp)));
            getSecurityConfig(temp.getPermissionParentId(), null, securityConfigs);
        }
        if (roleParentId != null && roleParentId != 0) {
            PermissionRoleView temp = list.stream().filter(p -> roleParentId.equals(p.getRoleId())).collect(ArrayListUtil.toSingleton());
            securityConfigs.add(new SecurityConfig(getEnName(temp)));
            getSecurityConfig(null, temp.getRoleParentId(), securityConfigs);
        }
    }


    private String getEnName(PermissionRoleView permissionRoleView) {
        //当前url如果没有指定角色 就给 root
        if (permissionRoleView == null || permissionRoleView.getEnName() == null) {
            return "root";
        }
        else {
            return permissionRoleView.getEnName();
        }
    }

    CustomFilterInvocationSecurityMetadataSource(PermissionRoleViewService permissionRoleViewService) {
        this.permissionRoleViewService = permissionRoleViewService;
        getResourcePermission();
    }

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        HttpServletRequest request = ((FilterInvocation) object).getHttpRequest();
        /*
          遍历每个资源（url），如果与用户请求的资源（url）匹配，则返回该资源（url）所需要的权限（角色）集合，
          如果全都不匹配，则表示用户请求的资源（url)不需要权限（角色）即可访问
         */
        return map.entrySet().stream()
                .filter(item -> new AntPathRequestMatcher(item.getKey()).matches(request))
                .map(Map.Entry::getValue).collect(ArrayListUtil.toSingleton());

        /*
        以下代码等同于上边的代码
        for (String url : map.keySet()) {
            if (new AntPathRequestMatcher(url).matches(request)) {
                return map.get(url);
            }
        }
        return null;*/
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}

