package xyz.nyist.configs.security;


import io.jsonwebtoken.Claims;
import xyz.nyist.configs.jwt.JwtTokenConfig;
import xyz.nyist.utils.IpUtil;
import xyz.nyist.view.UserSimple;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.annotation.Resource;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * @author Silence
 * @Description: 自定义token验证
 */
@Slf4j
@Component("customOncePerRequestFilter")
public class CustomOncePerRequestFilter extends OncePerRequestFilter {

    @Resource
    private JwtTokenConfig jwtTokenConfig;
    private static final String TOKEN_HEAD = "Bearer:";
    private static final String HEAD_KEY = "Authorization";

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain) throws ServletException, IOException {
        log.info("本次访问路径--------------" + request.getRequestURI());
        String authHeader = request.getHeader(HEAD_KEY);
        if (authHeader != null && authHeader.startsWith(TOKEN_HEAD) && SecurityContextHolder.getContext().getAuthentication() == null) {
            String token = authHeader.substring(7);
            //token是否过期
            JwtTokenConfig.CheckResult<Claims> checkResult = jwtTokenConfig.isExpired(token);
            if (checkResult.isSuccess()) {
                Claims claims = checkResult.getData();
                //从请求带的token中获取用户信息
                UserSimple userDetails = jwtTokenConfig.getUser(claims);
                //验证本次请求的token的合法性
                JwtTokenConfig.CheckResult<String> validateResult = jwtTokenConfig.validateToken(claims, IpUtil.getRealIp(request));
                if (validateResult.isSuccess()) {
                    if (validateResult.getData() != null) {
                        response.setHeader("token", validateResult.getData());
                    }
                    SecurityContextHolder.getContext().setAuthentication(userDetails);
                }
            }
        }
        filterChain.doFilter(request, response);
    }


}
