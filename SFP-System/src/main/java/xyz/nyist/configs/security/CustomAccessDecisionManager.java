package xyz.nyist.configs.security;


import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

import java.util.Collection;

/**
 * @Author: Silence
 * @Description: 自定义的权限控制管理器
 * @Date:Create：in 2020/2/20 16:26
 */
@Component("customAccessDecisionManager")
public class CustomAccessDecisionManager implements AccessDecisionManager {

    /**
     * 判断是否有权限
     *
     * @param auth             包含了UserDetails用户信息
     * @param object           包含了request请求信息
     * @param configAttributes 由CustomFilterInvocationSecurityMetadataSource.getAttributes(object)返回的请求的资源（url）所需要的权限（角色）集合
     */
    @Override
    public void decide(Authentication auth, Object object, Collection<ConfigAttribute> configAttributes) throws AccessDeniedException, InsufficientAuthenticationException {
        /*
         * 如果请求的资源不需要权限，则直接放行
         */
        if (configAttributes == null || configAttributes.size() <= 0) {
            return;
        }
        /*
         * 判断用户所拥有的权限是否是资源所需要的权限之一，如果是则放行，否则拦截
         */
        for (ConfigAttribute configAttribute : configAttributes) {
            String needRole = configAttribute.getAttribute();
            for (GrantedAuthority grantRole : auth.getAuthorities()) {
                if (needRole.trim().equals(grantRole.getAuthority().trim())) {
                    return;
                }
            }
        }
        throw new AccessDeniedException("没有权限");
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }
}

