package xyz.nyist.configs.security;

import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.SecurityConfigurerAdapter;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.web.DefaultSecurityFilterChain;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.stereotype.Component;
import xyz.nyist.configs.jwt.JwtTokenConfig;
import xyz.nyist.utils.IpUtil;
import xyz.nyist.view.UserSimple;
import xyz.nyist.vo.Result;

/**
 * @Author: Silence
 * @Description: 自定义登陆返回结果
 * @Date:Create：in 2020/2/16 16:02
 */
@Component("customAuthenticationConfig")
public class CustomAuthenticationConfig extends SecurityConfigurerAdapter<DefaultSecurityFilterChain, HttpSecurity> {

    @Autowired
    private JwtTokenConfig jwtTokenConfig;
    @Autowired
    private CustomAuthenticationProvider customAuthenticationProvider;
    @Autowired
    private CustomOncePerRequestFilter customOncePerRequestFilter;
    @Autowired
    private AuthenticationManager authenticationManager;

    @Override
    public void configure(HttpSecurity http) {
        http.authenticationProvider(customAuthenticationProvider);
        //添加验证TOKEN拦截器
        http.addFilterBefore(this.customOncePerRequestFilter, UsernamePasswordAuthenticationFilter.class);
        //添加登录结果拦截器
        http.addFilterBefore(customAuthenticationFilter(), UsernamePasswordAuthenticationFilter.class);
    }

    private CustomAuthenticationFilter customAuthenticationFilter() {
        CustomAuthenticationFilter filter = new CustomAuthenticationFilter();
        //登陆成功
        filter.setAuthenticationSuccessHandler((req, resp, authentication) -> {
            UserSimple userSimple = (UserSimple) authentication;
            String token = jwtTokenConfig.generateToken(userSimple, IpUtil.getRealIp(req));
            resp.setContentType("application/json;charset=UTF-8");
            resp.setHeader("token", token);
            resp.getWriter().write(JSON.toJSONString(new Result<>(Result.CodeStatus.OK, "登陆成功")));
        });
        //登陆失败
        filter.setAuthenticationFailureHandler((req, resp, e) -> {
            resp.setContentType("application/json;charset=UTF-8");
            String msg = "账号或密码错误".equals(e.getMessage()) ? e.getMessage() : "网络异常";
            resp.getWriter().write(JSON.toJSONString(new Result<>(Result.CodeStatus.FAIL, msg)));
        });
        filter.setAuthenticationManager(authenticationManager);
        return filter;
    }

}
