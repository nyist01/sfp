package xyz.nyist.configs.security;

import com.fasterxml.jackson.databind.ObjectMapper;
import xyz.nyist.view.UserSimple;
import lombok.Data;
import org.springframework.http.MediaType;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.InputStream;

/**
 * @Author: Silence
 * @Description: 自定义登录参数接受方式
 */
public class CustomAuthenticationFilter extends UsernamePasswordAuthenticationFilter {
    @Override
    public Authentication attemptAuthentication(HttpServletRequest request, HttpServletResponse response) throws AuthenticationException {
        //请求参数是json格式
        if (MediaType.APPLICATION_JSON_UTF8_VALUE.equalsIgnoreCase(request.getContentType()) || MediaType.APPLICATION_JSON_VALUE.equalsIgnoreCase(request.getContentType())) {
            ObjectMapper mapper = new ObjectMapper();
            UserSimple userSimple = null;
            try (InputStream is = request.getInputStream()) {
                LoginBean loginBean = mapper.readValue(is, LoginBean.class);
                userSimple = new UserSimple(loginBean.getUsername(), loginBean.getPassword(), loginBean.getIsAdmin());
                return this.getAuthenticationManager().authenticate(userSimple);
            } catch (IOException e) {
                e.printStackTrace();
                new UsernamePasswordAuthenticationToken("", "");
                return this.getAuthenticationManager().authenticate(userSimple);
            } finally {
                assert userSimple != null;
                userSimple.setDetails(authenticationDetailsSource.buildDetails(request));
            }
        }
        else {
            return super.attemptAuthentication(request, response);
        }
    }


    @Data
    private static class LoginBean {
        private String username;
        private String password;
        private Byte isAdmin;
    }
}
