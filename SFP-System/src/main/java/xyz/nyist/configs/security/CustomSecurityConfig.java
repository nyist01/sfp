package xyz.nyist.configs.security;

import com.alibaba.fastjson.JSON;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import xyz.nyist.vo.Result;

/**
 * @author Silence
 * @Description: security配置
 */
@Slf4j
@Configuration
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class CustomSecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    private CustomAuthenticationConfig customAuthenticationConfig;

    /**
     * @Description: security总体配置
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        //禁用csrf模式
        http.csrf().disable().cors().and().apply(customAuthenticationConfig);
        // 表单登录身份认证
        http.formLogin().permitAll();

        //退出登录
        http.logout()
                .logoutRequestMatcher(new AntPathRequestMatcher("/user/logout", "POST"))
                .logoutSuccessHandler((req, resp, auth) -> {
                    resp.setContentType("application/json;charset=UTF-8");
                    resp.getWriter().write(JSON.toJSONString(new Result<>(Result.CodeStatus.OK, "已退出登录")));
                })
                .clearAuthentication(true)
                .invalidateHttpSession(true);

        //放行静态资源
        http.authorizeRequests().antMatchers("/user/forget/**", "/test/**", "/files/**").permitAll();
        //添加拦截验证
        http.authorizeRequests().anyRequest().authenticated();

        //不生成session
        http.sessionManagement().sessionCreationPolicy(SessionCreationPolicy.STATELESS);
        //自定义权限不足返回结果
        http.exceptionHandling().accessDeniedHandler((req, resp, auth) -> {
            log.warn("权限不足  访问路径: " + req.getRequestURI());
            resp.setContentType("application/json;charset=UTF-8");
            resp.getWriter().write(JSON.toJSONString(new Result<>(Result.CodeStatus.ILLEGAL_TOKEN, "权限不足")));
        });
        //自定义未登录返回结果
        http.exceptionHandling().authenticationEntryPoint((req, resp, e) -> {
            log.warn("未登录  访问路径: " + req.getRequestURI());
            resp.setContentType("application/json;charset=UTF-8");
            resp.getWriter().write(JSON.toJSONString(new Result<>(Result.CodeStatus.TOKEN_EXPIRED, "身份认证已过期")));
        });

    }


    @Bean
    @Override
    public AuthenticationManager authenticationManagerBean() throws Exception {
        return super.authenticationManagerBean();
    }

    /**
     * @Description: 密码加密bean
     */
    @Bean
    public BCryptPasswordEncoder passwordEncoder() {
        return new BCryptPasswordEncoder();
    }


}
