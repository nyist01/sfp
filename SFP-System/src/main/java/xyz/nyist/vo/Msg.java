package xyz.nyist.vo;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalDateTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalDateTimeSerializer;
import xyz.nyist.entity.Message;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import java.time.LocalDateTime;

/**
 * @Author: Silence
 * @Description: 离线消息
 * @Date:Create：in 2020/3/25 11:10
 */
@Data
public class Msg {

    private Message message;
    private UnreadMessage unreadMessage;

    @Data
    public static class UnreadMessage {
        private Integer id;
        private String avatar;
        //未读消息数量
        private Integer badge;
        private String nickname;
        private String msg;
        @JsonDeserialize(using = LocalDateTimeDeserializer.class)
        @JsonSerialize(using = LocalDateTimeSerializer.class)
        @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
        @JsonFormat(timezone = "GMT+8", pattern = "yyyy-MM-dd HH:mm:ss")
        @TableField(value = "`time`")
        private LocalDateTime time;
    }

}
