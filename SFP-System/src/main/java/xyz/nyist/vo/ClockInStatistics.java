package xyz.nyist.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Silence
 * @Description: 打卡统计
 * @Date:Create：in 2020/3/17 18:09
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class ClockInStatistics {
    private Integer total;
    private Integer onWork;
    private Integer offWork;
    private Integer newUser;
}
