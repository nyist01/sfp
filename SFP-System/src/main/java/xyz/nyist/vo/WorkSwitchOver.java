package xyz.nyist.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/7 16:56
 */
@Data
@NoArgsConstructor
public class WorkSwitchOver {

    private Integer id;
    private Integer workOrderId;
    private Integer userId;
    private String content;
    private String departmentId;
}
