package xyz.nyist.vo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/18 8:23
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
public class WorkStatistics {
    private Integer total;
    private Integer finish;
}
