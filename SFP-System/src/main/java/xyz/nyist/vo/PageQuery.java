package xyz.nyist.vo;

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;

/**
 * @Author: Silence
 * @Description: 分页查询
 * @Date:Create：in 2019/10/12 14:29
 */
@Data
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class PageQuery<T> implements Serializable {

    private Integer page = 1;
    private Integer size = 10;
    private T t;
    private String keywords;

    public Page<T> getPageCondition() {
        return new Page<>(this.page, this.size);
    }

}
