package xyz.nyist.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/8 10:56
 */
@Data
@NoArgsConstructor
public class WorkOrderInsert {
    private String title;
    private String content;
    private String type;
    private Integer emergencyLevel;
    private int departmentId;
    private int userId;
    private List<Integer> filesId;
}
