package xyz.nyist.vo;

import lombok.Data;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/24 18:19
 */
@Data
public class CompleteInfo {
    private String pass;
    private String tel;
    private String mail;
    private String answer;
    private String issue;
    private String principal;
    private String code;
}
