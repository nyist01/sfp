package xyz.nyist.vo;

import lombok.Data;

import java.util.List;

/**
 * @Author: silence
 * @Description:
 * @Date:Create：in 2020/3/28 20:23
 */
@Data
public class RoleVo {
    private Integer id;
    private Integer parentId;
    private String name;
    private String enName;
    List<Integer> permissions;
}
