package xyz.nyist.vo;

import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.util.List;

/**
 * @Author: Silence
 * @Description: 处理工单vo
 * @Date:Create：in 2020/3/7 16:19
 */

@Data
@NoArgsConstructor
public class WorkOrderDispose implements Serializable {
    private Integer id;
    private Integer workOrderId;
    private String content;
    private String sketch;
    private List<Integer> filesId;
    private boolean end;
}
