package xyz.nyist.vo;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

import java.io.Serializable;
import java.util.Collection;

/**
 * @Author: Silence
 * @Description: 后端返回数据
 * @Date:Create：in 2020/2/14 13:37
 */
@Data
@ToString
@NoArgsConstructor
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result<T> implements Serializable {
    private Integer code;
    private String message;
    private T data;
    @JsonIgnore
    private String logs;

    public Result(Integer code, String message, T data) {
        this.code = code;
        this.message = message;
        this.data = data;
    }

    public Result(T data) {
        this.code = CodeStatus.OK;
        this.message = "操作成功";
        this.data = data;
    }

    public Result(Integer code, String message) {
        this.code = code;
        this.message = message;
    }

    public Result(Integer code, String message, String logs) {
        this.code = code;
        this.message = message;
        this.logs = logs;
    }

    public Result(Integer id, String message, String username, boolean b) {
        this.code = b ? CodeStatus.OK : CodeStatus.FAIL;
        this.message = message + (b ? "成功" : "失败");
        this.logs = "用户:" + username + "--" + message + (id == null ? "" : "(id:" + id + ")") + "--" + (b ? "成功" : "失败");
    }


    public Result(Collection<Integer> ids, String message, String username, boolean b) {
        this.code = b ? CodeStatus.OK : CodeStatus.FAIL;
        this.message = message + (b ? "成功" : "失败");
        this.logs = "用户:" + username + "--" + message + (ids == null ? "" : "(id:" + ids + ")") + "--" + (b ? "成功" : "失败");
    }

    public Result(String message, String username, boolean b) {
        this.code = b ? CodeStatus.OK : CodeStatus.FAIL;
        this.message = message + (b ? "成功" : "失败");
        this.logs = "用户:" + username + "--" + message + "--" + (b ? "成功" : "失败");
    }

    public static class CodeStatus {
        /**
         * 请求成功
         */
        public static final int OK = 20000;
        /**
         * 请求失败
         */
        public static final int FAIL = 50000;
        /**
         * 非法token
         */
        public static final int ILLEGAL_TOKEN = 50008;
        /**
         * 已在其他地方登录
         */
        public static final int OTHER_CLIENTS_LOGGED_IN = 50012;
        /**
         * token超时
         */
        public static final int TOKEN_EXPIRED = 50014;
    }
}
