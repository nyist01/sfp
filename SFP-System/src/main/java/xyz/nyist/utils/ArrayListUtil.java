package xyz.nyist.utils;

import java.util.stream.Collector;
import java.util.stream.Collectors;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 13:55
 */
public class ArrayListUtil {
    public static <T> Collector<T, ?, T> toSingleton() {
        return Collectors.collectingAndThen(Collectors.toList(), list -> list.size() > 0 ? list.get(0) : null);
    }
}
