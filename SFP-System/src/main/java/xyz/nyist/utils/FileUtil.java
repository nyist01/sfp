package xyz.nyist.utils;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import xyz.nyist.entity.Files;
import xyz.nyist.sevice.FilesService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.io.File;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

/**
 * @Author: Silence
 * @Description: 文件工具类
 * @Date:Create：in 2020/3/21 20:37
 */
@Slf4j
@Component("fileUtil")
public class FileUtil {

    @Value("${file-path}")
    private String path;
    private static final String IDS_KEY = "sfp:ids";

    @Resource
    private FilesService filesService;
    @Resource
    private RedisTemplate<String, Integer> redisTemplate;

    public void useFile(String content) {
        List<Integer> ids = MyStringUtil.getImgSrcId(content);
        if (!ids.isEmpty()) {
            redisTemplate.opsForList().leftPushAll(IDS_KEY, ids);
        }
    }

    public void useFile(Integer id) {
        if (id != null) {
            redisTemplate.opsForList().leftPush(IDS_KEY, id);
        }
    }

    public void useFile(List<Integer> ids) {
        if (!ids.isEmpty()) {
            redisTemplate.opsForList().leftPushAll(IDS_KEY, ids);
        }
    }

    @Async
    public void clearUpFile() {
        log.info("执行file使用情况更新开始..........");
        List<Integer> ids = redisTemplate.opsForList().range(IDS_KEY, 0, -1);
        if (ids != null) {
            List<Files> filesList = ids.stream().map(Files::new).collect(Collectors.toList());
            filesService.updateBatchById(filesList);
        }
        log.info("执行file使用情况更新结束..........");
        log.info("清理未使用的文件开始..............");
        List<Files> filesList = filesService.list(new QueryWrapper<Files>().lt("create_data", LocalDateTime.now().minusDays(1)).eq("is_use", 0));
        List<Integer> ids1 = filesList.stream().map(Files::getId).collect(Collectors.toList());
        if (!ids1.isEmpty()) {
            filesService.removeByIds(ids1);
        }
        filesList.forEach(files -> log.warn("删除文件" + files.getPath() + "----" + new File(this.path + files.getPath()).delete()));
        redisTemplate.delete(IDS_KEY);
        log.info("清理未使用的文件结束..............");
    }
}
