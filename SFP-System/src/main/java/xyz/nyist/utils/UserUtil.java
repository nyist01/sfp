package xyz.nyist.utils;

import com.baomidou.mybatisplus.core.conditions.update.LambdaUpdateWrapper;
import xyz.nyist.entity.User;
import xyz.nyist.sevice.UserService;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.concurrent.TimeUnit;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/18 19:23
 */
@Component
public class UserUtil {

    private static final String CODE_KEY = "sfp:code:";
    private static final String LOGIN_NUMBER_KEY = "sfp:loginNumber:";
    @Resource
    private RedisTemplate<String, String> redisTemplate;

    private final SendMailUtil sendMailUtil;
    private final UserService userService;

    public UserUtil(UserService userService, SendMailUtil sendMailUtil) {
        this.userService = userService;
        this.sendMailUtil = sendMailUtil;
    }

    /**
     * 添加当天登录次数
     * 修改用户登录次数和最后登陆时间 (异步)
     *
     * @param id 用户id
     */
    @Async
    public void addLoginNumber(Integer id) {
        //redis储存最近七天访问次数
        String today = LocalDate.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        String res = redisTemplate.opsForValue().get(LOGIN_NUMBER_KEY + today);
        int index = res == null ? 0 : Integer.parseInt(res);
        redisTemplate.opsForValue().set(LOGIN_NUMBER_KEY + today, String.valueOf((index + 1)), 8L, TimeUnit.DAYS);
        //修改登录次数
        LambdaUpdateWrapper<User> lu = new LambdaUpdateWrapper<>();
        lu.set(User::getUpdateTime, LocalDateTime.now());
        lu.setSql("login_number=login_number+1");
        lu.eq(User::getId, id);

        userService.update(lu);
    }


    /**
     * 统计登录次数
     * @return
     */
    public Integer[] statistics() {
        Integer[] res = new Integer[7];
        LocalDate today = LocalDate.now();
        for (int i = 0; i < 7; i++) {
            String number = redisTemplate.opsForValue().get(LOGIN_NUMBER_KEY + today.minusDays(6 - i).format(DateTimeFormatter.ofPattern("yyyy-MM-dd")));
            res[i] = number == null ? 0 : Integer.parseInt(number);
        }
        return res;
    }

    /**
     * 设置用户code 并发送到用户邮件
     *
     * @param mail 邮箱
     * @param id   用户id
     */
    public void setCode(String mail, Integer id) {
        Long expire = redisTemplate.getExpire(CODE_KEY + id, TimeUnit.SECONDS);
        System.out.println(expire);
        if (expire != null && expire < 120) {
            String code = MyStringUtil.getRandomString(6);
            sendMailUtil.sendSimpleMail(mail, "找回密码", code);
            redisTemplate.opsForValue().set(CODE_KEY + id, code, 3L, TimeUnit.MINUTES);
        }
    }

    /**
     * 验证code  如果成功且是第一次验证重新设置code有效期5分钟
     *
     * @param id   用户id
     * @param code code
     * @return 结果
     */
    public boolean verifyCode(Integer id, String code, boolean isFirst) {
        String rCode = redisTemplate.opsForValue().get(CODE_KEY + id);
        if (code.equals(rCode)) {
            if (isFirst) {
                redisTemplate.opsForValue().set(CODE_KEY + id, code, 5L, TimeUnit.MINUTES);
            }
            return true;
        }
        return false;
    }
}
