package xyz.nyist.utils;

import lombok.extern.slf4j.Slf4j;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * @Description: 自定义的字符串工具类
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:25
 */
@Slf4j
public class MyStringUtil {
    private static final String KEY = "?id=";
    private static final Pattern P_IMG = Pattern.compile("<(img|IMG)(.*?)(/>|></img>|>)");
    private static final Pattern P_SRC = Pattern.compile("(src|SRC)=([\"'])(.*?)([\"'])");
    private static final String STR = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";


    public static String getRandomString(int length) {
        Random random = new Random();
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < length; i++) {
            int number = random.nextInt(62);
            sb.append(STR.charAt(number));
        }
        return sb.toString();
    }


    public static List<Integer> getImgSrcId(String content) {
        List<Integer> list = new ArrayList<>();
        //开始匹配content中的<img />标签
        Matcher mImg = P_IMG.matcher(content);
        boolean resultImg = mImg.find();
        if (resultImg) {
            while (resultImg) {
                //获取到匹配的<img />标签中的内容
                String strImg = mImg.group(2);
                //开始匹配<img />标签中的src
                Matcher mSrc = P_SRC.matcher(strImg);
                if (mSrc.find()) {
                    String strSrc = mSrc.group(3);
                    if (strSrc.contains(KEY)) {
                        list.add(Integer.valueOf(strSrc.substring(strSrc.indexOf(KEY) + KEY.length())));
                    }
                }
                //结束匹配<img />标签中的src
                //匹配content中是否存在下一个<img />标签，有则继续以上步骤匹配<img />标签中的src
                resultImg = mImg.find();
            }
        }
        else if (content.contains(KEY)) {
            list.add(Integer.valueOf(content.substring(content.indexOf(KEY) + KEY.length())));
        }
        return list;
    }
}
