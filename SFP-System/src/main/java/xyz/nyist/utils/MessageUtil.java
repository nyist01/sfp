package xyz.nyist.utils;

import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.OrderItem;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import xyz.nyist.configs.mybatis.MyPage;
import xyz.nyist.entity.Message;
import xyz.nyist.sevice.MessageService;
import xyz.nyist.vo.PageQuery;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/12 15:35
 */
@Slf4j
@Component("messageUtil")
public class MessageUtil {
    private static final Integer TEXT_TYPE = 1;
    private static final Integer IMG_TYPE = 2;
    private static final Integer FILE_TYPE = 3;

    @Resource
    private RedisTemplate<String, Message> redisTemplate;


    @Resource
    private FileUtil fileUtil;
    @Resource
    private MessageService messageService;
    private static final String MSG_KEY = "sfp:msg:";

    public void addMessage(Message message) {
        //redis key为两用户id值相加  取的时候可以做到初步筛选
        redisTemplate.opsForList().leftPush(MSG_KEY + (message.getFrom() + message.getTo()), message);
        if (IMG_TYPE.equals(message.getType())) {
            fileUtil.useFile(message.getData());
        }
    }


    public List<Message> getMessage(PageQuery<Map<String, Integer>> pageQuery) {
        int offset = 0;
        Integer from = pageQuery.getT().get("from");
        Integer to = pageQuery.getT().get("to");
        Integer page = pageQuery.getPage();
        Integer size = pageQuery.getSize();
        List<Message> list = redisTemplate.opsForList().range(MSG_KEY + (from + to), 0, -1);
        if (list != null) {
            list = list.stream().filter(i -> i.getFrom().equals(from) || i.getFrom().equals(to)).collect(Collectors.toList());
            offset = list.size();
            if (list.size() >= (page - 1) * size) {
                list = list.subList((page - 1) * size, Math.min(list.size(), page * size));
            }
            else {
                list = new ArrayList<>();
            }
        }
        else {
            list = new ArrayList<>();
        }
        if (list.size() < size) {
            LambdaQueryWrapper<Message> lq = Wrappers.lambdaQuery();
            lq.and(i -> i.eq(Message::getFrom, from).eq(Message::getTo, to));
            lq.or(i -> i.eq(Message::getFrom, to).eq(Message::getTo, from));
            MyPage<Message> myPage = new MyPage<>(page, size - list.size(), size, offset - list.size());
            myPage.setOrders(Collections.singletonList(OrderItem.desc("`time`")));
            List<Message> tmp = messageService.page(myPage, lq).getRecords();
            list.addAll(tmp);
        }
        Collections.reverse(list);
        return list;
    }

    @Async
    public void messageRedisToMySql() {
        log.info("执行message持久化开始..........");
        Set<String> keys = redisTemplate.keys(MSG_KEY + "*");
        if (keys != null) {
            keys.forEach(key -> {
                List<Message> list = redisTemplate.opsForList().range(key, 0, -1);
                if (list != null && messageService.saveBatch(list)) {
                    redisTemplate.delete(key);
                }
            });
        }
        log.info("执行message持久化结束..........");
    }

}
