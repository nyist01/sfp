package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 18:15
  */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "work_logs_user_view")
public class WorkLogsUserView implements Serializable {
    @TableField(value = "id")
    private Integer id;

    @TableField(value = "work_order_id")
    private Integer workOrderId;

    @TableField(value = "create_time")
    private LocalDateTime createTime;

    @TableField(value = "content")
    private String content;

    /**
     * 简述
     */
    @TableField(value = "sketch")
    private String sketch;

    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 昵称
     */
    @TableField(value = "nickname")
    private String nickname;

    private static final long serialVersionUID = 1L;
}
