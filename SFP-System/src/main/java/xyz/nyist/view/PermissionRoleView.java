package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/21 16:34
  */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "permission_role_view")
public class PermissionRoleView implements Serializable {
    @TableField(value = "permission_id")
    private Integer permissionId;

    /**
     * 父权限
     */
    @TableField(value = "permission_parent_id")
    private Integer permissionParentId;

    @TableField(value = "role_id")
    private Integer roleId;

    /**
     * 父角色
     */
    @TableField(value = "role_parent_id")
    private Integer roleParentId;

    /**
     * 角色英文名称
     */
    @TableField(value = "en_name")
    private String enName;

    /**
     * 授权路径
     */
    @TableField(value = "url")
    private String url;

    /**
     * 角色名称
     */
    @TableField(value = "name")
    private String name;

    private static final long serialVersionUID = 1L;
}
