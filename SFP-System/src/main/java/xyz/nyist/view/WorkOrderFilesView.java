package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 17:37
  */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "work_order_files_view")
public class WorkOrderFilesView implements Serializable {
    @TableField(value = "path")
    private String path;

    @TableField(value = "file_name")
    private String fileName;

    @TableField(value = "id")
    private Integer id;

    @TableField(value = "work_order_id")
    private Integer workOrderId;

    private static final long serialVersionUID = 1L;
}
