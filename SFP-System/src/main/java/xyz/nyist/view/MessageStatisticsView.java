package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDate;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 13:16
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "message_statistics_view")
public class MessageStatisticsView implements Serializable {
    @TableField(value = "days")
    private LocalDate days;

    @TableField(value = "type")
    private Integer type;

    @TableField(value = "count")
    private Long count;

    private static final long serialVersionUID = 1L;
}
