package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.security.core.GrantedAuthority;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/23 19:42
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user_role_view")
public class UserRoleView implements GrantedAuthority {
    /**
     * 角色名称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 角色英文名称
     */
    @TableField(value = "en_name")
    private String enName;

    /**
     * 用户 ID
     */
    @TableField(value = "user_id")
    private Integer userId;

    @TableField(value = "role_id")
    private Integer roleId;

    private static final long serialVersionUID = 1L;

    @Override
    @JsonIgnore
    public String getAuthority() {
        return this.enName;
    }
}
