package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/21 16:23
  */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "role_permission_view")
public class RolePermissionView implements Serializable {
    @TableField(value = "id")
    private Integer id;

    /**
     * 父角色
     */
    @TableField(value = "parent_id")
    private Integer parentId;

    /**
     * 角色名称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 角色英文名称
     */
    @TableField(value = "en_name")
    private String enName;

    /**
     * 备注
     */
    @TableField(value = "description")
    private String description;

    @TableField(value = "permission_id")
    private Integer permissionId;

    /**
     * 权限名称
     */
    @TableField(value = "permission_name")
    private String permissionName;

    private static final long serialVersionUID = 1L;
}
