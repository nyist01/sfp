package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/16 14:07
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user_simple")
@EqualsAndHashCode(callSuper = true)
public class UserSimple extends AbstractAuthenticationUser {
    @TableField(value = "id")
    private Integer id;

    /**
     * 账号
     */
    @TableField(value = "principal")
    private String principal;

    /**
     * 密码
     */
    @TableField(value = "credentials")
    private String credentials;

    /**
     * 是否为管理员(0否，1是)
     */
    @TableField(value = "is_admin")
    private Byte isAdmin;

    private static final long serialVersionUID = 1L;

    public UserSimple(String principal, String credentials, Byte isAdmin) {
        this.principal = principal;
        this.credentials = credentials;
        this.isAdmin = isAdmin;
        super.setAuthenticated(false);
    }


    public UserSimple(UserSimple userSimple, Collection<UserRoleView> authorities) {
        this.id = userSimple.getId();
        this.principal = userSimple.getPrincipal();
        this.credentials = userSimple.getCredentials();
        this.isAdmin = userSimple.getIsAdmin();
        super.setAuthenticated(true);
        super.setAuthorities(authorities);
    }

    @Override
    public String getCredentials() {
        return this.credentials;
    }

    @Override
    public String getPrincipal() {
        return this.principal;
    }


    @Override
    public void eraseCredentials() {
        super.eraseCredentials();
        credentials = null;
    }

    @Override
    public void setAuthorities(Collection<UserRoleView> authorities) {
        super.setAuthorities(authorities);
    }
}
