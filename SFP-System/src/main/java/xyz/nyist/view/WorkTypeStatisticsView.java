package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 14:53
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "work_type_statistics_view")
public class WorkTypeStatisticsView implements Serializable {
    @TableField(value = "type")
    private String type;

    @TableField(value = "count")
    private Long count;

    private static final long serialVersionUID = 1L;
}
