package xyz.nyist.view;

import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/24 15:44
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user_details")
public class UserDetails implements Serializable {
    /**
     * 部门名称
     */
    @TableField(value = "department_name")
    private String departmentName;

    @TableField(value = "id")
    private Integer id;

    /**
     * 账号
     */
    @TableField(value = "principal")
    private String principal;

    @TableField(value = "create_time")
    private LocalDateTime createTime;

    @TableField(value = "update_time")
    private LocalDateTime updateTime;

    @TableField(value = "login_number")
    private Integer loginNumber;

    @TableField(value = "tel")
    private String tel;

    @TableField(value = "mail")
    private String mail;

    /**
     * 是否为管理员(0否，1是)
     */
    @TableField(value = "is_admin")
    private Byte isAdmin;

    /**
     * 昵称
     */
    @TableField(value = "nickname")
    private String nickname;

    /**
     * 头像
     */
    @TableField(value = "avatar")
    private String avatar;

    /**
     * 部门id
     */
    @TableField(value = "department_id")
    private Integer departmentId;

    @TableField(value = "sex")
    private String sex;

    /**
     * 密码
     */
    @TableField(value = "credentials")
    private String credentials;

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<UserRoleView> roles;

    @TableField(exist = false)
    private Boolean firstLogin;
}
