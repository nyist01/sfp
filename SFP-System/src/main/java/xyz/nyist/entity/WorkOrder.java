package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import xyz.nyist.view.UserDetails;
import xyz.nyist.view.WorkLogsUserView;
import xyz.nyist.view.WorkOrderFilesView;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/5 17:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "work_order")
public class WorkOrder implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "user_id")
    private Integer userId;

    @TableField(value = "title")
    private String title;

    @TableField(value = "content")
    private String content;

    @TableField(value = "create_time")
    private LocalDateTime createTime;

    @TableField(value = "status")
    private Integer status;

    @TableField(value = "type")
    private String type;

    /**
     * 紧急级别（1，2，3，4）
     */
    @TableField(value = "emergency_level")
    private Integer emergencyLevel;

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<WorkOrderFilesView> workOrderFilesViews;
    @TableField(exist = false)
    private List<WorkLogsUserView> workLogsUserViews;
    @TableField(exist = false)
    private UserDetails userDetails;
}
