package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/23 20:57
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user")
public class User implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 账号
     */
    @TableField(value = "principal")
    private String principal;

    /**
     * 密码
     */
    @TableField(value = "credentials")
    private String credentials;

    @TableField(value = "create_time")
    private LocalDateTime createTime;

    @TableField(value = "update_time")
    private LocalDateTime updateTime;

    @TableField(value = "login_number")
    private Integer loginNumber;

    @TableField(value = "tel")
    private String tel;

    @TableField(value = "mail")
    private String mail;

    /**
     * 是否为管理员(0否，1是)
     */
    @TableField(value = "is_admin")
    private Byte isAdmin;

    /**
     * 昵称
     */
    @TableField(value = "nickname")
    private String nickname;

    /**
     * 头像
     */
    @TableField(value = "avatar")
    private String avatar;

    /**
     * 部门id
     */
    @TableField(value = "department_id")
    private Integer departmentId;

    @TableField(value = "sex")
    private String sex;

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<Integer> roleIds;

    public User(String principal, String credentials, String nickname, String avatar, Integer departmentId) {
        this.principal = principal;
        this.credentials = credentials;
        this.createTime = LocalDateTime.now();
        this.updateTime = LocalDateTime.now();
        this.nickname = nickname;
        this.avatar = avatar;
        this.departmentId = departmentId;
    }

    public User(Integer id, Integer departmentId) {
        this.id = id;
        this.departmentId = departmentId;
    }
}
