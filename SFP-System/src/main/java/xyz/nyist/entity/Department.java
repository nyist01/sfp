package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import java.time.LocalDateTime;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 12:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "department")
public class Department implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 父部门
     */
    @TableField(value = "parent_id")
    private Integer parentId;

    /**
     * 部门名称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 备注
     */
    @TableField(value = "description")
    private String description;

    /**
     * 创建时间
     */
    @TableField(value = "created")
    private LocalDateTime created;

    /**
     * 最后修改时间
     */
    @TableField(value = "updated")
    private LocalDateTime updated;

    private static final long serialVersionUID = 1L;
}
