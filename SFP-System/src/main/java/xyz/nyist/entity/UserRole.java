package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/23 20:58
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "user_role")
public class UserRole implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 用户 ID
     */
    @TableField(value = "user_id")
    private Integer userId;

    /**
     * 角色 ID
     */
    @TableField(value = "role_id")
    private Integer roleId;

    private static final long serialVersionUID = 1L;
}
