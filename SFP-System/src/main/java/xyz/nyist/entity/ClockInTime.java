package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.datatype.jsr310.deser.LocalTimeDeserializer;
import com.fasterxml.jackson.datatype.jsr310.ser.LocalTimeSerializer;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.format.annotation.DateTimeFormat;

import java.io.Serializable;
import java.time.LocalTime;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/17 16:49
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "clock_in_time")
public class ClockInTime implements Serializable {
    @TableId(value = "id", type = IdType.INPUT)
    private Integer id;


    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    @DateTimeFormat(pattern = "HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "HH:mm")
    @TableField(value = "start_time1")
    private LocalTime startTime1;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    @DateTimeFormat(pattern = "HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "HH:mm")
    @TableField(value = "end_time1")
    private LocalTime endTime1;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    @DateTimeFormat(pattern = "HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "HH:mm")
    @TableField(value = "start_time2")
    private LocalTime startTime2;

    @JsonDeserialize(using = LocalTimeDeserializer.class)
    @JsonSerialize(using = LocalTimeSerializer.class)
    @DateTimeFormat(pattern = "HH:mm")
    @JsonFormat(timezone = "GMT+8", pattern = "HH:mm")
    @TableField(value = "end_time2")
    private LocalTime endTime2;

    private static final long serialVersionUID = 1L;
}
