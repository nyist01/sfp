package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import xyz.nyist.vo.RoleVo;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:25
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "role")
public class Role implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 父角色
     */
    @TableField(value = "parent_id")
    private Integer parentId;

    /**
     * 角色名称
     */
    @TableField(value = "name")
    private String name;

    /**
     * 角色英文名称
     */
    @TableField(value = "en_name")
    private String enName;

    /**
     * 备注
     */
    @TableField(value = "description")
    private String description;

    /**
     * 创建时间
     */
    @TableField(value = "created")
    private LocalDateTime created;

    /**
     * 最后修改时间
     */
    @TableField(value = "updated")
    private LocalDateTime updated;

    private static final long serialVersionUID = 1L;

    @TableField(exist = false)
    private List<Integer> permissionIds;

    public Role(RoleVo roleVo) {
        this.id = roleVo.getId();
        this.parentId = roleVo.getParentId();
        this.name = roleVo.getName();
        this.enName = roleVo.getEnName();
        this.updated = LocalDateTime.now();
        if (roleVo.getId() == null) {
            this.created = LocalDateTime.now();
        }
        this.permissionIds = roleVo.getPermissions();
    }
}
