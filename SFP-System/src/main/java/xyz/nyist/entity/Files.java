package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.time.LocalDateTime;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 10:37
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "files")
public class Files implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "path")
    private String path;

    @TableField(value = "file_name")
    private String fileName;

    @TableField(value = "is_use")
    private Byte isUse;

    @TableField(value = "create_data")
    private LocalDateTime createData;

    private static final long serialVersionUID = 1L;

    public Files(Integer id) {
        this.id = id;
        this.isUse = Byte.valueOf("1");
    }
}
