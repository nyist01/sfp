package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/14 13:25
  */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "security")
public class Security implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "issue")
    private String issue;

    @TableField(value = "answers")
    private String answers;

    @TableField(value = "user_id")
    private Integer userId;

    private static final long serialVersionUID = 1L;
}
