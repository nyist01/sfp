package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * @Author: silence
 * @Description: 角色权限表
 * @Date:Create：in 2020/3/28 20:51
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "role_permission")
public class RolePermission implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    /**
     * 角色 ID
     */
    @TableField(value = "role_id")
    private Integer roleId;

    /**
     * 权限 ID
     */
    @TableField(value = "permission_id")
    private Integer permissionId;

    private static final long serialVersionUID = 1L;
}
