package xyz.nyist.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import java.io.Serializable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 12:23
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName(value = "work_order_files")
public class WorkOrderFiles implements Serializable {
    @TableId(value = "id", type = IdType.AUTO)
    private Integer id;

    @TableField(value = "files_id")
    private Integer filesId;

    @TableField(value = "work_order_id")
    private Integer workOrderId;

    private static final long serialVersionUID = 1L;
}
