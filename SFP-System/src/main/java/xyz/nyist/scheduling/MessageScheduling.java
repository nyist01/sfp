package xyz.nyist.scheduling;

import xyz.nyist.utils.MessageUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author: Silence
 * @Description: message定时任务
 * @Date:Create：in 2020/3/17 9:10
 */
@Component
public class MessageScheduling {

    @Resource(name = "messageUtil")
    private MessageUtil messageUtil;

    /**
     * 每天凌晨四点进行
     */
    @Scheduled(cron = "0 0 4 * * ?")
    public void persistentMessage() {
        messageUtil.messageRedisToMySql();
    }
}
