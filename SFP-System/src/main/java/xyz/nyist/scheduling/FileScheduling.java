package xyz.nyist.scheduling;

import xyz.nyist.utils.FileUtil;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;

/**
 * @Author: Silence
 * @Description: file定时任务
 * @Date:Create：in 2020/3/21 20:46
 */
@Component
public class FileScheduling {

    @Resource(name = "fileUtil")
    private FileUtil fileUtil;

    /**
     * 清理未使用的文件，
     * 1.修改文件是否使用
     * 2.删除截至到前一天未使用的文件
     * 3.每天凌晨4点10分进行
     */
    @Scheduled(cron = "0 10 4 * * ?")
    public void clearUpFile() {
        fileUtil.clearUpFile();
    }
}
