package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.WorkTypeStatisticsView;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 14:53
 */
public interface WorkTypeStatisticsViewMapper extends BaseMapper<WorkTypeStatisticsView> {
}
