package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.WorkLogsUserView;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 18:15
  */
public interface WorkLogsUserViewMapper extends BaseMapper<WorkLogsUserView> {
}
