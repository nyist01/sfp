package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.RolePermission;

/**
 * @Author: silence
 * @Description:
 * @Date:Create：in 2020/3/28 20:51
 */
public interface RolePermissionMapper extends BaseMapper<RolePermission> {
}
