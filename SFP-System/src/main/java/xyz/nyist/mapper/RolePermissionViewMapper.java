package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.RolePermissionView;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/21 16:23
  */
public interface RolePermissionViewMapper extends BaseMapper<RolePermissionView> {
}
