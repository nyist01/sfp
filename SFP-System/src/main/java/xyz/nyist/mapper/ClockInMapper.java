package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.ClockIn;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/17 16:20
  */
public interface ClockInMapper extends BaseMapper<ClockIn> {
}
