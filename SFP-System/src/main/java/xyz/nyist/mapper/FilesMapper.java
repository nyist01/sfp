package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.Files;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 10:37
 */
public interface FilesMapper extends BaseMapper<Files> {
}
