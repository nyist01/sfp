package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.Role;

import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/14 13:25
 */
public interface RoleMapper extends BaseMapper<Role> {

    /**
     * 查询  所有子节点
     *
     * @param id id
     * @return 结果
     */
    List<Role> findChild(Integer id);

}
