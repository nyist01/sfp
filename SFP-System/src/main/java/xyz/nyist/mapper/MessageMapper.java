package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.Message;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/11 15:22
  */
public interface MessageMapper extends BaseMapper<Message> {
}
