package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.UserRole;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/23 20:58
 */
public interface UserRoleMapper extends BaseMapper<UserRole> {
}
