package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.Department;

import java.util.List;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 12:23
 */
public interface DepartmentMapper extends BaseMapper<Department> {
    /**
     * 查询  所有子节点(包括自己)
     *
     * @param id id
     * @return 结果
     */
    List<Department> findChild(Integer id);
}
