package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.Security;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/14 13:25
  */
public interface SecurityMapper extends BaseMapper<Security> {
}
