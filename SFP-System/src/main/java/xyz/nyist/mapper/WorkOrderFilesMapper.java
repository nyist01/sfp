package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.WorkOrderFiles;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 12:23
 */
public interface WorkOrderFilesMapper extends BaseMapper<WorkOrderFiles> {
}
