package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.PermissionRoleView;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/2/21 16:34
  */
public interface PermissionRoleViewMapper extends BaseMapper<PermissionRoleView> {
}
