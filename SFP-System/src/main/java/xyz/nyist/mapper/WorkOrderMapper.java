package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.WorkOrder;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/5 17:25
 */
public interface WorkOrderMapper extends BaseMapper<WorkOrder> {
}
