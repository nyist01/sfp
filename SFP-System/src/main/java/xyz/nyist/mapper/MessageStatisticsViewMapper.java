package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.MessageStatisticsView;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/22 13:16
 */
public interface MessageStatisticsViewMapper extends BaseMapper<MessageStatisticsView> {
}
