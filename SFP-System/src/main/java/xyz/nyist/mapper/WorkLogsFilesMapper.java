package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.WorkLogsFiles;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 12:23
  */
public interface WorkLogsFilesMapper extends BaseMapper<WorkLogsFiles> {
}
