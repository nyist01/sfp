package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.WorkOrderLogs;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/4 18:12
 */
public interface WorkOrderLogsMapper extends BaseMapper<WorkOrderLogs> {
}
