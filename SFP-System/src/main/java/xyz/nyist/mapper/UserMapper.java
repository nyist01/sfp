package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.User;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/23 20:57
 */
public interface UserMapper extends BaseMapper<User> {
}
