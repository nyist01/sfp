package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.WorkOrderFilesView;

/**
  * @Author: Silence
  * @Description:
  * @Date:Create：in  2020/3/4 17:37
  */
public interface WorkOrderFilesViewMapper extends BaseMapper<WorkOrderFilesView> {
}
