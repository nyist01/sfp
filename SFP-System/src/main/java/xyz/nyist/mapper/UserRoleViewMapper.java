package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.UserRoleView;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/23 19:42
 */
public interface UserRoleViewMapper extends BaseMapper<UserRoleView> {
}
