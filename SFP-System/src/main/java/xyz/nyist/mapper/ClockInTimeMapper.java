package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.entity.ClockInTime;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/17 16:49
 */
public interface ClockInTimeMapper extends BaseMapper<ClockInTime> {
}
