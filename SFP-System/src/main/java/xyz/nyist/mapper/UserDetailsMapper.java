package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.UserDetails;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/3/24 15:44
 */
public interface UserDetailsMapper extends BaseMapper<UserDetails> {
}
