package xyz.nyist.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.nyist.view.UserSimple;

/**
 * @Author: Silence
 * @Description:
 * @Date:Create：in 2020/2/16 14:07
 */
public interface UserSimpleMapper extends BaseMapper<UserSimple> {
}
